package usermanagement;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.HubClient;
import library.FontLibrary;
import library.ImageLibrary;
import server.ServerClientPackage;
import server.UserInformation;

public class UserBarPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	private UserInformation ui;
	private JLabel acceptLabel, deleteLabel, userIconLabel;
	private JLabel userLabel, userTypeLabel;
	private HubClient hubClient;
	
	public UserBarPanel(UserInformation ui, HubClient hubClient) {
		this.hubClient = hubClient;
		this.setPreferredSize(new Dimension(850,60));
		this.setBackground(Color.WHITE);
		this.ui = ui; // NOTE THAT THIS USER INFO IS USER TO BE DISPLAYED, NOT THE ADMINISTRATOR VIEWING THIS INFO
		this.setLayout(new GridBagLayout());
		this.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(200,200,200)));
		instantiateComponents();
		createGUI();
		addActionListeners();
	}
	private void instantiateComponents() {
		Image deleteImage;
		Image acceptImage;
		
		if (ui.isAdmin()) {
			userTypeLabel = new JLabel("Administrator");
			deleteImage = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/blanksquare.png"), 20, 20);
			acceptImage = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/blanksquare.png"), 20, 20);
		} else if (ui.isActive()) {
			userTypeLabel = new JLabel("Active Member");
			deleteImage = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/deletePost.png"), 20, 20);
			acceptImage = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/blanksquare.png"), 20, 20);
		} else {
			userTypeLabel = new JLabel("Requesting Membership");
			deleteImage = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/deletePost.png"), 20, 20);
			acceptImage = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/accept.png"), 20, 20);
		}
		userTypeLabel.setFont(FontLibrary.getFont(customui.Constants.FONT_OPEN_SANS, 18));
		acceptLabel = new JLabel(new ImageIcon(acceptImage));
		acceptLabel.setAlignmentX(RIGHT_ALIGNMENT);
		acceptLabel.setAlignmentY(CENTER_ALIGNMENT);
		acceptLabel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));	
		
		deleteLabel = new JLabel(new ImageIcon(deleteImage));
		deleteLabel.setAlignmentX(RIGHT_ALIGNMENT);
		deleteLabel.setAlignmentY(CENTER_ALIGNMENT);
		deleteLabel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));	
		
		Image img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/userTop.png"), 20, 20);
		userIconLabel = new JLabel(new ImageIcon(img));
		userIconLabel.setAlignmentX(LEFT_ALIGNMENT);
		userIconLabel.setAlignmentY(CENTER_ALIGNMENT);
		userIconLabel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
		
		
		userLabel = new JLabel(ui.getUsername() + " : " + ui.getFname() + " "  + ui.getLname());
		userLabel.setFont(FontLibrary.getFont(customui.Constants.FONT_OPEN_SANS, 18));

	}
	private void createGUI() {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(10,0,10,5);
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(userIconLabel, gbc);
		gbc.weightx = 1.0;
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.anchor = GridBagConstraints.WEST;
		gbc.fill = GridBagConstraints.HORIZONTAL;
		this.add(userLabel, gbc);
		gbc.weightx = 0;
		gbc.gridx = 2;
		gbc.gridy = 0;
		gbc.fill = GridBagConstraints.NONE;
		gbc.anchor = GridBagConstraints.EAST;
		this.add(userTypeLabel, gbc);
		gbc.gridx = 3;
		gbc.gridy = 0;
		this.add(acceptLabel, gbc);
		gbc.gridx = 4;
		gbc.gridy = 0;
		this.add(deleteLabel, gbc);
	}
	
	private void addActionListeners() {
		
		deleteLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent me) {
				if (ui.isAdmin()) return;
				hubClient.sendPackage(new ServerClientPackage(server.ServerClientPackage.Category.DELETE_USER, ui));

			}
		});
		
		acceptLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent me) {
				if (ui.isAdmin() || ui.isActive()) return;
				hubClient.sendPackage(new ServerClientPackage(server.ServerClientPackage.Category.ACTIVATE_USER, ui));
			}
		});
		
	}
}
