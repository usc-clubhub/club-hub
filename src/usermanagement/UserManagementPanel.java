package usermanagement;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Vector;

import javax.swing.JPanel;

import client.HubClient;
import server.UserInformation;

public class UserManagementPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	HubClient hubClient;
	private Vector<UserInformation> userList;
	
	public UserManagementPanel(Vector<UserInformation> userList, HubClient hubClient) {
		this.hubClient = hubClient;
		this.userList = userList;
		this.setBackground(Color.WHITE);
		this.setLayout(new GridBagLayout());
		createGUI();
	}
	
	private void instantiateComponents() {
		
	}
	private void createGUI() {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		for (int i = 0; i < userList.size(); i++) {
			gbc.gridy = i;
			gbc.gridx = 0;
			gbc.anchor = GridBagConstraints.WEST;
			this.add(new UserBarPanel(userList.get(i), hubClient), gbc);
		}
	}
}
