/* DESIGNED AND CODED BY JEREMY AFTEM */
package splash;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.Constants;
import customui.BorderButton;
import customui.HubButton;
import library.FontLibrary;
import library.ImageLibrary;
import utilities.scalableLabel;

public class SignUpSuccessPanel extends JPanel {
	private static final long serialVersionUID = -6157734000544090104L;
	
	private Image bg;
	private scalableLabel bgLabel;
	private JPanel centerPanel, buttonBox;
	private JLabel descriptionLabel, signUpLabel, successLabel;
	private String clubName;
	private BorderButton goBackButton;
	public SignUpSuccessPanel(String clubName) {
		this.clubName = clubName;
		setLayout(new BorderLayout());
		setSize(1100,699);
		instantiateComponents();
		createGUI();
	}
	
	private void instantiateComponents() {	
		bg = ImageLibrary.getImage(Constants.SPLASH_IMAGE);
		/* scalableLabel is a JLabel */
		bgLabel = new scalableLabel(new ImageIcon(bg));
		centerPanel = new JPanel();		
		buttonBox = new JPanel();
		descriptionLabel = new JLabel( clubName);
		Font clubFont = FontLibrary.getFont(customui.Constants.FONT_GLOBER_BOLD, 40);
		descriptionLabel.setFont(clubFont);
		descriptionLabel.setForeground(Color.WHITE);
		descriptionLabel.setHorizontalAlignment(JLabel.CENTER);
		signUpLabel = new JLabel("Registration");
		successLabel = new JLabel("<html><body style='width: 310px'>" + "You have successfully requested membership with "
		                          + clubName + ". When a " + clubName + "member has accepted your request, you will be able to login."
		                          + "</body></html>");
		Font successFont = FontLibrary.getFont(customui.Constants.FONT_GLOBER_BOLD, 17.5f);
		successLabel.setFont(successFont);
		successLabel.setForeground(Color.WHITE);
		Font registerFont = FontLibrary.getFont(customui.Constants.FONT_GLOBER_BOLD, 20);
		signUpLabel.setFont(registerFont);
		signUpLabel.setForeground(Color.WHITE);
		signUpLabel.setHorizontalAlignment(JLabel.CENTER);
		goBackButton = new BorderButton("GO BACK", Color.WHITE, Color.WHITE);
	}
	
	private void createGUI() {
		bgLabel.setLayout(new GridBagLayout());
		/* Center Panel is a panel located vertically and horizontally in the center of the frame */
		centerPanel.setPreferredSize(new Dimension(810,380)); // 560
		centerPanel.setLayout(null);
		/* The components inside the center panel are placed by pixels,
		 * OK because user can't resize smaller than 1100x699 */
		JPanel blackT = new JPanel();
		goBackButton.setBounds(205, 320, 385,50);
		blackT.setBackground(new Color(0,0,0,135));
		blackT.setBounds(198, 0, 400, 410);
		signUpLabel.setBounds(0, 122, 810, 25);
		descriptionLabel.setBounds(0,82, 810, 40);
		successLabel.setBounds(205, 100, 410, 200);
		Image img = ImageLibrary.getScaledImage(ImageLibrary.getImage(Constants.SPLASH_LOGO), 125, 34);
		JLabel logoLabel = new JLabel(new ImageIcon(img));
		logoLabel.setBounds(0, 0, 810, 85);
		/* Add all the components to the center panel */
		centerPanel.add(goBackButton);
		centerPanel.add(successLabel);
		centerPanel.add(descriptionLabel);
		centerPanel.add(signUpLabel);
		centerPanel.add(logoLabel);
		centerPanel.add(buttonBox);
		centerPanel.add(blackT);
		/* Transparent panel */
		centerPanel.setOpaque(false);
		buttonBox.setOpaque(false);
		/* Add the centerPanel to the center (vertically and horizontally) of the JLabel */
		bgLabel.add(centerPanel, new GridBagConstraints());
		add(bgLabel);
	}

	public void setSelectedClubName(String selectedItem) {
		clubName = selectedItem;
		descriptionLabel.setText(clubName);
		successLabel.setText("<html><body style='width: 310px'>" + "You have successfully requested membership with "
                + clubName + ". When a " + clubName + " member has accepted your request, you will be able to login."
                + "</body></html>");
		repaint();
	}
	
	public BorderButton getGoBackButton() {
		return goBackButton;
	}
	public void setBackgroundImage(ImageIcon ic) {
		bgLabel.setImg(ic);
		revalidate();
		repaint();
	}
}