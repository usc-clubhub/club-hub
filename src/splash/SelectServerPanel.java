package splash;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.Constants;
import customui.BorderButton;
import customui.HubButton;
import customui.HubComboBox;
import customui.ThemeColors;
import library.ImageLibrary;
import utilities.scalableLabel;

public class SelectServerPanel extends JPanel {
	private static final long serialVersionUID = -8929153530572963396L;
	
	private Image bg;
	private scalableLabel bgLabel;
	private HubComboBox<String> clubComboBox, serverComboBox;
	private HubButton selectServerButton;
	private JPanel centerPanel, buttonBox;
	private File serverFile;
	//private File clubFile;
	private BorderButton createClubButton;
	private Vector<ServerInformation> serverList;
	

	public SelectServerPanel() {
		serverList = new Vector<ServerInformation>();
		setLayout(new BorderLayout());
		setSize(1100,699);
		instantiateComponents();
		createGUI();
	}
	
	public String getHostname(String schoolName) {
		for (ServerInformation si : serverList) {
			if (si.getSchoolName() == schoolName) {
				return si.getServerAddress();
			}
		}
		return null;
	}
	public int getPort(String schoolName) {
		for (ServerInformation si : serverList) {
			if (si.getSchoolName() == schoolName) {
				return si.getPort();
			}
		}
		return 0;
	}
	
	//Reads the list of all available hosts into the combo box
	private void populateServerComboBox() throws IOException {
		String schoolName = null, serverName = null;
		int port = 0;
		serverFile = new File(Constants.SERVERS_FILE);
		Scanner read = new Scanner (serverFile);
		//read.useDelimiter("|");
		read.useDelimiter(", *");
		int i = 0;
		while (read.hasNext()) {
		   schoolName = read.next();
		   serverName = read.next();
		   port = Integer.parseInt(read.next());
		   serverComboBox.insertItemAt(schoolName, i);
		   serverList.add(new ServerInformation(schoolName,serverName,port));
		   i++;
		}
		read.close();
	}	
	//Reads the list of all available hosts into the combo box
	
	private void instantiateComponents() {	
		bg = ImageLibrary.getImage(Constants.SPLASH_IMAGE);
		/* scalableLabel is a JLabel */
		bgLabel = new scalableLabel(new ImageIcon(bg));
		bgLabel.setOpaque(false);
		clubComboBox = new HubComboBox<String>();
		serverComboBox = new HubComboBox<String>();
		selectServerButton = new HubButton(Constants.SELECT_SERVER_TEXT, ThemeColors.LOGIN_COLOR, ThemeColors.LOGIN_HIGHLIGHT_COLOR);
		createClubButton = new BorderButton(Constants.CREATE_CLUB_TEXT + " AT " + (String) serverComboBox.getSelectedItem(), ThemeColors.SIGNUP_COLOR, ThemeColors.SIGNUP_HIGHLIGHT_COLOR);

		centerPanel = new JPanel();		
		buttonBox = new JPanel();
	}
	public void setBackgroundImage(ImageIcon ic) {
		bgLabel.invalidate();
		bgLabel.setImg(ic);
		bgLabel.revalidate();
		bgLabel.repaint();
		this.revalidate();
		this.repaint();
	}
	
	private void createGUI() {
		bgLabel.setLayout(new GridBagLayout());
		/* Center Panel is a panel located vertically and horizontally in the center of the frame */
		centerPanel.setPreferredSize(new Dimension(310,360));
		centerPanel.setLayout(null);
		/* The components inside the center panel are placed by pixels,
		 * OK because user can't resize smaller than 1100x699 */
		serverComboBox.setText(Constants.SERVER_COMBOBOX_DEF_TEXT);
		clubComboBox.setText(Constants.CLUB_COMBOBOX_DEF_TEXT);
		try {
			populateServerComboBox();
			//populateClubComboBox();
		} catch (IOException e) {
			e.getMessage();
		}
		serverComboBox.setBounds(0, 115, 310, 50);
		clubComboBox.setBounds(0, 173, 310, 50);
		Image img = ImageLibrary.getScaledImage(ImageLibrary.getImage(Constants.SPLASH_LOGO), 310, 85);
		JLabel logoLabel = new JLabel(new ImageIcon(img));
		logoLabel.setBounds(0, 0, 310, 85);
		/* buttonBox: Container for buttons login and sign out */
		buttonBox.setLayout(null);
		buttonBox.setBounds(0, 240, 310, 50);
		selectServerButton.setBounds(0,0,310,50);
		createClubButton.setBounds(0, 310, 310, 50);

		buttonBox.add(selectServerButton);
		buttonBox.setOpaque(false);
		/* Add all the components to the center panel */
		centerPanel.add(logoLabel);
		centerPanel.add(serverComboBox);
		centerPanel.add(clubComboBox);
		centerPanel.add(buttonBox);
		centerPanel.add(createClubButton);
		createClubButton.setVisible(false);

		/* Transparent panel */
		centerPanel.setOpaque(false);
		/* Add the centerPanel to the center (vertically and horizontally) of the JLabel */
		bgLabel.add(centerPanel, new GridBagConstraints());
		add(bgLabel);
	}

	public BorderButton getCreateClubButton() {
		return createClubButton;
	}
	public HubButton getButton() {
		return selectServerButton;
	}
	public String getSelectedServer(){
		return (String)serverComboBox.getSelectedItem();
	}
	public String getSelectedClub(){
		return (String)clubComboBox.getSelectedItem();
	}
	public HubComboBox<String> getServerSelectBox(){
		return serverComboBox;
	}
	public HubComboBox<String> getClubSelectBox(){
		return clubComboBox;
	}

	public void setCreateClubButtonVisible(boolean b) {
		createClubButton.setText("CREATE CLUB AT " + ((String) serverComboBox.getSelectedItem()).toUpperCase());
		createClubButton.setVisible(b);
	}


}
	
