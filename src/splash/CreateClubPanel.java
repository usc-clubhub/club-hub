/* DESIGNED AND CODED BY JEREMY AFTEM */
package splash;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import client.Constants;
import client.HubClient;
import client.PasswordHasher;
import customui.HubButton;
import customui.HubPassField;
import customui.HubTextField;
import customui.ThemeColors;
import library.FontLibrary;
import library.ImageLibrary;
import server.ServerClientPackage;
import server.ServerClientPackage.Category;
import utilities.scalableLabel;

public class CreateClubPanel extends JPanel {
	private static final long serialVersionUID = -6157734000544090104L;
	
	private Image bg;
	private scalableLabel bgLabel;
	private HubTextField usernameTextField, fnameTextField, lnameTextField, newClubTextField;
	private HubPassField passwordTextField, confirmPasswordTextField;
	private HubButton createClubButton, cancelButton;
	private JPanel centerPanel, buttonBox;
	private JLabel descriptionLabel, signUpLabel;
	private HubClient hubClient;
	private LoginGUI lgui;
	
	public CreateClubPanel(HubClient hc, LoginGUI lgui) {
		setLayout(new BorderLayout());
		setSize(1100,699);
		instantiateComponents();
		createGUI();
		addActions();
		hubClient = hc;
		this.lgui = lgui;
	}
	
	private void instantiateComponents() {	
		bg = ImageLibrary.getImage(Constants.SPLASH_IMAGE);
		/* scalableLabel is a JLabel */
		bgLabel = new scalableLabel(new ImageIcon(bg));
		usernameTextField = new HubTextField("Email");
		usernameTextField.setIcon(Constants.USER_ICON);
		newClubTextField = new HubTextField("New Club Name");
		passwordTextField = new HubPassField(Constants.PASSWORD_TEXT);
		confirmPasswordTextField = new HubPassField("Confirm Password");
		fnameTextField = new HubTextField("First Name");
		lnameTextField = new HubTextField("Last Name");
		createClubButton = new HubButton(Constants.CREATE_CLUB_TEXT, ThemeColors.SIGNUP_COLOR, ThemeColors.SIGNUP_HIGHLIGHT_COLOR);
		cancelButton = new HubButton(Constants.CANCEL_TEXT, ThemeColors.LOGIN_COLOR, ThemeColors.LOGIN_HIGHLIGHT_COLOR);
		centerPanel = new JPanel();		
		buttonBox = new JPanel();
		descriptionLabel = new JLabel("Create New Club");
		Font clubFont = FontLibrary.getFont(customui.Constants.FONT_GLOBER_BOLD, 40);
		descriptionLabel.setFont(clubFont);
		descriptionLabel.setForeground(Color.WHITE);
		descriptionLabel.setHorizontalAlignment(JLabel.CENTER);
		signUpLabel = new JLabel("Registration");
		Font registerFont = FontLibrary.getFont(customui.Constants.FONT_GLOBER_BOLD, 20);
		signUpLabel.setFont(registerFont);
		signUpLabel.setForeground(Color.WHITE);
		signUpLabel.setHorizontalAlignment(JLabel.CENTER);
	}
	
	private void createGUI() {
		bgLabel.setLayout(new GridBagLayout());
		/* Center Panel is a panel located vertically and horizontally in the center of the frame */
		centerPanel.setPreferredSize(new Dimension(810,510)); // 560
		centerPanel.setLayout(null);
		/* The components inside the center panel are placed by pixels,
		 * OK because user can't resize smaller than 1100x699 */
		newClubTextField.setBounds(250,155,310,50);
		signUpLabel.setBounds(0, 122, 810, 25);
		descriptionLabel.setBounds(0,82, 810, 40);
		fnameTextField.setBounds(250, 213, 150, 50);
		lnameTextField.setBounds(410, 213, 150, 50);
		usernameTextField.setBounds(250, 271, 310, 50);
		passwordTextField.setBounds(250, 329, 310, 50);
		confirmPasswordTextField.setBounds(250, 387, 310, 50);
		Image img = ImageLibrary.getScaledImage(ImageLibrary.getImage(Constants.SPLASH_LOGO), 125, 34);
		JLabel logoLabel = new JLabel(new ImageIcon(img));
		logoLabel.setBounds(0, 0, 810, 85);
		/* buttonBox: Container for buttons login and sign out */
		buttonBox.setLayout(null);
		buttonBox.setBounds(250, 451, 310, 60);
		createClubButton.setBounds(0,0,147,50);
		cancelButton.setBounds(160,0,147,50);
		//signupButton.setPreferredSize(new Dimension(310, 50));
		//signupButton.setBounds(163,0,147,50);
		buttonBox.add(createClubButton);
		buttonBox.add(cancelButton);
		/* Add all the components to the center panel */
		centerPanel.add(descriptionLabel);
		centerPanel.add(signUpLabel);
		centerPanel.add(newClubTextField);
		centerPanel.add(logoLabel);
		centerPanel.add(fnameTextField);
		centerPanel.add(lnameTextField);
		centerPanel.add(usernameTextField);
		centerPanel.add(passwordTextField);
		centerPanel.add(confirmPasswordTextField);
		centerPanel.add(buttonBox);
		/* Transparent panel */
		centerPanel.setOpaque(false);
		buttonBox.setOpaque(false);
		/* Add the centerPanel to the center (vertically and horizontally) of the JLabel */
		bgLabel.add(centerPanel, new GridBagConstraints());
		add(bgLabel);
	}
	
	public void addActions(){
		createClubButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				//Checks to see if fields are all filled
				//System.out.println("Button pressed");
				Vector<String> fields = new Vector<>();
				String hostname, clubname, fname, lname, username, password1, password2;
				hostname = hubClient.getHostname();
				clubname = newClubTextField.getText();
				fname = fnameTextField.getText();
				lname = lnameTextField.getText();
				username = usernameTextField.getText();
				password1 = passwordTextField.getText();
				password2 = confirmPasswordTextField.getText();
				PasswordHasher pwh = new PasswordHasher();
				String hashedPass = pwh.encrypt(password1);
				fields.addElement(hostname);
				fields.addElement(clubname);
				fields.addElement(fname);
				fields.addElement(lname);
				fields.addElement(username);
				fields.addElement(hashedPass);
				
				if(!fieldsAllFilled(fields)){
					JOptionPane.showMessageDialog(CreateClubPanel.this, "Please fill out all fields", 
							"Error", JOptionPane.PLAIN_MESSAGE);
					return;
				}
				
				//Checks to see if passwords match
				if(!password1.equals(password2)){
					JOptionPane.showMessageDialog(CreateClubPanel.this, "Passwords do not match", 
							"Error", JOptionPane.PLAIN_MESSAGE);
					return;
				}
				
				//Send package to client
				ServerClientPackage scp = new ServerClientPackage(Category.CLUB_REGISTER, fields);
				hubClient.sendPackage(scp);
				
			}
		});
	}
	public HubButton getCancelButton() {
		return cancelButton;
	}
	
	public HubButton getButtonSignUp() {
		return createClubButton;
	}
	
	private boolean fieldsAllFilled(Vector<String> fields){
		for(String field : fields){
			if (field.isEmpty())
				return false;
		}
		return true;
	}

	public void setBackgroundImage(ImageIcon ic) {
		bgLabel.invalidate();
		bgLabel.setImg(ic);
		bgLabel.revalidate();
		bgLabel.repaint();
		this.revalidate();
		this.repaint();
	}
}