package splash;

import java.io.Serializable;

public class ServerInformation implements Serializable{
	private static final long serialVersionUID = 1L;
	private String serverAddress;
	private String schoolName;
	private int port;
	public ServerInformation(String schoolName, String serverAddress, int port) {
		this.schoolName = schoolName;
		this.serverAddress = serverAddress;
		this.port = port;
	}
	public String getServerAddress() {
		return serverAddress;
	}
	public void setServerAddress(String serverAddress) {
		this.serverAddress = serverAddress;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public int getPort() {
		return port;
	}
	public void setPort(int port) {
		this.port = port;
	}
}
