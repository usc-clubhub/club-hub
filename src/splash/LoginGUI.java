/* DESIGNED AND CREATED BY JEREMY AFTEM
 * This JFrame is a window frame for the LoginPanel and SelectServerPanel
 */

package splash;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

import client.HubClient;
import client.PasswordHasher;
import customui.BorderButton;
import customui.HubButton;
import customui.HubComboBox;
import customui.HubPassField;
//import main.GuestMainFrame;
import library.ImageLibrary;
import main.GuestMainFrame;
import main.MainFrame;
import server.ServerClientPackage;
import server.ServerClientPackage.Category;
import server.UserInformation;


public class LoginGUI extends JFrame {
	
	private HubClient hubClient = null;
	private static final long serialVersionUID = -6157734000544090104L;
	private LoginPanel login;
	private SelectServerPanel selectServer;
	private CreateClubPanel createClubPanel;
	private SignUpSuccessPanel signUpSuccessPanel;
	private SignUpPanel signUpPanel;
	private HubButton selectServerButton, signUpButton, signUpSendButton, loginButton, cancelSignUpButton, createClubButton;
	private String selectedServer;
	private String selectedClub;
	private HubPassField passField;
	private HubComboBox<String> serverSelectBox, clubSelectBox;

	
	private ImageIcon currBg;
	
	public LoginGUI() {
		setTitle("Club Hub Login");
		setSize(1200,796);
		setMinimumSize(new Dimension(1200,796));
		setMaximumSize(new Dimension(1200,796));
		setPreferredSize(new Dimension(1200,796));
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);	
		instantiateComponents();
		createGUI();
		addActionListeners();	
		//Get port and create client
		//hubClient = new HubClient();
	}
	
	private void instantiateComponents() {

		login = new LoginPanel();
		selectServer = new SelectServerPanel();
		signUpSuccessPanel = new SignUpSuccessPanel(null);
		signUpPanel = new SignUpPanel(null);
		cancelSignUpButton = signUpPanel.getCancelButton();
		selectServerButton = selectServer.getButton();
		signUpButton = login.getButtonSignUp();
		loginButton = login.getButtonLogin();
		serverSelectBox = selectServer.getServerSelectBox();
		clubSelectBox = selectServer.getClubSelectBox();
		createClubButton = selectServer.getCreateClubButton();
	}
	
	private void createGUI() {
		add(selectServer);
	}
	
	private void addActionListeners() {
		passField = login.getPasswordTextField();
		passField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
			}
		});
		selectServerButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Establish connection to host and database
				selectedServer = selectServer.getSelectedServer();
				selectedClub = selectServer.getSelectedClub();
				//Do nothing if no club or server is selected
				if (selectedServer == null || selectedClub == null) return;
				//Repaint screen
				getContentPane().removeAll();
				getContentPane().invalidate();
				getContentPane().add(login);
				getContentPane().revalidate();
				getContentPane().repaint();
				setVisible(true);
			}
		});
		serverSelectBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae){
				String schoolName = (String)serverSelectBox.getSelectedItem();
				String hostname = selectServer.getHostname(schoolName);
				int port = selectServer.getPort(schoolName);
				hubClient = new HubClient(hostname, port, LoginGUI.this);
				ServerClientPackage scp = new ServerClientPackage(Category.REQUEST_CLUBS, hostname);
				hubClient.sendPackage(scp);
				selectServer.setCreateClubButtonVisible(true);
			}
		});
		clubSelectBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				signUpPanel.setSelectedClubName((String)clubSelectBox.getSelectedItem());
				signUpPanel.setClient(hubClient);
			}
		});
		signUpButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				getContentPane().removeAll();
				getContentPane().invalidate();
				getContentPane().add(signUpPanel);
				getContentPane().revalidate();
				getContentPane().repaint();
				setVisible(true);
			}
		});
		cancelSignUpButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				getContentPane().removeAll();
				getContentPane().invalidate();
				getContentPane().add(selectServer);
				getContentPane().revalidate();
				getContentPane().repaint();
				setVisible(true);
			}
		});
		createClubButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				createClubPanel = new CreateClubPanel(hubClient, LoginGUI.this);
				createClubPanel.setBackgroundImage(currBg);
				getContentPane().removeAll();
				getContentPane().invalidate();
				getContentPane().add(createClubPanel);
				getContentPane().revalidate();
				getContentPane().repaint();
				HubButton cancelClubCreate = createClubPanel.getCancelButton();
				cancelClubCreate.addActionListener(new ActionListener(){
					public void actionPerformed(ActionEvent ae){
						getContentPane().removeAll();
						getContentPane().invalidate();
						getContentPane().add(selectServer);
						getContentPane().revalidate();
						getContentPane().repaint();
						setVisible(true);
					}
				});
			}
		});
		loginButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				PasswordHasher ph = new PasswordHasher();
				String username, password;
				String schoolName = (String)serverSelectBox.getSelectedItem();
				String hostname = selectServer.getHostname(schoolName);
				username = login.getUsernameField();
				password = ph.encrypt(login.getPasswordField());
				if(username.isEmpty() || password.isEmpty()){
					JOptionPane.showMessageDialog(null, "Incorrect username or password", 
							"Login failed", JOptionPane.PLAIN_MESSAGE);
				}
				ArrayList<String> loginInfo = new ArrayList<>();
				loginInfo.add(hostname.replaceAll(" ", "_"));
				loginInfo.add(selectedClub.replaceAll(" ", "_"));
				loginInfo.add(username);
				loginInfo.add(password);
				ServerClientPackage scp = new ServerClientPackage(Category.LOGIN, loginInfo);
				hubClient.sendPackage(scp);
			}
		});
		BorderButton goBackButton = signUpSuccessPanel.getGoBackButton();
		goBackButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				getContentPane().removeAll();
				getContentPane().invalidate();
				getContentPane().add(selectServer);
				getContentPane().revalidate();
				getContentPane().repaint();
				setVisible(true);
			}
		});
		
		HubButton guestButton = login.getGuestButton();
		guestButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				ArrayList<String> serverAndClub = new ArrayList<>();
				String schoolName = (String)serverSelectBox.getSelectedItem();
				String hostname = selectServer.getHostname(schoolName);
				serverAndClub.add(hostname);
				serverAndClub.add((String)clubSelectBox.getSelectedItem());
				ServerClientPackage scp = new ServerClientPackage(Category.GUEST_LOGIN, serverAndClub);
				hubClient.sendPackage(scp);
			}
		});
	}
	public void populateClubs(Vector<String> clubs){
		clubSelectBox.removeAllItems();
		for(int i = 0; i < clubs.size(); i++){
			clubSelectBox.insertItemAt(clubs.get(i), i);
		}
	}
	public static void main(String args[]) {

		
		System.setProperty("awt.useSystemAAFontSettings","on");
		System.setProperty("swing.aatext", "true");
		LoginGUI loginGUI = new LoginGUI();
		try {
			Image icon = ImageIO.read(new File("img/ui/icon.png"));
			loginGUI.setIconImage(icon);
		} catch (IOException e) {
			e.printStackTrace();
		}
		loginGUI.setVisible(true);
	}
	public void showCannotConnectToServer() {
		JOptionPane.showMessageDialog(this, "Cannot connect to server.", "Warning", JOptionPane.WARNING_MESSAGE);
	}	
    protected void paintComponent(Graphics g) {
        Graphics2D graphics2D = (Graphics2D)g;
        //Antialiasing ON
        graphics2D.setRenderingHint(
                RenderingHints.KEY_ANTIALIASING, 
                RenderingHints.VALUE_ANTIALIAS_ON);
    }
    public void proceedToMainWindow(){
    	MainFrame mf = new MainFrame(hubClient);
		try {
			Image icon = ImageIO.read(new File("img/ui/icon.png"));
			mf.setIconImage(icon);
		} catch (IOException e) {
			e.printStackTrace();
		}
    	mf.setVisible(true);
    	this.setVisible(false);
    }
    public void proceedToGuestWindow(){
    	hubClient.setUserInfo(new UserInformation("Guest", "Guest", "Guest", (String)clubSelectBox.getSelectedItem(), new Color(35,35,35)));
    	//GuestMainFrame gmf = new GuestMainFrame(hubClient, (String)clubSelectBox.getSelectedItem());
    	//gmf.setVisible(true);
    	GuestMainFrame gmf = new GuestMainFrame(hubClient, (String)clubSelectBox.getSelectedItem());
		try {
			Image icon = ImageIO.read(new File("img/ui/icon.png"));
			gmf.setIconImage(icon);
		} catch (IOException e) {
			e.printStackTrace();
		}
    	gmf.setVisible(true);
    	this.setVisible(false);
    }
    public void showLoginError(){
    	JOptionPane.showMessageDialog(null, "Incorrect username or password", 
				"Login failed", JOptionPane.PLAIN_MESSAGE);
    }
    public void showSignUpError1(){
    	JOptionPane.showMessageDialog(null, "You have already requested access", 
				"Sign-up failed", JOptionPane.PLAIN_MESSAGE);
    }
    public void showSignUpError2(){
    	JOptionPane.showMessageDialog(null, "A user with this username already exists", 
				"Sign-up failed", JOptionPane.PLAIN_MESSAGE);
    }
    public void showSignUpSuccess(){
    	getContentPane().removeAll();
		getContentPane().invalidate();
		signUpSuccessPanel.setSelectedClubName(signUpPanel.getClubName());
		getContentPane().add(signUpSuccessPanel);
		getContentPane().revalidate();
		getContentPane().repaint();
		setVisible(true);
    }
    
    public void setBackgroundImage(BufferedImage img) {
		ImageIcon ic = new ImageIcon(img);
    	currBg = ic;
		selectServer.setBackgroundImage(ic);
		//createClubPanel.setBackgroundImage(ic);
		signUpPanel.setBackgroundImage(ic);
		signUpSuccessPanel.setBackgroundImage(ic);
		login.setBackgroundImage(ic);
    }
}