package calendar;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
//import java.util.Date;
import java.util.Date;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import client.HubClient;
import customui.Constants;
import customui.HubButton;
import customui.HubTextField;
import customui.ThemeColors;
import library.FontLibrary;
import server.ServerClientPackage;
import server.ServerClientPackage.Category;


public class AddEventPanel {
	//GUI
	private JPanel mainaePanel, midPanel;
	private JLabel eventNameLabel;
	private HubTextField eventNameTF, dateTF;
	private HubButton createEventButton;
	//Server-Client
	private HubClient hc;
	//Custom UI
	private Font font;
	private CalendarPanel cp;
	
	//Constructor
	public AddEventPanel(HubClient hc, CalendarPanel cp){
		this.hc = hc;
		this.cp = cp;
		initializeVariables();
		createGUI();
		addActions();
	}

	private void initializeVariables() {
		//Main
		mainaePanel = new JPanel();
		
		//Font
		font = FontLibrary.getFont(Constants.FONT_GLOBER_REGULAR, 18);	
		//Top
		midPanel = new JPanel();
		eventNameLabel = new JLabel("Select a date and enter the event details");
		eventNameTF = new HubTextField("Enter Event Name Here");
		dateTF = new HubTextField("SELECT A DATE");
		
		//Bottom
		createEventButton = new HubButton("Create Event", ThemeColors.DARKGRAY, ThemeColors.DARKGRAY);
	}

	private void createGUI() {
		
		mainaePanel.setLayout(new GridLayout(4,1));
		mainaePanel.setFont(font);
		mainaePanel.setBorder(BorderFactory.createTitledBorder(null, "Add an Event", TitledBorder.CENTER, TitledBorder.TOP, font));
		mainaePanel.setBackground(Color.WHITE);
		
		//Mid
		eventNameLabel.setFont(font);
		//midPanel.setLayout(new GridLayout(3,1));
		dateTF.setEditable(false);
		eventNameTF.setPreferredSize(new Dimension(250, 30));
		dateTF.setPreferredSize(new Dimension(250,30));
		midPanel.add(eventNameLabel);
		midPanel.add(eventNameTF);
		midPanel.add(dateTF);

		mainaePanel.add(midPanel);
		mainaePanel.add(Box.createVerticalGlue());
		mainaePanel.add(Box.createVerticalGlue());
		
		//Bottom
		createEventButton.setBackground(ThemeColors.DARKGRAY);
		createEventButton.setOpaque(true);
		mainaePanel.add(createEventButton);
	}

	private void addActions() {
		createEventButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(eventNameTF.getText().isEmpty()){
					JOptionPane.showMessageDialog(null, "Event must have a name", 
							"Error", JOptionPane.PLAIN_MESSAGE);
					return;
				} else if (cp.getSelectedDate() == null){
					JOptionPane.showMessageDialog(null, "Please select a date", 
							"Error", JOptionPane.PLAIN_MESSAGE);
					return;
				}
				createNewEvent();
			}
			
		});
	}
	
	public void createNewEvent(){
		String eventName = eventNameTF.getText();
		Date eventDate = cp.getSelectedDate();
		CalendarEvent newEvent = new CalendarEvent(eventName, eventDate);
		//System.out.println(newEvent.getEventName() + " @ " + newEvent.getEventDate()); 
		ServerClientPackage scp = new ServerClientPackage(Category.ADD_CALENDAR_EVENT, newEvent);
		hc.sendPackage(scp);
	}
	
	public void setDateTF(Date date){
		DateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
		String dateString = dateFormat.format(date);
		dateTF.setText(dateString);
	}
	
	public JPanel getaePanel() {
		return mainaePanel;
	}

}
