package calendar;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import client.HubClient;
import customui.Constants;
import customui.HubButton;
import library.FontLibrary;
import server.ServerClientPackage;
import server.ServerClientPackage.Category;

public class EventsOnDatePanel {
	//GUI
	private JPanel eodPanel;
	
	//Server-Client 
	private HubClient hc;
	//private CalendarPanel cp;
	
	//Custom UI
	private Font font;
	
	public EventsOnDatePanel(HubClient hc){
		this.hc = hc;
		initializeVariables();
		createGUI();
	}

	private void initializeVariables() {
		font = FontLibrary.getFont(Constants.FONT_GLOBER_REGULAR, 18);
		eodPanel = new JPanel();
	}

	private void createGUI() {
		//Set border
		eodPanel.setBorder(BorderFactory.createTitledBorder(null, "Upcoming Events", TitledBorder.CENTER, TitledBorder.TOP, font));
		//Set background
		eodPanel.setBackground(Color.WHITE);
	}
	
	public JPanel geteodPanel() {
		return eodPanel;
	}

	public void populateEvents(ArrayList<CalendarEvent> events) {
		eodPanel.removeAll();
		for(CalendarEvent ce : events){
			JPanel jp = new JPanel();
			jp.setLayout(new GridLayout(2, 1));
			JLabel nameLabel = new JLabel(ce.getEventName(), SwingConstants.CENTER);
			nameLabel.setFont(font);
			jp.add(nameLabel);
			HubButton deleteButton = new HubButton("Delete Event", Color.PINK, Color.PINK);
			deleteButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent arg0) {
					int reply = JOptionPane.showConfirmDialog(null, "Delete " + ce.getEventName() + "?", 
							"", JOptionPane.YES_NO_OPTION);
					if(reply == JOptionPane.YES_OPTION){
						deleteThisEvent(ce);
						JOptionPane.showMessageDialog(null, ce.getEventName() + " has been deleted");
						eodPanel.revalidate();
						eodPanel.repaint();
					}
				}			
			});
			jp.add(deleteButton);
			eodPanel.add(jp);
		}
		eodPanel.revalidate();
		eodPanel.repaint();
	}

	protected void deleteThisEvent(CalendarEvent ce) {
		ServerClientPackage scp = new ServerClientPackage(Category.DELETE_CALENDAR_EVENT, ce);
		hc.sendPackage(scp);
	}

}
