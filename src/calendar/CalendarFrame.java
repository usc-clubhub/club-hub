package calendar;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;

import client.HubClient;

//requestCalendarEvents
//create public void method takes arraylist of calendarevents, fills up list on panel
//send serverclient package w category requestCalendarEvents + date

public class CalendarFrame extends JFrame{
	
	private static final long serialVersionUID = 4177812638584761101L;
	//GUI
	private JPanel mainPanel, topPanel, bottomPanel;
	private CalendarPanel calPanel;
	private AddEventPanel aePanel;
	private EventsOnDatePanel eodPanel;
	//Server-Client
	private HubClient hc;
	
	public CalendarFrame(HubClient hc){
		this.hc = hc;
		hc.setCalendarFrame(this);
		initializeVariables();
		createGUI();
	}
	
	 
	private void initializeVariables() {
		mainPanel = new JPanel();
		topPanel = new JPanel();
		bottomPanel = new JPanel();
		
		calPanel = new CalendarPanel(hc);
		aePanel = new AddEventPanel(hc, calPanel);
		calPanel.setAEP(aePanel);
		eodPanel = new EventsOnDatePanel(hc);
	}


	private void createGUI() {
		//Dimensions
		Dimension d = new Dimension(1100, 650);
		//Layout
		mainPanel.setLayout(new GridLayout(2,1));
		mainPanel.setPreferredSize(d);
		
		topPanel.setLayout(new GridLayout(1,2));
		topPanel.add(calPanel.getcalPanel());
		topPanel.add(aePanel.getaePanel());
		
		bottomPanel.setLayout(new GridLayout(1,1));
		bottomPanel.add(eodPanel.geteodPanel());
		
		mainPanel.add(topPanel);
		mainPanel.add(bottomPanel);
	}


	public JPanel getPanel() {
		return mainPanel;
	}
	
	public void populateEvents(ArrayList<CalendarEvent> event) {
		eodPanel.populateEvents(event);
	}	

}
