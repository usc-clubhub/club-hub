package calendar;

import java.io.Serializable;
import java.util.Date;

public class CalendarEvent implements Serializable {

	private static final long serialVersionUID = -9057920952388155190L;
	private String eventName;
	private Date eventDate;
	private double eventCost;
	private double eventRevenue;
	
	//public Event(String eventName, Date eventDate){
	public CalendarEvent(String eventName, Date eventDate){
		this.eventName = eventName;
		this.eventDate = eventDate;
		//this.eventCost = eventCost;
		//this.eventRevenue = eventRevenue;
	}
	
	public String getEventName(){
		return eventName;
	}
	
	public Date getEventDate(){
		return eventDate;
	}
	
	public Double getEventCost(){
		return eventCost;
	}
	
	public Double getEventRevenue(){
		return eventRevenue;
	}
	
	public void setEventName(String eventName){
		this.eventName = eventName;
	}
	
	public void setEventDate(Date eventDate){
		this.eventDate = eventDate;
	}
}
