package calendar;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

import client.HubClient;
import customui.BorderButton;
import customui.Constants;
import customui.HubComboBox;
import customui.ThemeColors;
import library.FontLibrary;
import server.ServerClientPackage;
import server.ServerClientPackage.Category;

public class CalendarPanel extends JPanel{
    static JLabel monthLabel;
    static BorderButton prevButton, nextButton;
    static JTable calTable;
    static HubComboBox<String> cmbYear;
    static DefaultTableModel calTableModel; 
    static JScrollPane stblCalendar;
    static JPanel calPanel;
    GregorianCalendar cal;
    static int realYear, realMonth, realDay, currentYear, currentMonth;
    String[] headers = {"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
    static String[] months =  {"January", "February", "March", "April", 
    		"May", "June", "July", "August", "September", "October", "November", "December"};
    
    private Font font;
    private Date selectedDate = null;
    private HubClient hc;
    private AddEventPanel aep;
    
    public CalendarPanel(HubClient hc){
    	this.hc = hc;
    	intializeVariables();
    	createGUI();
    	addActions();
    	
    }
    
    public static void refreshCalendar(int month, int year){
        //Variables
       
        int totalDays, monthStart;
        
        prevButton.setEnabled(true);
        nextButton.setEnabled(true);
        if (month == 0 && year <= realYear-10){prevButton.setEnabled(false);} 
        if (month == 11 && year >= realYear+100){nextButton.setEnabled(false);} 
        monthLabel.setText(months[month]); 
        monthLabel.setBounds(160-monthLabel.getPreferredSize().width/2, 25, 180, 25); //Re-align label with calendar
        cmbYear.setSelectedItem(String.valueOf(year)); //Select the correct year in the combo box
        
        //Clear table
        for (int i=0; i<6; i++){
            for (int j=0; j<7; j++){
                calTableModel.setValueAt(null, i, j);
            }
        }
        
        
        GregorianCalendar cal = new GregorianCalendar(year, month, 1);
        totalDays = cal.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
        monthStart = cal.get(GregorianCalendar.DAY_OF_WEEK);
        
        for (int i=1; i<=totalDays; i++){
            int row = new Integer((i+monthStart-2)/7);
            int column  =  (i+monthStart-2)%7;
            calTableModel.setValueAt(i, row, column);
        }
        
        //Apply renderers
        calTable.setDefaultRenderer(calTable.getColumnClass(0), new tblCalendarRenderer());
    }
    
    private void intializeVariables() {
    	
    	font = FontLibrary.getFont(Constants.FONT_GLOBER_REGULAR, 18);
    	
    	monthLabel = new JLabel ("January");
    	monthLabel.setFont(font);
        cmbYear = new HubComboBox<String>();
        
        prevButton = new BorderButton("Prev", ThemeColors.DARKGRAY, ThemeColors.DARKGRAY);
        nextButton = new BorderButton("Next", ThemeColors.DARKGRAY, ThemeColors.DARKGRAY);
        
        calTableModel = new DefaultTableModel(){
			private static final long serialVersionUID = 5055950190570492402L;
			public boolean isCellEditable(int rowIndex, int colIndex){return false;}
		};
        calTable = new JTable(calTableModel);
        calTable.setCellSelectionEnabled(true);
        stblCalendar = new JScrollPane(calTable);
        
        calPanel = new JPanel();
		
        cal = new GregorianCalendar();
	}

	private void createGUI() {
		
		//Set the border
        calPanel.setBorder(BorderFactory.createTitledBorder(null, "Calendar", TitledBorder.CENTER, TitledBorder.TOP, font));
        //Set background
        calPanel.setBackground(Color.WHITE);
        calPanel.setOpaque(true);
        
        prevButton.setBackground(ThemeColors.DARKGRAY);
        prevButton.setOpaque(true);
        
        nextButton.setBackground(ThemeColors.DARKGRAY);
        nextButton.setOpaque(true);
        
        calPanel.add(prevButton);
	    calPanel.add(monthLabel);
	    calPanel.add(cmbYear);
	    calPanel.add(nextButton);
	    calPanel.add(stblCalendar);

        calPanel.setBounds(0, 0, 620, 635);
        monthLabel.setBounds(160-monthLabel.getPreferredSize().width/2, 25, 100, 25);
        cmbYear.setBounds(230, 305, 80, 20);
        prevButton.setBounds(10, 25, 50, 25);
        nextButton.setBounds(260, 25, 50, 25);
        stblCalendar.setBounds(10, 50, 300, 250);
        
        //frmMain.setResizable(false);
        //frmMain.setVisible(true);
        
        //Calendar
        realDay = cal.get(GregorianCalendar.DAY_OF_MONTH);
        realMonth = cal.get(GregorianCalendar.MONTH);
        realYear = cal.get(GregorianCalendar.YEAR);
        currentMonth = realMonth;
        currentYear = realYear;
		
        //Headers of months
        for (int i=0; i<7; i++){
            calTableModel.addColumn(headers[i]);
        }
        
        //Set background
        calTable.getParent().setBackground(calTable.getBackground());
        calTable.getTableHeader().setFont(font);;
        
        //No resize/reorder
        calTable.getTableHeader().setResizingAllowed(false);
        calTable.getTableHeader().setReorderingAllowed(false);
        
        //Single cell selection
        calTable.setColumnSelectionAllowed(true);
        calTable.setRowSelectionAllowed(true);
        calTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        
        //Set row/column count
        calTable.setRowHeight(38);
        calTableModel.setColumnCount(7);
        calTableModel.setRowCount(6);
        
        //Populate table
        for (int i=realYear-100; i<=realYear+100; i++){
            cmbYear.addItem(String.valueOf(i));
        }
        
        
	}
	
	 private void addActions() {
		 
		 refreshCalendar(realMonth, realYear);
		 
		 prevButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (currentMonth == 0){ //Back one year
		             currentMonth = 11;
		             currentYear -= 1;
		         }
		         else{ //Back one month
		             currentMonth -= 1;
		         }
		         refreshCalendar(currentMonth, currentYear);
			} 
		 });
		 
		 calTable.addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent me){
				int row = calTable.getSelectedRow();
				int col = calTable.getSelectedColumn();
				//Apply renderers
				calTable.setDefaultRenderer(calTable.getColumnClass(0), new SelectedCellRenderer());
				//revalidate();
				//repaint();
				selectedDate = getDateAt(row, col);
				aep.setDateTF(selectedDate);
				//System.out.println("date: " + selectedDate);
				if(selectedDate != null){
					ServerClientPackage scp = new ServerClientPackage(Category.REQUEST_CALENDAR_EVENTS, selectedDate);
					hc.sendPackage(scp);
				}
			}
		 });
		 
		 nextButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (currentMonth == 11){
	                currentMonth = 0;
	                currentYear += 1;
	            }
	            else{
	                currentMonth += 1;
	            }
	            refreshCalendar(currentMonth, currentYear);
			}
		 });
		 
		 cmbYear.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if (cmbYear.getSelectedItem() != null){
		             String b = cmbYear.getSelectedItem().toString();
		             currentYear = Integer.parseInt(b);
		             refreshCalendar(currentMonth, currentYear);
		         }
			}
		 });
	 }
	 
	    private class SelectedCellRenderer extends DefaultTableCellRenderer {

	        private static final long serialVersionUID = 1L;

	        @Override
	        public Component getTableCellRendererComponent(
	                JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
	            super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
	            if (hasFocus) {
	                setBackground(Color.PINK);
	            }
	            else {
	                    if (column == 0 || column == 6){ //Week-end
	                        setBackground(Color.LIGHT_GRAY);
	                    }
	                    else{ //Week
	                        setBackground(new Color(255, 255, 255));
	                    }
	                    if (value != null){
	                        if (Integer.parseInt(value.toString()) == realDay && currentMonth == realMonth && currentYear == realYear){ //Today
	                            setBackground(new Color(220, 220, 255));
	                        }
	                    }
	                    setBorder(null);
	                    setForeground(Color.black);
	                    return this;
	            }
	            return this;
	        }
	        
	    }
    
   private Date getDateAt(int row, int col) {
    	Integer day = (Integer) calTable.getValueAt(row, col);
    	String date = currentYear+"/"+(currentMonth+1)+"/"+day;
    	DateFormat df = new SimpleDateFormat("yyyy/MM/dd");
		Date selectedDate = null;
		try {
			selectedDate = df.parse(date);
			return selectedDate;
		} catch (ParseException e) {
			return null;
		}
	}
	//Calendar Panel GUI
    static class tblCalendarRenderer extends DefaultTableCellRenderer{
		private static final long serialVersionUID = -5950349054570803398L;

		public Component getTableCellRendererComponent (JTable table, Object value, boolean selected, boolean focused, int row, int column){
            super.getTableCellRendererComponent(table, value, selected, focused, row, column);
            if (column == 0 || column == 6){ //Week-end
                setBackground(Color.LIGHT_GRAY);
            }
            else{ //Week
                setBackground(new Color(255, 255, 255));
            }
            if (value != null){
                if (Integer.parseInt(value.toString()) == realDay && currentMonth == realMonth && currentYear == realYear){ //Today
                    setBackground(new Color(220, 220, 255));
                }
            }
            setBorder(null);
            setForeground(Color.black);
            return this;
        }
    }
    
    public void setAEP(AddEventPanel aep){
    	this.aep = aep;
    }
    
	public JPanel getcalPanel() {
		return calPanel;
	}

	public Date getSelectedDate() {
		// TODO Auto-generated method stub
		return selectedDate;
	}
}