package server;

import java.awt.Color;
import java.io.Serializable;

public class UserInformation implements Serializable{

	private String email;
	private String fname;
	private String lname;
	private String club;
	private Color prefColor;
	private boolean isAdmin;
	private boolean isActive;
	public UserInformation(String email, String fname, String lname, String club, Color color){
		this.email = email;
		this.fname = fname;
		this.lname = lname;
		this.club = club;
		prefColor = color;
		isActive = false;
		isAdmin = false;
	}	
	public String getUsername(){
		return email;
	}
	public String getFname(){
		return fname;
	}
	public String getLname(){
		return lname;
	}
	public String getClub(){
		return club;
	}
	public Color getColor(){
		return prefColor;
	}
	public void setColor(Color color){
		prefColor = color;
	}
	public boolean isAdmin() {
		return isAdmin;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setAdminTrue() {
		isAdmin = true;
	}
	public void setActive() {
		isActive = true;
	}
}

