package server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerListener extends Thread {
	
	private Server server;
	private ServerSocket ss;
	
	public ServerListener(ServerSocket ss, Server server){
		this.server = server;
		this.ss = ss;
	}
	
	public void run(){
		try{
			while(true){
				Socket s = ss.accept();
				server.appendToLog("Accepted connection: " + s.getInetAddress());
				ServerClientCommunicator scc = new ServerClientCommunicator(s, server);
				server.addThread(scc);
			}
		} catch (IOException ioe){
			System.out.println("ioe : " + ioe.getMessage());
		} finally {
			if(ss!=null){
				try{
					ss.close();
				} catch(IOException ioe){
					ioe.printStackTrace();
				}
			}
		}
	}
}
