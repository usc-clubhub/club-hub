package server;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class ClubRegistrationParser {

	private String clubname;
	private String fname;
	private String lname;
	private String email;
	private String password;
	private String hostname;
	private Server server;
	
	public ClubRegistrationParser(Server server, String hostname, String clubname, String fname, String lname, String email, String password){
		this.hostname = hostname;
		this.clubname = clubname;
		this.fname = fname;
		this.lname = lname;
		this.email = email;
		this.password = password;
		this.server = server;
	}
	
	public boolean CreateDatabase(){
		//Create the database
		String noSpaces = clubname.replaceAll(" ", "_");
		Connection conn = null;
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://" + hostname + "?user=root&password=root");
			String toExecute = RegistrationConstants.MAKE_DATABASE + " " + noSpaces+";";
			PreparedStatement pst = conn.prepareStatement(toExecute);getClass();
			server.appendToLog("Attempting to create club: " + clubname);
			pst.executeUpdate();
			server.appendToLog("Club successfully created");
		} catch (SQLException sqle){
			System.out.println("sqle: "+ sqle.getMessage());
			server.appendToLog("Club creation failed: club "+ "\"" + clubname + "\"" + "already exists");
			return false;
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		
		//Add tables and first member
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://" + hostname + "/" + noSpaces + "?user=root&password=root");
			
			//Create member table
			String toExecute = RegistrationConstants.CREATE_MEMBER_TABLE;
			PreparedStatement pst = conn.prepareStatement(toExecute);
			pst.executeUpdate();
			
			//Create finance table
			toExecute = RegistrationConstants.CREATE_FINANCE_TABLE;
			pst = conn.prepareStatement(toExecute);
			pst.executeUpdate();
			
			//Create posts table
			toExecute = RegistrationConstants.CREATE_POSTS_TABLE;
			pst = conn.prepareStatement(toExecute);
			pst.executeUpdate();
			
			//Create comments table
			toExecute = RegistrationConstants.CREATE_COMMENTS_TABLE;
			pst = conn.prepareStatement(toExecute);
			pst.executeUpdate();
			
			//Create users table
			toExecute = RegistrationConstants.CREATE_USER_TABLE;
			pst = conn.prepareStatement(toExecute);
			pst.executeUpdate();
			
			//Create preferences table
			toExecute = RegistrationConstants.CREATE_USER_PREFERENCES;
			pst = conn.prepareStatement(toExecute);
			pst.executeUpdate();
			
			//Create club description 
			toExecute = RegistrationConstants.CREATE_CLUB_DESCRIPTION;
			pst = conn.prepareStatement(toExecute);
			pst.executeUpdate();
			
			//Create calendar events
			toExecute = RegistrationConstants.CREATE_CALENDAR_EVENTS_TABLE;
			pst = conn.prepareStatement(toExecute);
			pst.executeUpdate();
			
			//Add first member
			toExecute = RegistrationConstants.CREATE_FIRST_MEMBER;
			pst = conn.prepareStatement(toExecute);
			pst.setString(1, fname);
			pst.setString(2, lname);
			pst.setString(3, email);
			pst.executeUpdate();
			
			//Add first user
			toExecute = RegistrationConstants.CREATE_FIRST_USER;
			pst = conn.prepareStatement(toExecute);
			pst.setString(1, email);
			pst.setString(2, fname);
			pst.setString(3, lname);
			pst.setString(4, password);
			pst.executeUpdate();
			
			//Add user preferences
			toExecute = RegistrationConstants.CREATE_FIRST_USER_PREFERENCES;
			pst = conn.prepareStatement(toExecute);
			pst.setString(1, email);
			pst.setString(2, "35 35 35");
			pst.executeUpdate();
			
			//Set default club description
			toExecute = RegistrationConstants.SET_DEFAULT_CLUB_DESCRIPTION;
			pst = conn.prepareStatement(toExecute);
			pst.executeUpdate();
			
		} catch (SQLException sqle){
			System.out.println("sqle: "+ sqle.getMessage());
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		
		//Add the new club to the Club_List database
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection("jdbc:mysql://localhost/ClubList?user=root&password=root");
			PreparedStatement pst = conn.prepareStatement("INSERT INTO Clubs(hostname, club) VALUES (?,?)");
			pst.setString(1, hostname);
			pst.setString(2, noSpaces);
			//server.appendToLog("Attempting to create club: " + club name);
			pst.executeUpdate();
			
		} catch (SQLException sqle){
			System.out.println("sqle: "+ sqle.getMessage());
		} catch (ClassNotFoundException cnfe) {
			cnfe.printStackTrace();
		}
		
		return true;
	}
}
