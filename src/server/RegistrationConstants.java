package server;

public class RegistrationConstants {

	public static String MAKE_DATABASE = "CREATE DATABASE";
	
	
	public static String CREATE_MEMBER_TABLE = "CREATE TABLE Members (memberID int(11) UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,"
			+ " fname VARCHAR(40) NOT NULL, lname VARCHAR(40) NOT NULL, dues_paid BOOL NOT NULL, join_date TIMESTAMP NOT NULL,"
			+ "hasCar BOOL NOT NULL, year VARCHAR(20), isAdmin BOOL NOT NULL, email VARCHAR(50) UNIQUE NOT NULL,"
			+ "phone VARCHAR(25), memType VARCHAR(40));";
	
	public static String CREATE_FINANCE_TABLE = "CREATE TABLE Finances (id INT(10) UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,"
			+ "event VARCHAR(100) NOT NULL, description TEXT, price DECIMAL(9,2) NOT NULL,date TIMESTAMP NOT NULL,vendor VARCHAR(100),buyer VARCHAR(100));";
	
	public static String CREATE_POSTS_TABLE = "CREATE TABLE Posts (id int(10) UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT, title VARCHAR(100) NOT NULL,"
			+ " content TEXT NOT NULL, date TIMESTAMP NOT NULL, isPublic BOOL NOT NULL, author VARCHAR(100) NOT NULL);";
	
	public static String CREATE_COMMENTS_TABLE = "CREATE TABLE Comments (id INT(10) UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,"
			+ "content TEXT NOT NULL, date TIMESTAMP NOT NULL, author VARCHAR(100) NOT NULL, postID INT(10) NOT NULL);";
	
	public static String CREATE_CALENDAR_EVENTS_TABLE = "CREATE TABLE Calendar_Events(id INT(10) PRIMARY KEY NOT NULL AUTO_INCREMENT,"
			+ " eventName VARCHAR(100) NOT NULL, eventDate DATETIME NOT NULL, eventCost DECIMAL(9,2), eventRevenue DECIMAL(9,2));";
	
	public static String CREATE_USER_TABLE = "CREATE TABLE Users(id INT(10) UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT, email VARCHAR(50) UNIQUE NOT NULL, passwd VARCHAR(100) NOT NULL,"
			+ "fname VARCHAR(40) NOT NULL, lname VARCHAR(40) NOT NULL, memType ENUM('admin','active','inactive') NOT NULL, lastLogin TIMESTAMP);";
	
	public static String CREATE_USER_PREFERENCES = "CREATE TABLE User_Preferences(id INT(10) UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,"
			+ "email VARCHAR(50) NOT NULL UNIQUE, colorValues VARCHAR(15) NOT NULL);";
	
	public static String CREATE_CLUB_DESCRIPTION = "CREATE TABLE Club_Description(id INT(1) UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT, "
			+ "description TEXT NOT NULL);";
	
	
	
	public static String CREATE_FIRST_MEMBER = "INSERT INTO Members(fname, lname, email, isAdmin, hasCar, dues_paid, join_date)"
			+ " VALUES(?,?,?, TRUE, FALSE, TRUE, NOW());";
	public static String CREATE_FIRST_USER = "INSERT INTO Users(email, fname, lname, passwd, memType, lastLogin) VALUES (?,?,?,?, 'admin', NOW())";
	public static String CREATE_FIRST_USER_PREFERENCES = "INSERT INTO User_Preferences(email, colorValues) VALUES (?,?)";
	public static String SET_DEFAULT_CLUB_DESCRIPTION = "INSERT INTO Club_Description(description) VALUES ('Default Text')";
}
	

