package server;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.table.DefaultTableModel;

import announcements.Announcement;
import announcements.Comment;
import calendar.CalendarEvent;
import finance.Transaction;
import membership.Member;
import server.ServerClientPackage.Category;

public class ServerClientCommunicator extends Thread{
	
	private Socket s;
	private Server server;
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private UserInformation ui;
	private ClubConnection cc;
	
	public ServerClientCommunicator(Socket s, Server server) throws IOException {
		this.s = s;
		this.server = server;
		this.oos = new ObjectOutputStream(s.getOutputStream());
		this.ois = new ObjectInputStream(s.getInputStream());
		this.start();
	}
	

	public void run(){
		try {
			while(true) {
				ServerClientPackage scp = (ServerClientPackage)ois.readObject();
				//Add all the necessary actions for receiving different package types
				
				if(scp.getCategory() == Category.REQUEST_CLUBS){
					//Connect to database
					String host = (String)scp.getObject();
					String URI = "jdbc:mysql://"+host+"/ClubList?user=root&password=root";
					Connection conn = null;
					ResultSet rs = null;
					Vector<String> clubNames = new Vector<>();
					try {
						Class.forName("com.mysql.jdbc.Driver");
						conn = DriverManager.getConnection(URI);
						PreparedStatement pst = conn.prepareStatement("SELECT * FROM Clubs WHERE hostname=?");
						pst.setString(1, host);
						rs = pst.executeQuery();
						while(rs.next()){
							clubNames.addElement(rs.getString("club").replaceAll("_", " "));
						}
						rs.close();
						pst.close();
						ServerClientPackage query = new ServerClientPackage(Category.CLUB_LIST, clubNames);
						
						oos.writeObject(query);
						oos.flush();
						
						/* GET BG IMG AND SEND IT */
						BufferedImage img = ImageIO.read(new File(Constants.BACKGROUND_IMAGE));
					    ByteArrayOutputStream stream = new ByteArrayOutputStream();
				        ImageIO.write(img, "png", stream);
				        
				        oos.writeObject(new ServerClientPackage(Category.BG_IMG, stream.toByteArray()));
				        oos.flush();
						//Get all clubs
					} catch (SQLException e) {
						e.printStackTrace();
					} finally {
						try{
							if(conn!=null){
								conn.close();
							}
						} catch (SQLException sqle){
							sqle.printStackTrace();
						}
					}	
				}
				
				else if(scp.getCategory() == Category.REQUEST_USER_LIST) {
					Vector<UserInformation> userList = new Vector<UserInformation>();
					ResultSet rs = cc.getUsers();
					server.appendToLog("User attempting to retreive users list");
					if(!rs.isBeforeFirst()){
						ServerClientPackage response = new ServerClientPackage(Category.USER_LIST, null);
						oos.writeObject(response);
						oos.flush();
					}else{
						while(rs.next()){
							String memType = rs.getString("memType");
							String fname = rs.getString("fname");
							String lname = rs.getString("lname");
							String email = rs.getString("email");
							UserInformation ui = new UserInformation(email, fname, lname, null, null);
							if (memType.equals("admin")) {
								ui.setAdminTrue();
							}
							else if (memType.equals("active")) {
								ui.setActive();
							}
							userList.add(ui);
						}
						ServerClientPackage response = new ServerClientPackage(Category.USER_LIST, userList);
						oos.writeObject(response);
						oos.flush();
					}
				}
				
				else if(scp.getCategory() == Category.CLUB_REGISTER){
					Vector<String> fields = (Vector<String>)scp.getObject();
					ClubRegistrationParser csp = new ClubRegistrationParser(server, fields.elementAt(0), fields.elementAt(1), 
							fields.elementAt(2), fields.elementAt(3), fields.elementAt(4), fields.elementAt(5));
					boolean success = csp.CreateDatabase();
					
					//if it failed, notify user
					if(!success){
						ServerClientPackage response = new ServerClientPackage(Category.CLUB_EXISTS, null);
						oos.writeObject(response);
						oos.flush();
					}
					else {
						//Create UserInformation
						UserInformation userInfo;
						cc = server.createClubConnection(fields.get(0),fields.get(1));
						String fname = fields.elementAt(2);
						String lname = fields.elementAt(3);
						String email = fields.elementAt(4);
						Color color = cc.getPreferredColor(email);
						userInfo = new UserInformation(email,fname,lname,fields.get(1),color);
						ui = userInfo;
						userInfo.setAdminTrue();
						server.putThreadIntoTable(this);
						ServerClientPackage response = new ServerClientPackage(Category.CLUB_CREATE_SUCCESS, userInfo);
						server.appendToLog("Logging into " + fields.get(1) + "@" + fields.get(0) + " as: "
								+ email);
						oos.writeObject(response);
						oos.flush();
					}
				}
				
				else if(scp.getCategory() == Category.LOGIN){
					//Verify credentials
					ArrayList<String> fields = (ArrayList<String>)scp.getObject();
					String URI = "jdbc:mysql://"+fields.get(0)+"/"+ fields.get(1)+ "?user=root&password=root";
					Connection conn = null;
					ResultSet rs = null;
					Vector<String> clubNames = new Vector<>();
					ServerClientPackage response;
					try {
						Class.forName("com.mysql.jdbc.Driver");
						conn = DriverManager.getConnection(URI);
						PreparedStatement pst = conn.prepareStatement("SELECT * FROM Users WHERE email=? AND passwd=? AND (memType='admin' OR memType='active')");
						pst.setString(1, fields.get(2));
						pst.setString(2, fields.get(3));
						server.appendToLog("User attempting to log in to "+ fields.get(1)+"@"+fields.get(0)
								+" with username: "+fields.get(2)+" and password: "+fields.get(3));
						rs = pst.executeQuery();
						if (!rs.isBeforeFirst() ) {    
							//Login invalid
							response = new ServerClientPackage(Category.LOGIN_FAILURE, null);
							oos.writeObject(response);
							oos.flush();
							server.appendToLog("Login failed");
						} else {
							//If valid, instantiate ClubConnection
							server.appendToLog("Login succeeded");
							cc = server.createClubConnection(fields.get(0),fields.get(1));
							
							//Get user information
							Color prefColor = cc.getPreferredColor(fields.get(2));
							rs.next();
							UserInformation uinfo = new UserInformation(fields.get(2),rs.getString("fname"), rs.getString("lname"),
									fields.get(1),prefColor);
							
							//Set uninfo admin to true if user is an administrator
							String memType = rs.getString("memType");
							//System.out.println(memType);
							if (memType.equals("admin")) {
								uinfo.setAdminTrue();
							}
							ui = uinfo;
							response = new ServerClientPackage(Category.LOGIN_SUCCESS, uinfo);
							server.putThreadIntoTable(this);
							//and send confirmation to user to move to next frame
							oos.writeObject(response);
							oos.flush();
						}
						rs.close();
						pst.close();
					} catch (SQLException e) {
						e.printStackTrace();
					} finally {
						try{
							if(conn!=null){
								conn.close();
							}
						} catch (SQLException sqle){
							sqle.printStackTrace();
						}
					}	
				}
				else if(scp.getCategory() == Category.UPDATE_COLOR){
					UserInformation ui = (UserInformation)scp.getObject();
					Color userColor = ui.getColor();
					String RGB = "" + userColor.getRed() + " " + userColor.getGreen() +
							" " + userColor.getBlue();
					cc.updateUserColor(ui.getUsername(),RGB);
				}
				else if(scp.getCategory() == Category.GET_MEMBERS){
					ResultSet rs = cc.getMembers();
					Object[][] rowData = {};
					Object[] columnNames = {"First Name", "Last Name", "Email", "Phone", "Year", "Has Car", "Member Type", "Dues Paid", "Admin"};;
					DefaultTableModel listTableModel = new DefaultTableModel(rowData, columnNames);
					while(rs.next()) {
						String fname  = rs.getString("fname");
						String lname  = rs.getString("lname");
						String dues_paid;
						if (rs.getInt("dues_paid") == 0) {
							dues_paid = "No";
						}
						else {
							dues_paid = "Yes";
						}
						String has_car;
						if (rs.getInt("hasCar") == 0) {
							has_car = "No";
						}
						else {
							has_car = "Yes";
						}
						String year = rs.getString("year");
						String phone = rs.getString("phone");
						String memType = rs.getString("memType");
						String isAdmin;
						if (rs.getInt("isAdmin") == 0) {
							isAdmin = "No";
						}
						else {
							isAdmin = "Yes";
						}
						String email = rs.getString("email");
					    listTableModel.addRow(new Object[]{fname, lname, email, phone, year, has_car, memType, dues_paid, isAdmin});
				    }
					ServerClientPackage returnMem = new ServerClientPackage(Category.RETURN_MEMBERS, listTableModel);
					oos.writeObject(returnMem);
					oos.flush();
				}
				else if(scp.getCategory() == Category.ADD_MEMBER){
					Member m = (Member)scp.getObject();
					cc.addMember(m);
				}
				else if (scp.getCategory() == Category.DELETE_MEMBER) {
					Member m = (Member)scp.getObject();
					cc.deleteMember(m);
				}
				
				//FINANCE DATA COMMUNICATION
				else if(scp.getCategory() == Category.GET_TRANSACTIONS){
					ResultSet rs = cc.getFinances();
					Object[][] rowData = {};
					Object[] columnNames = {"ID", "Event", "Description", "Price", "Date", "Vendor", "Buyer"};
					DefaultTableModel tableModel = new DefaultTableModel(rowData, columnNames);
					while(rs.next()) {
						int id = rs.getInt("id");
						String event  = rs.getString("event");
						String description = rs.getString("description");
						double price = rs.getDouble("price");
						String date = new SimpleDateFormat("MM/dd/yyyy").format(rs.getTimestamp("date"));
						String vendor = rs.getString("vendor");
						String buyer = rs.getString("buyer");
					    tableModel.addRow(new Object[]{id, event, description, price, date, vendor, buyer});
				    }
					ServerClientPackage returnTransactions = new ServerClientPackage(Category.RETURN_TRANSACTIONS, tableModel);
					oos.writeObject(returnTransactions);
					oos.flush();
				}
				else if(scp.getCategory() == Category.ADD_TRANSACTION){
					Transaction m = (Transaction)scp.getObject();
					cc.addTransaction(m);
					
					ResultSet rs = cc.getNewTransaction();
					Transaction t = null;
					if(rs.next()){
						t = new Transaction(	rs.getString("event"), 
								rs.getString("description"), 
								rs.getDouble("price"), 
								rs.getString("vendor"), 
								rs.getString("buyer"));
						t.id = rs.getInt("id");
						System.out.println("new transaction: "+t.id+" "+t.event+" "+t.description+" "+t.price);
					}					
					
					
					ServerClientPackage updateTransactions = new ServerClientPackage(Category.UPDATE_TRANSACTION, t);
					oos.writeObject(updateTransactions);
					oos.flush();
					
				}
				else if (scp.getCategory() == Category.DELETE_TRANSACTION) {
					int tID = (int)scp.getObject();
					cc.deleteTransaction(tID);
				}
				
				else if(scp.getCategory() == Category.REQUEST_ANNOUNCEMENTS){
					ResultSet rs = cc.getPosts();
					ArrayList<Announcement> announcements = new ArrayList<>();
					if(!rs.isBeforeFirst()){
						ServerClientPackage response = new ServerClientPackage(Category.NO_ANNOUNCEMENTS, null);
						oos.writeObject(response);
						oos.flush();
					}else{		
						while(rs.next()){
							String title = rs.getString("title");
							Timestamp stamp = rs.getTimestamp("date");
							Date date = new Date(stamp.getTime());
							DateFormat df = new SimpleDateFormat("MMM dd, YYYY 'at' HH:mm");
							String formattedDate = df.format(date);
							boolean isPublic = rs.getBoolean("isPublic");
							String author = rs.getString("author");
							String content = rs.getString("content");
							int id = rs.getInt("id");
							Announcement a = new Announcement(content, title, formattedDate, author, isPublic, id);
							announcements.add(a);
						}
						ServerClientPackage response = new ServerClientPackage(Category.FETCH_ANNOUNCEMENTS, announcements);
						oos.writeObject(response);
						oos.flush();
					}
				}
				else if(scp.getCategory() == Category.NEW_POST){
					Announcement a = (Announcement)scp.getObject();
					cc.storeNewPost(a);
					server.appendToLog(a.getAuthor() + " posted in " + cc.getClub().replaceAll("_", " ")
							+ "@"+cc.getServer());
					server.sendNewPostNotification(ui.getClub());
				}
				else if(scp.getCategory() == Category.DELETE_POST){
					Announcement a = (Announcement)scp.getObject();
					cc.deletePost(a);
					server.appendToLog("Deleted post by " + a.getAuthor());
				}
				else if(scp.getCategory() == Category.SIGN_UP_REQUEST){
					//Verify credentials
					Vector<String> fields = (Vector<String>)scp.getObject();
					String URI = "jdbc:mysql://"+fields.get(0)+"/"+ fields.get(1)+ "?user=root&password=root";
					Connection conn = null;
					ResultSet rs = null;
					server.appendToLog("User: " + fields.get(4) + " requesting access to: " + fields.get(1)+
							"@" + fields.get(0));
					ServerClientPackage response = null;
					try {
						Class.forName("com.mysql.jdbc.Driver");
						conn = DriverManager.getConnection(URI);
						PreparedStatement pst = conn.prepareStatement("SELECT * FROM Users WHERE email=?");
						pst.setString(1, fields.get(4));
						rs = pst.executeQuery();
						if (rs.isBeforeFirst() ) {    
							//Username is already in use
							rs.next();
							boolean active = rs.getBoolean("active");
							if(!active){
								server.appendToLog("Error: access already requested by user: " + fields.get(4));
								response = new ServerClientPackage(Category.ALREADY_REQUESTED, null);
							} else {
								server.appendToLog("Error: User " + fields.get(4) + " already exists and is active");
								response = new ServerClientPackage(Category.USER_EXISTS, null);
							}
							oos.writeObject(response);
							oos.flush();
						} else {
							//If valid, instantiate ClubConnection
							server.appendToLog("Access requested for user " + fields.get(4));
							PreparedStatement toInsert = conn.prepareStatement("INSERT INTO Users (email, fname, lname, passwd, memType) VALUES (?,?,?,?, 'inactive')");
							toInsert.setString(1, fields.get(4));
							toInsert.setString(2, fields.get(2));
							toInsert.setString(3, fields.get(3));
							toInsert.setString(4, fields.get(5));
							toInsert.execute();
							response = new ServerClientPackage(Category.SIGN_UP_SUCCESS, null);
							//and send confirmation to user to move to next frame
							oos.writeObject(response);
							oos.flush();
						}
						rs.close();
						pst.close();
					} catch (SQLException e) {
						e.printStackTrace();
					} finally {
						try{
							if(conn!=null){
								conn.close();
							}
						} catch (SQLException sqle){
							sqle.printStackTrace();
						}
					}	
				}
				else if(scp.getCategory() == Category.TIME_UPDATE){
					String username = (String)scp.getObject();
					if(!username.equals("Guest")){
						cc.updateLastLoginTime((String)scp.getObject());
						server.appendToLog("Updating last active time");
					}
					
				}
				else if(scp.getCategory() == Category.DELETE_USER) {
					Vector<UserInformation> userList = new Vector<UserInformation>();
					server.appendToLog("Administrator deleting user");
					UserInformation deleteUserInfo = (UserInformation)scp.getObject();
					ResultSet rs = cc.deleteUser(deleteUserInfo);
					if(!rs.isBeforeFirst()){
						ServerClientPackage response = new ServerClientPackage(Category.USER_LIST, null);
						oos.writeObject(response);
						oos.flush();
					}else{
						while(rs.next()){
							String memType = rs.getString("memType");
							String fname = rs.getString("fname");
							String lname = rs.getString("lname");
							String email = rs.getString("email");
							UserInformation ui = new UserInformation(email, fname, lname, null, null);
							if (memType.equals("admin")) {
								ui.setAdminTrue();
							}
							else if (memType.equals("active")) {
								ui.setActive();
							}
							userList.add(ui);
						}
						ServerClientPackage response = new ServerClientPackage(Category.USER_LIST, userList);
						oos.writeObject(response);
						oos.flush();
					}
				}
				else if(scp.getCategory() == Category.ACTIVATE_USER) {
					Vector<UserInformation> userList = new Vector<UserInformation>();
					server.appendToLog("Administrator activating user");
					UserInformation activateUserInfo = (UserInformation)scp.getObject();
					ResultSet rs = cc.activateUser(activateUserInfo);
					if(!rs.isBeforeFirst()){
						ServerClientPackage response = new ServerClientPackage(Category.USER_LIST, null);
						oos.writeObject(response);
						oos.flush();
					}else{
						while(rs.next()){
							String memType = rs.getString("memType");
							String fname = rs.getString("fname");
							String lname = rs.getString("lname");
							String email = rs.getString("email");
							UserInformation ui = new UserInformation(email, fname, lname, null, null);
							if (memType.equals("admin")) {
								ui.setAdminTrue();
							}
							else if (memType.equals("active")) {
								ui.setActive();
							}
							userList.add(ui);
						}
						ServerClientPackage response = new ServerClientPackage(Category.USER_LIST, userList);
						oos.writeObject(response);
						oos.flush();
					}
				}
				else if(scp.getCategory() == Category.GET_CLUB_DESCRIPTION) {
					String clubDesc = cc.getClubDescription();
					oos.writeObject(new ServerClientPackage(Category.SEND_CLUB_DESCRIPTION, clubDesc));
					oos.flush();
				}
				else if(scp.getCategory() == Category.SET_CLUB_DESCRIPTION){
					String clubDesc = (String)scp.getObject();
					cc.setClubDescription(clubDesc);
					server.appendToLog(ui.getUsername()+ " changed club description for " + ui.getClub());
				}
				else if(scp.getCategory() == Category.REQUEST_CALENDAR_EVENTS){
					java.util.Date date = (java.util.Date)scp.getObject();
					ArrayList<CalendarEvent> events = cc.getCalendarEvents(date);
					oos.writeObject(new ServerClientPackage(Category.SEND_CALENDAR_EVENTS, events));
					oos.flush();
				}
				else if(scp.getCategory() == Category.FETCH_COMMENTS) {
					Announcement a = (Announcement)scp.getObject();
					ArrayList<Comment> commentList;
					commentList = cc.getComments(a.getID());
					ArrayList<Object> pair = new ArrayList<Object>();
					pair.add(commentList);
					pair.add(a.getID());
					ServerClientPackage response = new ServerClientPackage(Category.COMMENT_LIST, pair);
					oos.writeObject(response);
					oos.flush();
				}
				else if (scp.getCategory() == Category.NEW_COMMENT) {
					Comment c = (Comment)scp.getObject();
					cc.storeNewComment(c);
				}
				else if (scp.getCategory() == Category.DELETE_COMMENT) {
					Comment c = (Comment)scp.getObject();
					ArrayList<Comment> commentList;
					ArrayList<Object> pair = new ArrayList<Object>();
					commentList = cc.deleteComment(c);
					pair.add(commentList);
					pair.add(c.getID());
					ServerClientPackage response = new ServerClientPackage(Category.COMMENT_LIST, pair);
					oos.writeObject(response);
					oos.flush();
				}
				else if (scp.getCategory() == Category.GUEST_LOGIN){
					ArrayList<String> serverAndClub = (ArrayList<String>)scp.getObject();
					cc = server.createClubConnection(serverAndClub.get(0), serverAndClub.get(1));
					ServerClientPackage response = new ServerClientPackage(Category.GUEST_LOGIN_SUCCESS, null);
					ui = new UserInformation("Guest", "Guest", "Guest", serverAndClub.get(1), new Color(35,35,35));
					server.putThreadIntoTable(this);
					oos.writeObject(response);
					oos.flush();
				}
				else if (scp.getCategory() == Category.ADD_CALENDAR_EVENT){
					CalendarEvent ce = (CalendarEvent)scp.getObject();
					cc.addCalendarEvent(ce);
					ArrayList<CalendarEvent> events = cc.getCalendarEvents(ce.getEventDate());
					server.appendToLog(ui.getFname()+ " "+ ui.getLname() + " created an event"
							+ " in "+ ui.getClub());
					oos.writeObject(new ServerClientPackage(Category.SEND_CALENDAR_EVENTS, events));
					oos.flush();
				}
				else if (scp.getCategory() == Category.DELETE_CALENDAR_EVENT){
					CalendarEvent ce = (CalendarEvent)scp.getObject();
					cc.deleteCalendarEvent(ce);
					ArrayList<CalendarEvent> events = cc.getCalendarEvents(ce.getEventDate());
					server.appendToLog(ui.getFname()+ " "+ ui.getLname() + " deleted an event"
							+ " from "+ ui.getClub());
					oos.writeObject(new ServerClientPackage(Category.SEND_CALENDAR_EVENTS, events));
					oos.flush();
				}
				else if (scp.getCategory() == Category.EDIT_CALENDAR_EVENT){
					Vector<CalendarEvent> eventToUpdate = (Vector<CalendarEvent>)scp.getObject();
					cc.editCalendarEvent(eventToUpdate.get(0), eventToUpdate.get(1));
					ArrayList<CalendarEvent> events = cc.getCalendarEvents(eventToUpdate.get(0).getEventDate());
					server.appendToLog(ui.getFname()+ " "+ ui.getLname() + " edited an event"
							+ " in "+ ui.getClub());
					oos.writeObject(new ServerClientPackage(Category.SEND_CALENDAR_EVENTS, events));
					oos.flush();
				}
				
			}
		} catch (ClassNotFoundException cnfe) {
			System.out.println("cnfe in run: " + cnfe.getMessage());
		} catch (IOException ioe) {
			System.out.println("ioe in run: " + ioe.getMessage());
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public UserInformation getUserInformation() {
		return ui;
	}
	
	public void sendPackage(ServerClientPackage pckg) {
		try {
			oos.writeObject(pckg);
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
