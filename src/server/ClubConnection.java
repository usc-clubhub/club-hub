package server;

import java.awt.Color;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import announcements.Announcement;
import announcements.Comment;
import calendar.CalendarEvent;
import finance.Transaction;
import membership.Member;

public class ClubConnection {
	
	private Connection conn;
	private String URI;
	private String server, club;
	public ClubConnection(String server, String club)
	{
		URI = "jdbc:mysql://"+server+"/"+club.replaceAll(" ", "_")+"?user=root&password=root";
		conn = null;
		this.server = server;
		this.club = club;
	}
	
	private Connection connect() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver");
		return DriverManager.getConnection(URI);
	}
	public String getServer(){
		return server;
	}
	
	public String getClub(){
		return club;
	}
	public ResultSet getUsers() throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		conn = connect();
		Statement st = conn.createStatement();
		rs = st.executeQuery("SELECT * FROM Users ORDER BY memType DESC");
		return rs;
	}
	public ResultSet getMembers() throws ClassNotFoundException, SQLException{
		ResultSet rs = null;
		conn = connect();
		Statement st = conn.createStatement();
		rs =  st.executeQuery("SELECT * FROM Members");
		return rs;
	}
	
	public void addMember(Member m) throws ClassNotFoundException, SQLException{
		final String addMember = "INSERT INTO Members(memberID, fname, lname, dues_paid, join_date, hasCar, year, isAdmin, email, phone, memType) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
		try {
			PreparedStatement ps = conn.prepareStatement(addMember);
			ps.setInt(1, 0);
			ps.setString(2, m.getFirstName());
			ps.setString(3, m.getLastName());
			if (m.getDues().equals("Yes")) {
				ps.setInt(4, 1);				
			}
			else {
				ps.setInt(4, 0);				
			}
	         //Date object
			 Date date= new Date();
			 long time = date.getTime();
			 Timestamp ts = new Timestamp(time);
			 ps.setTimestamp(5, ts);
			 //set has car
			if (m.getCar().equals("Yes")) {
				ps.setInt(6, 1);				
			}
			else {
				ps.setInt(6, 0);				
			}
			ps.setString(7, m.getYear());
			ps.setInt(8, 0);
			ps.setString(9, m.getEmail());
			ps.setString(10, m.getPhone());
			ps.setString(11, m.getMemType());
			ps.executeUpdate();
			
		} catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void deleteMember(Member m) throws ClassNotFoundException, SQLException {
		// Delete from users and user preferneces tables
		PreparedStatement pst = conn.prepareStatement("DELETE FROM MEMBERS WHERE email=?");
		pst.setString(1, m.getEmail());
		pst.execute();
	}
	
	//FINANCE DATA MANIPULATION
	public ResultSet getFinances() throws ClassNotFoundException, SQLException {
		ResultSet rs = null;
		conn = connect();
		Statement st = conn.createStatement();
		rs =  st.executeQuery("SELECT * FROM Finances");
		return rs;
	}
	
	public ResultSet getNewTransaction() throws ClassNotFoundException, SQLException{
		ResultSet rs = null;
		conn = connect();
		Statement st = conn.createStatement();
		rs = st.executeQuery("SELECT * FROM Finances ORDER BY id DESC");
		return rs;
	}
	
	public void addTransaction(Transaction t) throws ClassNotFoundException, SQLException{
		//System.out.println("Adding transaction to database");
		final String addTransaction = "INSERT INTO Finances(id, event, description, price, date, vendor, buyer) VALUES (?,?,?,?,?,?,?)";
		try {
			conn = connect();
			PreparedStatement ps = conn.prepareStatement(addTransaction);
			ps.setInt(1, 0);
			ps.setString(2, t.event);
			ps.setString(3, t.description);
			ps.setDouble(4, t.price);
			ps.setTimestamp(5, t.date);
			ps.setString(6, t.vendor);
			ps.setString(7, t.buyer);
			ps.executeUpdate();
			ps.close();
			
		} catch(SQLException sqle) {
			System.out.println("Adding transaction to server database failed.\n"+sqle.getMessage());
		} finally{
			if (conn != null){
				try{
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public void deleteTransaction(int tID) throws ClassNotFoundException, SQLException {
		// Delete from users and user preferneces tables
		PreparedStatement pst = conn.prepareStatement("DELETE FROM Finances WHERE ID=?");
		pst.setInt(1, tID);
		pst.execute();
	}
	
	public ResultSet getPosts() throws ClassNotFoundException, SQLException{
		ResultSet rs = null;
		conn = connect();
		Statement st = conn.createStatement();
		rs =  st.executeQuery("SELECT * FROM Posts ORDER BY date DESC");
		return rs;
	}
	
	public void close() throws SQLException{
		conn.close();
	}
	public Color getPreferredColor(String user) throws ClassNotFoundException, SQLException{
		ResultSet rs = null;
		conn = connect();
		PreparedStatement pst = conn.prepareStatement("SELECT * FROM User_Preferences WHERE email= ?");
		pst.setString(1, user);
		rs = pst.executeQuery();
		rs.next();
		String RGB = rs.getString("colorValues");
		String[] RGBValues = RGB.split("\\s+");
		int R = Integer.parseInt(RGBValues[0]);
		int G = Integer.parseInt(RGBValues[1]);
		int B = Integer.parseInt(RGBValues[2]);
		return new Color(R,G,B);
	}
	public ResultSet deleteUser(UserInformation deleteUser) throws SQLException, ClassNotFoundException{
		// Delete from users and user preferneces tables
		PreparedStatement pst = conn.prepareStatement("DELETE FROM Users WHERE email=?");
		pst.setString(1, deleteUser.getUsername());
		pst.execute();
		pst = conn.prepareStatement("DELETE FROM User_Preferences WHERE email=?");
		pst.setString(1, deleteUser.getUsername());
		// Return result set of list of users
		return getUsers();
	}
	public ResultSet activateUser(UserInformation activateUser) throws SQLException, ClassNotFoundException {
		System.out.println(activateUser.getUsername());
		System.out.println("activateUser");
		PreparedStatement pst = conn.prepareStatement("UPDATE Users SET memType='active' WHERE email=?");
		pst.setString(1, activateUser.getUsername());
		pst.execute();
		pst = conn.prepareStatement("INSERT INTO User_Preferences(email, colorValues) VALUES (?,?)");
		pst.setString(1, activateUser.getUsername());
		pst.setString(2, "35 35 35");
		pst.execute();
		return getUsers();
	}
	public void storeNewPost(Announcement a){
		try {
			conn = connect();
			PreparedStatement pst = conn.prepareStatement("INSERT INTO Posts (title, content, author, isPublic, date) VALUES (?,?,?,?, NOW())");
			pst.setString(1, a.getAnnouncementTitle());
			pst.setString(2, a.getAnnouncementText());
			pst.setString(3, a.getAuthor());
			pst.setBoolean(4, a.isPublic());
			pst.executeUpdate();
			pst.close();
		} catch (ClassNotFoundException e1) {
			e1.printStackTrace();
		} catch (SQLException e1) {
			e1.printStackTrace();
		} finally {
			if (conn != null){
				try{
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public void storeNewComment(Comment c){
		try{
			conn = connect();
			PreparedStatement pst = conn.prepareStatement("INSERT INTO Comments(postID, content, author, date) VALUES (?,?,?,NOW())");
			pst.setInt(1, c.getID());
			pst.setString(2, c.getContent());
			pst.setString(3, c.getAuthor());
			pst.executeUpdate();
			pst.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null){
				try{
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	public ArrayList<Comment> getComments(int postID){
		try {
			conn = connect();
			PreparedStatement pst = conn.prepareStatement("SELECT * FROM Comments WHERE postID=? ORDER BY date ASC");
			pst.setInt(1, postID);
			ResultSet rs = pst.executeQuery();
			ArrayList<Comment> comments = new ArrayList<Comment>();
			if(!rs.isBeforeFirst()){ 
				//returns an empty array if there were no comments
				return comments;
			} else {
				while(rs.next()){
					Timestamp stamp = rs.getTimestamp("date");
					Date date = new Date(stamp.getTime());
					DateFormat df = new SimpleDateFormat("MMM dd, YYYY 'at' HH:mm");
					String formattedDate = df.format(date);
					int commentID = rs.getInt("id");
					Comment c = new Comment(rs.getString("author"), rs.getString("content"),
							formattedDate, rs.getInt("postID"));
					c.setCommentID(commentID);
					comments.add(c);
					
				}
			}
			return comments;
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null){
				try{
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	
	public void deletePost(Announcement a){
		try {
			conn = connect();
			PreparedStatement pst = conn.prepareStatement("DELETE FROM Posts WHERE title=? AND content=? AND author=?");
			pst.setString(1, a.getAnnouncementTitle());
			pst.setString(2, a.getAnnouncementText());
			pst.setString(3, a.getAuthor());
			pst.executeUpdate();
			pst = conn.prepareStatement("DELETE FROM Comments WHERE postID=?");
			pst.setInt(1, a.getID());
			pst.executeUpdate();
			pst.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null){
				try{
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	public ArrayList<Comment> deleteComment(Comment c) {
		try {
			conn = connect();
			PreparedStatement pst = conn.prepareStatement("DELETE FROM Comments WHERE id=?");
			pst.setInt(1, c.getCommentID());
			pst.executeUpdate();
			pst.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return getComments(c.getID());

	}

	public void updateUserColor(String username, String RGB) {
		try{
			conn = connect();
			PreparedStatement pst = conn.prepareStatement("UPDATE User_Preferences SET colorValues = ? WHERE email=?");
			pst.setString(1, RGB);
			pst.setString(2, username);
			pst.executeUpdate();
			pst.close();
		} catch (ClassNotFoundException | SQLException e){
			e.printStackTrace();
		} finally {
			if (conn != null){
				try{
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public boolean updateLastLoginTime(String user) {
		try{
			conn = connect();
			PreparedStatement pst = conn.prepareStatement("UPDATE Users SET lastLogin= NOW() WHERE email=?");
			pst.setString(1, user);
			pst.executeUpdate();
			pst.close();
			return true;
		} catch (ClassNotFoundException | SQLException e){
			System.out.println("Guest login, no update");
			return false;
		} finally {
			if (conn != null){
				try{
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

	public String getClubDescription() {
		try {
			conn = connect();
			PreparedStatement pst = conn.prepareStatement("SELECT * FROM Club_Description WHERE id=1");
			ResultSet rs = pst.executeQuery();
			rs.next();
			String clubDesc = rs.getString("description");
			rs.close();
			pst.close();
			return clubDesc;
			
		} catch (ClassNotFoundException | SQLException e){
			e.printStackTrace();
			return null;
		} finally {
			if (conn != null){
				try{
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}

	public void setClubDescription(String clubDesc) {
		try{
			conn = connect();
			PreparedStatement pst = conn.prepareStatement("UPDATE Club_Description SET description=? WHERE id=1");
			pst.setString(1, clubDesc);
			pst.executeUpdate();
			pst.close();
		} catch (ClassNotFoundException | SQLException e){
			e.printStackTrace();
		} finally {
			if (conn != null){
				try{
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	public ArrayList<CalendarEvent> getCalendarEvents(Date date){
		ArrayList<CalendarEvent> events = null;
		try{
			conn = connect();
			PreparedStatement pst = conn.prepareStatement("SELECT * FROM Calendar_Events WHERE eventDate=?");
			//pst.setDate(1, date);
			pst.setDate(1, new java.sql.Date(date.getTime()));
			ResultSet rs = pst.executeQuery();
			events = new ArrayList<>();
			if(rs.isBeforeFirst()){
				while(rs.next()){
					CalendarEvent ce = new CalendarEvent(rs.getString("eventName"), rs.getDate("eventDate"));
					//CalendarEvent ce = new CalendarEvent(rs.getString("eventName"));
					events.add(ce);
				}
			}
		} catch (ClassNotFoundException | SQLException e){
			e.printStackTrace();
		} finally {
			if (conn != null){
				try{
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return events;
	}

	public void addCalendarEvent(CalendarEvent ce) {
		try{
			conn = connect();
			PreparedStatement pst = conn.prepareStatement("INSERT INTO Calendar_Events(eventName, eventDate) VALUES(?,?)");
			pst.setString(1, ce.getEventName());
			pst.setDate(2, new java.sql.Date(ce.getEventDate().getTime()));
			pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e){
			e.printStackTrace();
		} finally {
			if (conn != null){
				try{
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
	}
	public void deleteCalendarEvent(CalendarEvent ce){
		try{
			conn = connect();
			PreparedStatement pst = conn.prepareStatement("DELETE FROM Calendar_Events WHERE eventName=? AND eventDate=?");
			pst.setString(1, ce.getEventName());
			pst.setDate(2, new java.sql.Date(ce.getEventDate().getTime()));
			pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e){
			e.printStackTrace();
		} finally {
			if (conn != null){
				try{
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public void editCalendarEvent(CalendarEvent ce1, CalendarEvent ce2) {
		try{
			conn = connect();
			PreparedStatement pst = conn.prepareStatement("UPDATE Calendar_Events SET eventName=?, eventDate=? WHERE"
					+ " eventName=? AND eventDate=?");
			pst.setString(1, ce2.getEventName());
			pst.setDate(2, new java.sql.Date(ce2.getEventDate().getTime()));
			pst.setString(3, ce1.getEventName());
			pst.setDate(4, new java.sql.Date(ce1.getEventDate().getTime()));
			pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e){
			e.printStackTrace();
		} finally {
			if (conn != null){
				try{
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}
}

