package server;

import java.io.Serializable;

public class ServerClientPackage implements Serializable {
	
	private static final long serialVersionUID = 6672059723469L;
	private Category cat;
	private Object ob;
	public enum Category {
		REQUEST_CLUBS,
		CLUB_LIST,
		//Login
		LOGIN,
		LOGIN_SUCCESS,
		LOGIN_FAILURE,
		HEARTBEAT,
		CLUB_REGISTER,
		CLUB_EXISTS,
		CLUB_CREATE_SUCCESS,
		//Members
		GET_MEMBERS,
		ADD_MEMBER,
		DELETE_MEMBER,
		RETURN_MEMBERS,
		//Finance
		GET_TRANSACTIONS,
		ADD_TRANSACTION,
		DELETE_TRANSACTION,
		RETURN_TRANSACTIONS,
		UPDATE_TRANSACTION,
		//Announcements
		REQUEST_ANNOUNCEMENTS,
		FETCH_ANNOUNCEMENTS,
		NO_ANNOUNCEMENTS,
		NEW_POST,
		SIGN_UP_REQUEST, 
		ALREADY_REQUESTED, 
		USER_EXISTS, 
		SIGN_UP_SUCCESS, 
		DELETE_POST, 
		UPDATE_COLOR, 
		TIME_UPDATE,
		REQUEST_USER_LIST,
		USER_LIST,
		DELETE_USER,
		ACTIVATE_USER,
		BG_IMG, 
		GET_CLUB_DESCRIPTION, 
		SEND_CLUB_DESCRIPTION, 
		FETCH_COMMENTS,
		COMMENT_LIST,
		NEW_COMMENT,
		DELETE_COMMENT,
		//Calendar Events
		REQUEST_CALENDAR_EVENTS,
		SEND_CALENDAR_EVENTS,
		//Events
		ADD_CALENDAR_EVENT,
		DELETE_CALENDAR_EVENT,
		SET_CLUB_DESCRIPTION, 
		GUEST_LOGIN, 
		GUEST_LOGIN_SUCCESS, 
		EDIT_CALENDAR_EVENT
	}
	
	public ServerClientPackage(Category cat, Object ob){
		this.cat = cat;
		this.ob = ob;
	}
	
	public Object getObject(){
		return ob;
	}
	public Category getCategory(){
		return cat;
	}
}
