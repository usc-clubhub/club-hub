CREATE DATABASE
USE
CREATE TABLE Members (memberID int(11) UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT, fname VARCHAR(40) NOT NULL, lname VARCHAR(40) NOT NULL, dues_paid BOOL NOT NULL, join_date TIMESTAMP NOT NULL);
CREATE TABLE Finances (id int(10) UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,reason VARCHAR(100) NOT NULL,price DECIMAL(9,2) NOT NULL,date TIMESTAMP NOT NULL,vendor VARCHAR(100),buyer VARCHAR(100));
CREATE TABLE Posts (id int(10) UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT, content TEXT NOT NULL, date TIMESTAMP NOT NULL, author INT(11) NOT NULL,FOREIGN KEY fk(author) REFERENCES Members(memberID));


