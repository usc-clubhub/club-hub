package server;

import java.awt.Dimension;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Hashtable;
import java.util.Scanner;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import server.ServerClientPackage.Category;

public class Server extends JFrame {
	
	
	private static final long serialVersionUID = 1L;
	private Vector<ServerClientCommunicator> sccVec;
	private ServerSocket ss;
	private JTextArea serverLog;
	private JScrollPane jsp;
	private int port;
	private ServerListener serverListener;
	private ClubConnection cc;
	private Hashtable<String, Vector<ServerClientCommunicator>> sccTable;
	
	public Server(){
		sccTable = new Hashtable<String, Vector<ServerClientCommunicator>>();
		Scanner scanner = null;
		sccVec = new Vector<>();
		try {
			scanner = new Scanner(new File("src/server/serverConfig.txt"));
			port = scanner.nextInt();
		} catch (FileNotFoundException e) {
			System.out.println("fnfe: "+e.getMessage());
		}
		setTitle("Server");
		setSize(450,300);
		setMinimumSize(new Dimension(350,300));
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		createGUI();
		setVisible(true);
		try {
			ss = new ServerSocket(port);
		} catch (IOException e) {
			System.out.println("ioe: "+ e.getMessage());;
		}
		serverListener = new ServerListener(ss, this);
		serverListener.start();
		appendToLog("Server started listening on port: "+port);
	}
	
	private void createGUI(){
		//Instantiate components
		serverLog = new JTextArea();
		//serverLog.setLineWrap(wrap);
		jsp = new JScrollPane(serverLog);
		jsp.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		serverLog.setEditable(false);
		setTitle("Server Log");
		jsp.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {  
	        public void adjustmentValueChanged(AdjustmentEvent e) {  
	            e.getAdjustable().setValue(e.getAdjustable().getMaximum());  
	        }
	    });
		this.add(jsp);
		
	}
	public static void main(String[] args){
		
		@SuppressWarnings("unused")
		Server ClubHubServer = new Server();
	}
	public void addThread(ServerClientCommunicator scc){
		sccVec.add(scc);
	}
	public void appendToLog(String text){
		java.util.Date date = new java.util.Date();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd '@' HH:mm:ss");
		String formattedDate = df.format(date);
		serverLog.append(formattedDate+" - " + text +"\n");
	}
	public ClubConnection createClubConnection(String host, String club){
		ClubConnection cc = new ClubConnection(host, club);
		return cc;
	}
	public ClubConnection getClubConnect(){
		return cc;
	}
	public void putThreadIntoTable(ServerClientCommunicator scc) {
		UserInformation uinfo = scc.getUserInformation();
		Vector<ServerClientCommunicator> sccList = null;
		if (sccTable.containsKey(uinfo.getClub())) {
			sccList = sccTable.get(uinfo.getClub());
		} else {
			sccList = new Vector<ServerClientCommunicator>();
		}
		sccList.add(scc);
		sccTable.put(uinfo.getClub(), sccList);
	}
	public void removeThreadFromTable(ServerClientCommunicator scc) {
		
	}

	public void sendNewPostNotification(String clubName) {
		// Send to all clubs
		Vector<ServerClientCommunicator> sccList = sccTable.get(clubName);
		for (ServerClientCommunicator scc : sccList) {
			scc.sendPackage(new ServerClientPackage(Category.NEW_POST, null));
		}
	}
}
