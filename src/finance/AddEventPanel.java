package finance;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import customui.HubTextField;

public class AddEventPanel extends JPanel{

	private static final long serialVersionUID = 1L;
	private JLabel eventLabel;
	private HubTextField eventText;
	private boolean valid;
	
	public AddEventPanel(){
		super();
		valid = false;
		this.setBackground(Color.WHITE);
		instantiateComponents();
		createGUI();
		
	}
	
	private void instantiateComponents(){
		eventLabel = new JLabel("Event Name: ");
		eventText = new HubTextField("Ex. Fundraiser");
	}
	
	private void createGUI(){
		this.setBorder(new TitledBorder("Add Event"));
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();			
		gbc.weightx = 1; gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0; gbc.gridy = 0;
		add(eventLabel, gbc); gbc.gridx = 1;
		add(eventText, gbc);
	}
	
	public void verify(JComboBox<String> events){
		String text = getEventString();
		//if the text field is not empty
		if(!text.equals("") && text != null ){
			//if the events doesn't already exist
			boolean exists = false;
			for(int i = 0; i < events.getItemCount(); i++){
				if(events.getItemAt(i).toLowerCase().equals(text.toLowerCase())){
					exists = true;
				}
			}
			if(!exists){
				valid = true;
				return;
			}
		}		
		//if event input is invalid (empty or already exists) error message will display
		JOptionPane.showMessageDialog(this, "Please enter valid event name that does not already exist.");
	}
	
	public boolean isValid(){
		return valid;
	}
	
	public String getEventString(){
		return eventText.getText();
	}
	
	public void clearForm(){
		eventText.setText("");
		valid = false;
	}
}
