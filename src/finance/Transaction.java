package finance;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

public class Transaction implements Serializable{
	private static final long serialVersionUID = 1L;
	public String event, description, vendor, buyer;
	public int id;
	public Timestamp date;
	public double price;
	
	public Transaction(String event, String description, double price, String vendor, String buyer){
		this.event = event;
		this.description = description;
		this.price = price;
		this.vendor = vendor;
		this.buyer = buyer;
		//Date object
		Date date= new Date();
		long time = date.getTime();
		this.date = new Timestamp(time);
	}	
	
	public static Object[] toObjectArray(Transaction t){
		return new Object[] {
				t.id,
				t.event,
				t.description,
				t.price,
				t.date,
				t.vendor,
				t.buyer
		};
	}
}
