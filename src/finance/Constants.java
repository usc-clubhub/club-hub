package finance;

public class Constants {
	public static final String[] TRANSACTION_TABLE_COLUMNS = {
			"Event", "Description", "Price", "Date", "Vendor", "Buyer"
	};
	
	public static final String ADD_EVENT_ITEM = "Add Event...";
	
}
