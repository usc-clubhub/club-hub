package finance;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import client.HubClient;
import customui.BorderButton;
import customui.Constants;
import customui.HubButton;
import customui.HubComboBox;
import customui.HubScrollBarUI;
import customui.ThemeColors;
import library.FontLibrary;
import library.ImageLibrary;
import membership.ForcedListSelectionModel;
import server.ServerClientPackage;
import server.ServerClientPackage.Category;

public class FinancePanel extends JPanel{

	private static final long serialVersionUID = 1L;
	private HubButton addTransactionButton, removeTransactionButton;
	private JLabel currBalanceAmountLabel, revenueAmountLabel, expenseAmountLabel;
	private JTable transactionTable;
	private JLabel dollarLabel;
	private JPanel contentPanel;
	private Color mColor;
	private AddTransactionForm mAddTransactionForm;
	private Vector<String> events;
	private Font font;
	private boolean received = false;
	
	
	private HubClient mHubClient;

	public FinancePanel(HubClient hc, Dimension d){
		super();
		mHubClient = hc;
		mHubClient.setFinancePanel(this);

		font = FontLibrary.getFont(Constants.FONT_GLOBER_REGULAR, 18);	
		
		this.setPreferredSize(d);
		this.setMinimumSize(d);
		this.setSize(d);
		
		events = new Vector<String>();
		if(events.size() == 0){
			events.add("Membership");
			events.add("Administration");
		}
	

		currBalanceAmountLabel = new JLabel();
		revenueAmountLabel = new JLabel();
		expenseAmountLabel = new JLabel();
		
		ServerClientPackage scp = new ServerClientPackage(Category.GET_TRANSACTIONS, null);
		hc.sendPackage(scp);	
	
		while (received == false) {
			try {
				Thread.sleep(150);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		//this.client = client;
		
		this.setBackground(Color.WHITE);
		this.setLayout(new BorderLayout());
		instantiateComponents();
		createGUI();
		addActionListeners();	
		setFont(font);
	}
	
	private void instantiateComponents(){
		contentPanel = new JPanel();
		mAddTransactionForm = new AddTransactionForm();
		addTransactionButton = new HubButton("+", ThemeColors.SIGNUP_COLOR, ThemeColors.SIGNUP_HIGHLIGHT_COLOR);
		addTransactionButton.setPreferredSize(new Dimension(50,50));
		removeTransactionButton = new HubButton("-", ThemeColors.LOGIN_COLOR, ThemeColors.LOGIN_HIGHLIGHT_COLOR);
		removeTransactionButton.setPreferredSize(new Dimension(50,50));
	}	

	public void setTable(JTable jt, Color c){
		//creates table model that doesn't allow for cells to be edited
		//transactionTableModel = new DefaultTableModel(mFinance.transactionData, Constants.TRANSACTION_TABLE_COLUMNS);
		mColor = c;		
		transactionTable = jt;
		
		DefaultTableCellRenderer center = new DefaultTableCellRenderer();
		center.setHorizontalAlignment(JLabel.CENTER);


		for (int i = 0; i < transactionTable.getColumnCount(); i++) {
			transactionTable.getColumnModel().getColumn(i).setCellRenderer(center);			
		}	
		transactionTable.setTableHeader(new JTableHeader(transactionTable.getColumnModel()) {
			private static final long serialVersionUID = 1L;
			@Override public Dimension getPreferredSize() {
			    this.setBackground(Color.white);
			    this.setFont(new Font("Helvana", Font.BOLD, 13));
				Dimension d = super.getPreferredSize();
			    d.height = 35;
			    return d;
			  }
			});
		transactionTable.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, new Color(200,200,200)));
		transactionTable.getTableHeader().setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, new Color(200,200,200)));
		transactionTable.setRowHeight(50);
		transactionTable.getTableHeader().setReorderingAllowed(false);
		transactionTable.setSelectionModel(new ForcedListSelectionModel());
		transactionTable.setBackground(Color.WHITE);
		transactionTable.setGridColor(Color.black);
        transactionTable.setBackground(Color.darkGray);
        transactionTable.setForeground(Color.white);
        transactionTable.setSelectionBackground(mColor);
        transactionTable.setSelectionForeground(Color.white);
        transactionTable.setOpaque(false);
        transactionTable.setFont(new Font("Helvana", Font.PLAIN, 12));
		transactionTable.setAutoCreateRowSorter(true);
		transactionTable.setPreferredScrollableViewportSize(new Dimension(1000, 500));
		transactionTable.setRowSelectionAllowed(true);
		updateEvents();
		calculateBalances();	
		revalidate();
		repaint();
		received = true;

		
	}
	
	private void createGUI(){
		currBalanceAmountLabel.setFont(new Font("Trebuchet MS", Font.TRUETYPE_FONT, 95));
		currBalanceAmountLabel.setAlignmentX(CENTER_ALIGNMENT);
		currBalanceAmountLabel.setForeground(new Color(75,75,75));
		revenueAmountLabel.setFont(new Font("Trebuchet MS", Font.TRUETYPE_FONT, 25));
		revenueAmountLabel.setAlignmentX(RIGHT_ALIGNMENT);
		revenueAmountLabel.setForeground(new Color(66,199,116));
		expenseAmountLabel.setFont(new Font("Trebuchet MS", Font.TRUETYPE_FONT, 25));		
		expenseAmountLabel.setAlignmentX(RIGHT_ALIGNMENT);
		expenseAmountLabel.setForeground(new Color(219,84,84));
		


		
		JPanel headerPanel = new JPanel();
		headerPanel.setBackground(Color.WHITE);
		headerPanel.setLayout(new GridBagLayout());
		headerPanel.setBorder(new EmptyBorder(15, 15, 15, 15));
		JPanel amountsPanel = new JPanel();
		amountsPanel.setBackground(Color.WHITE);
		GridBagConstraints gbc = new GridBagConstraints();
		Image img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/dollarBill.png"), 130, 104);
		dollarLabel = new JLabel(new ImageIcon(img));
		amountsPanel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		JPanel topPanel = new JPanel();
		topPanel.setBackground(Color.WHITE);
		topPanel.setLayout(new GridBagLayout());
		c.insets = new Insets(0,10,0,10);
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 0.0; c.weighty = 0.5; c.gridwidth = 1; c.gridx = 1;	c.gridy = 0;
		amountsPanel.add(currBalanceAmountLabel, c);
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weighty = 0.0; c.weightx = 0.0; c.gridx = 0; c.gridy = 0;
		amountsPanel.add(dollarLabel, c);
		JPanel buttonsPanel = new JPanel();
		buttonsPanel.setBackground(Color.WHITE);
		buttonsPanel.add(addTransactionButton);
		buttonsPanel.add(removeTransactionButton);	
		
		c = new GridBagConstraints();
		c.weightx = 0; c.gridwidth = 2; c.gridx = 0; c.gridy = 0;
		topPanel.add(amountsPanel, c); 
		c.weightx = 0.5; c.gridwidth = 1; c.gridx = 0; c.gridy = 1;
		topPanel.add(revenueAmountLabel, c);
		c.weightx = 0.5; c.gridwidth = 1; c.gridx = 1; c.gridy = 1;
		topPanel.add(expenseAmountLabel, c);
		c.weightx = 0; c.gridwidth = 2; c.gridx = 0; c.gridy = 2;
		topPanel.add(buttonsPanel, c);	
		
		gbc = new GridBagConstraints();
		gbc.gridx = 0; gbc.gridy = 0;
		gbc.anchor = GridBagConstraints.EAST;
		gbc.anchor = GridBagConstraints.CENTER;
		gbc.gridx = 0; gbc.gridy = 1;
		headerPanel.add(topPanel,  gbc);
		
		contentPanel.setLayout(new BorderLayout());
		contentPanel.add(headerPanel, BorderLayout.NORTH);
		JScrollPane scrollPane = new JScrollPane(transactionTable);
		scrollPane.getVerticalScrollBar().setUI(new HubScrollBarUI());
		contentPanel.add(scrollPane);
		contentPanel.setBackground(Color.WHITE);
		
		add(contentPanel);
	}
	
	private void addActionListeners(){
		//remove transaction functionality
		removeTransactionButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent arg0) {
				// if a row is selected, remove selected rows
				if(transactionTable.getSelectedRow()!= -1){
					DefaultTableModel dtm = (DefaultTableModel)transactionTable.getModel();
					int id = (int) transactionTable.getValueAt(transactionTable.getSelectedRow(), 0);
					dtm.removeRow(transactionTable.getSelectedRow());
					ServerClientPackage scp = new ServerClientPackage(Category.DELETE_TRANSACTION, id);
					mHubClient.sendPackage(scp);
					calculateBalances();
				}
			}
		});
		
		//add transaction functionality
		addTransactionButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				// open new transaction dialog
				remove(contentPanel);
				add(mAddTransactionForm);
				revalidate();
				repaint();
			}
			
		});
	}
	
	
	class AddTransactionForm extends JPanel {
		private static final long serialVersionUID = 1L;
		
		private JLabel eventLabel;
		private JComboBox<String> eventComboBox;		
		private HubButton cancelAddEventButton, submitEventButton, submitTransactionButton, addEventButton, cancelTransactionButton;
		private JPanel eventPanel, addEventButtonPanel, addTransactionButtonPanel;
		private AddEventPanel addEventPanel;
		private AddTransactionPanel addTransactionPanel;
		
		public AddTransactionForm(){
			super();
			setSize(400,400);
			setMinimumSize(new Dimension(400,400));
			setMaximumSize(new Dimension(400,400));
			setPreferredSize(new Dimension(400,400));

			
			this.instantiateComponents();	
			this.createGUI();
			this.addActionListeners();
			setBackground(mColor);
			
			setVisible(true);
		}
		
		private void instantiateComponents(){
			//event panel components
			eventPanel = new JPanel();
			eventPanel.setBackground(Color.WHITE);
			eventLabel = new JLabel("Events: ");
			eventComboBox = new HubComboBox<String>(events);//(atdFinance.events);				
			addEventButton = new HubButton("+", ThemeColors.SIGNUP_COLOR, ThemeColors.SIGNUP_HIGHLIGHT_COLOR);
			addEventButton.setPreferredSize(new Dimension(30,30));
			
			//event panel components
			addEventPanel = new AddEventPanel();			
			addEventButtonPanel = new JPanel();
			cancelAddEventButton = new HubButton("CANCEL", ThemeColors.LOGIN_COLOR, ThemeColors.LOGIN_HIGHLIGHT_COLOR);
			submitEventButton = new HubButton("ADD EVENT", ThemeColors.SIGNUP_COLOR, ThemeColors.SIGNUP_HIGHLIGHT_COLOR);
			
			
			//transaction panel components
			addTransactionPanel = new AddTransactionPanel(new Dimension(this.getWidth()/2, this.getHeight()));			
			addTransactionButtonPanel = new JPanel();
			cancelTransactionButton = new HubButton("CANCEL", ThemeColors.LOGIN_COLOR, ThemeColors.LOGIN_HIGHLIGHT_COLOR);
			submitTransactionButton = new HubButton("ADD TRANS", ThemeColors.SIGNUP_COLOR, ThemeColors.SIGNUP_HIGHLIGHT_COLOR);
			
		}
		
		private void createGUI(){
			setLayout(new BorderLayout());
			//set event panel at top
			eventPanel.add(eventLabel);
			eventPanel.add(eventComboBox);
			eventPanel.add(addEventButton);	
			//set button panel at bottom for new events
			addEventButtonPanel.add(submitEventButton);
			addEventButtonPanel.add(cancelAddEventButton);	
			addEventButtonPanel.setBackground(Color.WHITE);
			//set button panel at bottom for new transactions
			addTransactionButtonPanel.add(submitTransactionButton);
			addTransactionButtonPanel.add(cancelTransactionButton);
			addTransactionButtonPanel.setBackground(Color.WHITE);
			
			//add panels to layout
			add(eventPanel, BorderLayout.NORTH);
			add(addTransactionPanel, BorderLayout.CENTER);
			add(addTransactionButtonPanel, BorderLayout.SOUTH);
		}
		
		private void addActionListeners(){			
			//panel switching functionality
			addEventButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					showAddEventPanel();					
				}				
			});			
			cancelAddEventButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent arg0) {
					addEventPanel.clearForm();
					showAddTransactionPanel();					
				}
				
			});			
			//add event functionality
			submitEventButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					//check if user input is valid
					addEventPanel.verify(eventComboBox);	
					//if so, update events list
					if(addEventPanel.isValid()){
						//System.out.println("add event is valid: "+ 	addEventPanel.getEventString());					
						eventComboBox.addItem(addEventPanel.getEventString());
						showAddTransactionPanel();
					}
					//System.out.println("add event button clicked");
					
					revalidate();
					repaint();
					
					//atdFinance.printEvents();
					addEventPanel.clearForm();
				}				
			});			
			//add transaction functionality
			submitTransactionButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent arg0) {
					//check if user input is valid
					addTransactionPanel.verify();
					if(addTransactionPanel.isValid()){
						Transaction nt = addTransactionPanel.getTransaction(eventComboBox.getItemAt(eventComboBox.getSelectedIndex()));
						ServerClientPackage scp = new ServerClientPackage(Category.ADD_TRANSACTION, nt);
						mHubClient.sendPackage(scp);
						closeForm();
					}	
					addTransactionPanel.clearForm();	
					updateEvents();
				}				
			});
			
			cancelTransactionButton.addActionListener(new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent arg0) {
					addTransactionPanel.clearForm();
					closeForm();					
				}
			});
		}
		
		private void showAddEventPanel(){
			remove(addTransactionPanel);
			remove(addTransactionButtonPanel);
			addEventButton.setEnabled(true);
			
			add(addEventPanel, BorderLayout.CENTER);
			add(addEventButtonPanel, BorderLayout.SOUTH);
			//System.out.println("add event is selected");
			revalidate();
			repaint();
			
		}
		
		private void showAddTransactionPanel(){
			remove(addEventPanel);
			remove(addEventButtonPanel);
			addEventButton.setEnabled(true);
			add(addTransactionPanel, BorderLayout.CENTER);
			add(addTransactionButtonPanel, BorderLayout.SOUTH);
			revalidate();
			repaint();
		}
	}
	
	private void closeForm(){
		remove(mAddTransactionForm);
		add(contentPanel);
		revalidate();
		repaint();
	}
	
	private void updateEvents(){
		for(int i = 0; i < transactionTable.getRowCount(); i++){
			String s = (String)transactionTable.getValueAt(i, 1);
			if(!events.contains(s)){
				events.add(s);
			}
		}
	}
	
	protected int getLastestTransactionID(){
		int largest = 0;
		for(int i = 0; i < transactionTable.getRowCount(); i++){
			int id = (int)transactionTable.getValueAt(i, 0);
			if(id > largest){
				largest = id;
			}
		}
		return largest;
	}
	
	public void addTransactionRow(Transaction t){
		System.out.println("in gui add transaction");
		DefaultTableModel dtm = (DefaultTableModel)transactionTable.getModel();
		dtm.addRow(new Object[]{t.id, t.event, t.description, t.price, new SimpleDateFormat("MM/dd/yyyy").format(t.date), t.vendor, t.buyer});
		calculateBalances();
	}
	
	public void calculateBalances(){
		double currentBalance = 0;
		double revenue = 0;
		double expense = 0;
		for(int i = 0; i < transactionTable.getRowCount(); i++){
			double amt = (double)transactionTable.getValueAt(i, 3);
			currentBalance += amt;
			if(amt > 0){
				revenue+=amt;
			}
			else if(amt < 0){
				expense +=amt;
			}
		}
		
		
		
		currBalanceAmountLabel.setText("$"+currentBalance);
		revenueAmountLabel.setText("$"+revenue);
		expenseAmountLabel.setText("$"+expense);
		revalidate();
		repaint();
		
	}
}
