package finance;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import customui.HubTextField;

public class AddTransactionPanel extends JPanel{
	private static final long serialVersionUID = 1L;
	
	private JLabel descriptionLabel, priceLabel, vendorLabel, buyerLabel;
	private HubTextField descriptionText, vendorText, buyerText, priceText;
	
	private String description, vendor, buyer;
	private double price;
	private boolean valid;
	
	public AddTransactionPanel(Dimension d){
		super();
		setSize(d);
		setMinimumSize(d);
		setPreferredSize(d);
		this.setBackground(Color.WHITE);
		instantiateComponents();
		createGUI();
	}
	
	private void instantiateComponents(){		
		descriptionLabel = new JLabel("Description: ");
		priceLabel = new JLabel("Price: ");
		vendorLabel = new JLabel("Vendor: ");
		buyerLabel = new JLabel("Buyer: ");			
		descriptionText = new HubTextField("Item Description");
		vendorText = new HubTextField("Ex. Costco, Ralphs");
		buyerText = new HubTextField("Ex. John Smith");
		priceText = new HubTextField("100.00");
		
		
//		NumberFormat priceFormat = NumberFormat.getCurrencyInstance(Locale.US);
//		priceFormat.setMaximumFractionDigits(2); //will accept two digits behind decimal
//		NumberFormatter priceFormatter = new NumberFormatter(priceFormat);
//		priceFormatter.setAllowsInvalid(false);
//		priceFormatter.setOverwriteMode(true);
//		priceFormattedTextField = new HubFormattedTextField("1.57", priceFormatter);	
	}
	
	private void createGUI(){
		//add transaction panel
		this.setBorder(new TitledBorder("Add Transaction"));
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();			
		gbc.weightx = 1; gbc.fill = GridBagConstraints.HORIZONTAL;
		gbc.gridx = 0; gbc.gridy = 0;
		this.add(descriptionLabel, gbc); gbc.gridx = 1;
		this.add(descriptionText, gbc); gbc.gridx = 0; gbc.gridy = 2;
		this.add(priceLabel, gbc); gbc.gridx = 1;
		this.add(priceText, gbc); gbc.gridx = 0; gbc.gridy = 3;
		
		this.add(vendorLabel, gbc); gbc.gridx = 1;
		this.add(vendorText, gbc); gbc.gridx = 0; gbc.gridy = 4;
		this.add(buyerLabel, gbc); gbc.gridx = 1;
		this.add(buyerText, gbc); gbc.gridx = 0; gbc.gridy = 5;
	}	
	
	public void verify(){
		description = descriptionText.getText();
		
		vendor = vendorText.getText();
		buyer = buyerText.getText();
		
		//check price 
		try{
			price = Double.parseDouble(priceText.getText());
		} catch(NumberFormatException nfe){
			JOptionPane.showMessageDialog(null, "Please enter valid price value.");
			return;
		}
		
		boolean hasDescription = description != null && !description.equals("");
		boolean hasVendor = vendor!=null && !vendor.equals("");
		boolean hasBuyer = buyer!=null && !buyer.equals("");
		boolean hasPrice = price!=0;
		boolean isComplete = hasPrice && hasDescription && hasVendor;
		
		if(isComplete){
			valid = true;
			if(!hasBuyer){
				buyer = "Admin";
			}
			return;
		}
		
		JOptionPane.showMessageDialog(null, "Please complete form.");
		
	}
	
	public boolean isValid(){
		return valid;
	}
	
	public void clearForm(){
		descriptionText.setText("");
		vendorText.setText("");
		buyerText.setText("");
		priceText.setText("0");
		valid = false;
	}
	
	public Transaction getTransaction(String event){
		return new Transaction( event, description, price, vendor, buyer);
	}
}
