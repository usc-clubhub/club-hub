package announcements;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JTextField;

import client.HubClient;
import customui.HubButton;
import server.ServerClientPackage;
import server.UserInformation;

public class NewCommentPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private JTextField newComment;
	private HubButton postButton;
	private HubClient hc;
	private Announcement announcement;
	
	public NewCommentPanel(HubClient hc, Announcement announcement) {
		this.announcement = announcement;
		this.setBackground(Color.WHITE);
		this.hc = hc;
		this.setLayout(new GridBagLayout());
		instantiateComponents();
		createGUI();
		addActionListeners();
	}
	
	private void instantiateComponents() {
		newComment = new JTextField();
		newComment.setBorder(BorderFactory.createMatteBorder(2, 2, 2, 2, new Color(228,228,228)));
		newComment.setPreferredSize(new Dimension(600,35));
		postButton = new HubButton("POST", customui.ThemeColors.LOGIN_COLOR, customui.ThemeColors.LOGIN_HIGHLIGHT_COLOR);
		postButton.setPreferredSize(new Dimension(150, 35));
	}
	private void createGUI() {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(5,5,5,5);
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(newComment, gbc);
		gbc.gridx = 1;
		gbc.gridy = 0;
		this.add(postButton, gbc);
	}
	private void addActionListeners() {
		postButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent a) {
				UserInformation ui = hc.getUserInfo();
				Comment c = new Comment(ui.getFname() + " " + ui.getLname(), newComment.getText(), null, announcement.getID());
				ServerClientPackage scp = new ServerClientPackage(server.ServerClientPackage.Category.NEW_COMMENT, c);
				hc.sendPackage(scp);
				hc.sendPackage(new ServerClientPackage(ServerClientPackage.Category.FETCH_COMMENTS, announcement));
			}
		});
	}
}
