package announcements;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;

import client.HubClient;
import library.FontLibrary;
import server.ServerClientPackage;

public class CommentPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private Comment c;
	
	private JLabel author, date, body;
	private Font bodyFont, authorFont, dateFont;
    private final String html1 = "<html><body style='width: 550px'><p style=\"line-height: 30pt;\">";
    private final String html2 = "</p></body></html>";
    private HubClient hc;
    private JLabel deleteLabel;
    
	public CommentPanel(Comment c, HubClient hc) {
		this.hc = hc;
		this.c = c;
		this.setLayout(new GridBagLayout());
		this.setBackground(Color.WHITE);
		this.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(228,228,228)));
		bodyFont = FontLibrary.getFont(customui.Constants.FONT_OPEN_SANS, 15.0f);
		authorFont = FontLibrary.getFont(customui.Constants.FONT_OPEN_SANS, 15.0f);
		authorFont = authorFont.deriveFont(Font.BOLD);
		dateFont = FontLibrary.getFont(customui.Constants.FONT_OPEN_SANS, 13.0f);
		instantiateComponents();
		createGUI();
		addActionListeners();
	}
	private void instantiateComponents() {
		body = new JLabel(html1 + c.getContent() + html2);
		body.setFont(bodyFont);
		author = new JLabel(c.getAuthor());
		date = new JLabel(c.getDate());
		date.setForeground(new Color(190,190,190));
		deleteLabel = new JLabel("Delete");
		deleteLabel.setFont(dateFont);
		deleteLabel.setForeground(new Color(218,133,133));
	}
	private void createGUI() {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.anchor = GridBagConstraints.WEST;
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(author, gbc);
		gbc.gridx = 0;
		gbc.gridy = 1;
		this.add(body, gbc);
		gbc.gridx = 0;
		gbc.gridy = 2;
		gbc.ipady = 5;
		this.add(date, gbc);
		gbc = new GridBagConstraints();
		gbc.gridx = 1;
		gbc.gridy = 2;
		this.add(deleteLabel, gbc);
		if (hc.getUserInfo().getUsername() == "Guest") {
			deleteLabel.setVisible(false);
		}
	}
	private void addActionListeners() {
		deleteLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent ae) {
				hc.sendPackage(new ServerClientPackage(ServerClientPackage.Category.DELETE_COMMENT, c));
			}
		});
	}
}
