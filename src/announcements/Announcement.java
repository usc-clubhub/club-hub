package announcements;

import java.io.Serializable;

/* For sending/receiving over socket */
public class Announcement implements Serializable {
	private static final long serialVersionUID = 1L;
	String announcementText;
	String announcementTitle;
	String date;
	String author;
	boolean isPublic;
	int id;
	
	public Announcement(String announcementText, String announcementTitle, String date, String author, boolean isPublic, int id) {
		this.id = id;
		this.announcementText = announcementText;
		this.announcementTitle = announcementTitle;
		this.date = date;
		this.author = author;
		this.isPublic = isPublic;
	}
	public String getAnnouncementText() {
		return announcementText;
	}
	public String getAnnouncementTitle() {
		return announcementTitle;
	}
	public String getDate() {
		return date;
	}
	public String getAuthor() {
		return author;
	}
	public boolean isPublic() {
		return isPublic;
	}
	public int getID(){
		return id;
	}
}
