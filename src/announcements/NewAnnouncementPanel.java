
package announcements;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
/* A panel that displays an individual announcement */
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import client.HubClient;
import customui.HubButton;
import customui.HubScrollBarUI;
import customui.ThemeColors;
import library.FontLibrary;
import server.ServerClientPackage;
import server.UserInformation;
/* This is a panel for an individual announcement */
public class NewAnnouncementPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private Font bodyFont, titleFont, dateFont, authorFont;
	private JLabel title, body, date, author, charCount;
	private JScrollPane scrollPane;
	private JTextArea textArea;
	private JTextField textField;
	private HubButton showEditor;
	private HubButton submitButton;
	private int charsUsed = 0;
	private JPanel bottomPanel;
	private HubClient hubClient;
	private JCheckBox isPublic;
    
	public NewAnnouncementPanel(HubClient hubClient) {
		this.hubClient = hubClient;
		//this.setPreferredSize(new Dimension(650, 200));	
		this.setBackground(Color.WHITE);
		this.setLayout(new GridBagLayout());
		this.setBorder(BorderFactory.createMatteBorder(0, 0, 0, 0, new Color(190,190,190)));	
		titleFont = FontLibrary.getFont(customui.Constants.FONT_GLOBER_BOLD, 20.0f);
		bodyFont = FontLibrary.getFont(customui.Constants.FONT_GLOBER_REGULAR, 17.0f);
		authorFont = FontLibrary.getFont(customui.Constants.FONT_GLOBER_REGULAR, 15.0f);
		
		instantiateComponents();
		createGUI();
		addActionListeners();
	}
	
	private void instantiateComponents() {
		isPublic = new JCheckBox("Make public?");
		isPublic.setOpaque(false);
		isPublic.setFocusable(false);
		isPublic.setVisible(false);
		isPublic.setFont(FontLibrary.getFont(customui.Constants.FONT_OPEN_SANS, 12.0f));

		title = new JLabel("New Announcement");
		//title.setBorder(new EmptyBorder(15,0,5,0));
		title.setFont(titleFont);
		title.setAlignmentX(LEFT_ALIGNMENT);
		
		scrollPane = new JScrollPane();
		showEditor = new HubButton("POST ANNOUNCEMENT", customui.ThemeColors.LOGIN_HIGHLIGHT_COLOR, customui.ThemeColors.LOGIN_HIGHLIGHT_COLOR);
		if (hubClient.getUserInfo().getUsername() == "Guest") {
			showEditor.setVisible(false);
		}
		
		showEditor.setPreferredSize(new Dimension(250,40));
		showEditor.setFocusable( false );
		
		submitButton = new HubButton("SUBMIT", customui.ThemeColors.LOGIN_COLOR, customui.ThemeColors.LOGIN_HIGHLIGHT_COLOR);
		submitButton.setAlignmentX(RIGHT_ALIGNMENT);
		submitButton.setVisible(false);
		title = new JLabel("Post Announcement");
		Font textFieldsFont = new Font("Trebuchet MS", Font.PLAIN, 19);
		title.setFont(titleFont);
		textArea = new JTextArea();
		textArea.setFont(textFieldsFont);
		textArea.setRows(5);
		textArea.setLineWrap(true);
		textArea.setDocument(new TextFieldLimiter(500));
		textArea.setSize(new Dimension(950,120));
		scrollPane.setBorder(BorderFactory.createMatteBorder(15, 15, 15, 15, new Color(235,235,235)));
		scrollPane.getViewport().add(textArea);
		scrollPane.getVerticalScrollBar().setUI(new HubScrollBarUI());
		scrollPane.setPreferredSize(new Dimension(950,130));
		scrollPane.setVisible(false);
		textField = new JTextField() {
			private static final long serialVersionUID = 1L;
			protected void paintComponent(Graphics g) {
				super.paintComponent(g);	
		        if(this.getText().equals("")) {
		            int height = this.getHeight();
		            Font prev = g.getFont();
		            Color prevColor = g.getColor();
		            g.setFont(textFieldsFont);
		            g.setColor(ThemeColors.INACTIVE_TEXT);
		            int h = g.getFontMetrics().getHeight();
		            int textBottom = (height - h) / 2 + h - 2;
		            Graphics2D g2d = (Graphics2D) g;
		            RenderingHints hints = g2d.getRenderingHints();
		            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		            g2d.drawString("Post title..", 16, textBottom);
		            g2d.setRenderingHints(hints);
		            g.setFont(prev);
		            g.setColor(prevColor);
		        }
			}
		};
		textField.setFont(new Font("Trebuchet MS", Font.PLAIN, 19));
		textField.setDocument(new TextFieldLimiter(100));
		textField.setPreferredSize(new Dimension(950,50));
		//textField.setSize(new Dimension (950,30));
		textField.setBorder(BorderFactory.createMatteBorder(10,15,0,15, new Color(235,235,235)));
		textField.setVisible(false);
		
		charCount = new JLabel("Characters used: " + charsUsed + "/500");
		charCount.setFont(new Font("Trebuchet MS", Font.PLAIN, 14));
		charCount.setForeground(new Color(145,145,145));
		charCount.setAlignmentX(LEFT_ALIGNMENT);
		charCount.setVisible(false);
	}
	private void createGUI() {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(showEditor, gbc);

		gbc.insets = new Insets(0,0,0,0);
		gbc.gridx = 0;
		gbc.gridy = 1;
		this.add(textField);

		gbc.gridx = 0;
		gbc.gridy = 2;
		this.add(scrollPane, gbc);

		//gbc.insets = new Insets(10,0,0,0);
		gbc.anchor = GridBagConstraints.WEST;
		gbc.gridx = 0;
		gbc.gridy = 3;
		this.add(charCount, gbc);

		gbc.gridx = 0;
		gbc.gridy = 4;
		gbc.anchor = GridBagConstraints.EAST;
		this.add(submitButton, gbc);
		gbc.gridy = 5;
		this.add(isPublic, gbc);

	}
	private void addActionListeners() {
		showEditor.addActionListener(new ActionListener() {
			Boolean isVisible = false;
            public void actionPerformed(ActionEvent e) {
            	if (isVisible == false) {
					scrollPane.setVisible(true);
					showEditor.setVisible(false);
					submitButton.setVisible(true);
					textField.setVisible(true);
					charCount.setVisible(true);
					isPublic.setVisible(true);
					isVisible = true;
            	} else {
            		scrollPane.setVisible(false);
            		submitButton.setVisible(false);
            		textField.setVisible(false);
            		charCount.setVisible(false);
            		isPublic.setVisible(false);
            		isVisible = false;
            	}
				revalidate();
				repaint();
            }
		});
		
		textArea.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent arg0) {
				String text = textArea.getText();	
				charsUsed = text.length();
				charCount.setText("Characters used: " + charsUsed + "/500");
				charCount.revalidate();
				charCount.repaint();
				revalidate();
				repaint();
			}
		});
		
		submitButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String text = textArea.getText();
				String titleText = textField.getText();
				UserInformation ui = hubClient.getUserInfo();
				Announcement newAnnouncement = new Announcement(text, titleText, null, ui.getFname() + " " + ui.getLname(), isPublic.isSelected(), -1);
				hubClient.sendPackage(new ServerClientPackage(server.ServerClientPackage.Category.NEW_POST, newAnnouncement));
			}
		});
	}
	
	
	class TextFieldLimiter extends PlainDocument
	{
	  int maxChar = -1;
	  public TextFieldLimiter(int len){maxChar = len;}
	  public void insertString(int offs, String str, AttributeSet a) throws BadLocationException
	  {
	    if (str != null && maxChar > 0 && this.getLength() + str.length() > maxChar)
	    {
	      java.awt.Toolkit.getDefaultToolkit().beep();
	      return;
	    }
	    super.insertString(offs, str, a);
	  }
	}
	
}
