package announcements;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JPanel;

import client.HubClient;
import customui.HubButton;
import server.ServerClientPackage;
import server.ServerClientPackage.Category;
import server.UserInformation;

public class AnnouncementsGUI extends JPanel {
	
	private ArrayList<Announcement> announcements = null; // Only announcements to display will be held here. 
	private ArrayList<AnnouncementPanel> panelList = null;
	private static final long serialVersionUID = 1L;
	protected NewAnnouncementPanel nap;
	private int newAnnouncements = 0;
	protected HubButton newAnnouncementNotification;
	private HubClient hubClient;
	
	public AnnouncementsGUI(HubClient client) {
		this.hubClient = client;
		hubClient.setAnnouncementsGUI(this);
		ServerClientPackage scp = new ServerClientPackage(server.ServerClientPackage.Category.REQUEST_ANNOUNCEMENTS, null);
		hubClient.sendPackage(scp);
		this.setBackground(Color.WHITE);
		this.setLayout(new GridBagLayout());
		instantiateComponents();
		createGUI();
		addActionListeners();	
	}
	
	private void instantiateComponents() {
		panelList = new ArrayList<AnnouncementPanel>();
		newAnnouncementNotification = new HubButton("(" + newAnnouncements + ") NEW ANNOUNCEMENTS", customui.ThemeColors.SIGNUP_COLOR, customui.ThemeColors.SIGNUP_HIGHLIGHT_COLOR);
		newAnnouncementNotification.setPreferredSize(new Dimension(250,40));
		newAnnouncementNotification.setFocusable(false);
		newAnnouncementNotification.setVisible(false);
		nap = new NewAnnouncementPanel(hubClient);
		announcements = new ArrayList<Announcement>();
		
		/* TESTING CODE BELOW */
	/*	
		for (int i = 0; i < 10; i++) {
			announcements.add(new Announcement("This is an example announcement text<br>Test<br>Test" +
					"This is an example. This is an example. This is an example. This is an example."
					+ "This is an example. This is an example. This is an example. This is an example.", "Announcement Title", "07/19/2016", "Jeremy Aftem", true));
		}
		*/
		
	}
	protected void createGUI() {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.ipady = 10;
		this.add(newAnnouncementNotification, gbc);
		gbc.ipady = 0;
		gbc.gridx = 0;
		gbc.gridy = 1;
		this.add(nap, gbc);
		if (hubClient.getUserInfo().getUsername() == "Guest") {
			nap.setVisible(false);
		}
	}
	private void addActionListeners() {
		newAnnouncementNotification.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent ae) {
				ServerClientPackage scp = new ServerClientPackage(server.ServerClientPackage.Category.REQUEST_ANNOUNCEMENTS, null);
				hubClient.sendPackage(scp);
			}
		});
		
	}
	public void newAnnouncementNotification() {
		if (hubClient.getUserInfo().getUsername() == "Guest") return;
		newAnnouncements++;
		newAnnouncementNotification.setText("(" + newAnnouncements + ") NEW ANNOUNCEMENTS");
		newAnnouncementNotification.setVisible(true);
		this.revalidate();
		this.repaint();
	}
	/* This happens when a user either clicks a new announcement notification button or
	 * the navigation bar's "Announcements"
	 */
	public void populateAnnouncements(ArrayList<Announcement> announcements) {
		newAnnouncements = 0;
		this.removeAll();
		this.revalidate();
		this.repaint();
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.ipady = 10;
		newAnnouncementNotification.setVisible(false);
		gbc.ipady = 0;
		this.add(newAnnouncementNotification, gbc);
		//this.announcements.clear();
		panelList.clear();
		this.announcements = announcements;
		gbc.gridx = 0;
		gbc.gridy = 1;
		nap = new NewAnnouncementPanel(hubClient);
		this.add(nap, gbc);
		int display = Constants.DEFAULT_DISPLAY;
		if (announcements.size() < display) {
			display = announcements.size();
		}
		for (int i = 0; i < display; i++) {
			gbc.gridy = i + 2;
			UserInformation ui = hubClient.getUserInfo();
			panelList.add(new AnnouncementPanel(announcements.get(i), this, hubClient));

			if (!ui.getUsername().equals("Guest") || announcements.get(i).isPublic()) {
				this.add(panelList.get(i), gbc);
			}
		}
		this.revalidate();
		this.repaint();
	}
	public void addAnnouncementsList(ArrayList<Announcement> announcements) {
		this.announcements = announcements;
	}
	public void setComments(ArrayList<Comment> commentList, int ID) {
		for (AnnouncementPanel ap : panelList) {
			if (ap.getID() == ID) {
				ap.clearOldComments();
				ap.addComments(commentList);
				
			}
		}
	}
	public void deleteAnnouncement(Announcement toDelete) {
		int removeIndex = announcements.indexOf(toDelete);
		ServerClientPackage scp = new ServerClientPackage(Category.DELETE_POST, toDelete);
		hubClient.sendPackage(scp);
		announcements.remove(removeIndex);
		populateAnnouncements(announcements);
	}
}