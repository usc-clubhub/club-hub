package announcements;

import java.io.Serializable;

public class Comment implements Serializable{
	private static final long serialVersionUID = 8767509281L;
	String commentText;
	String date;
	String author;
	int postID;
	int commentID;
	
	public Comment(String author, String commentText, String date, int id) {
		commentID = -1;
		this.author = author;
		this.commentText = commentText;
		this.date = date;
		postID = id;
	}
	
	public String getContent(){
		return commentText;
	}
	public String getAuthor(){
		return author;
	}
	public String getDate() {
		return date;
	}
	public int getID(){
		return postID;
	}
	public int getCommentID() {
		return commentID;
	}
	public void setCommentID(int commentID) {
		this.commentID = commentID;
	}
}
