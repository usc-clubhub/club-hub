package announcements;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
/* A panel that displays an individual announcement */
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import client.HubClient;
import library.FontLibrary;
import library.ImageLibrary;
import server.ServerClientPackage;
import server.UserInformation;
/* This is a panel for an individual announcement */
public class AnnouncementPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	
	private ArrayList<Comment> comments;
	private JPanel cPanel;
	private Announcement announcement;
	private Font bodyFont, titleFont, dateFont, authorFont, commentsFont;
	private JLabel title, body, date, author;
    final String html1 = "<html><body style='width: 750px'><p style=\"line-height: 35pt;\">";
    final String html2 = "</p></body></html>";
    private AnnouncementsGUI aGUI;
    private HubClient hc;
   private NewCommentPanel ncp;
    
    private JLabel showComments;
    
    private JLabel deleteIcon;
    
	public AnnouncementPanel(Announcement announcement, AnnouncementsGUI aGUI, HubClient hc) {
		this.aGUI = aGUI;
		this.hc = hc;
		this.setBackground(Color.WHITE);
		this.announcement = announcement;
		this.setLayout(new GridBagLayout());
		this.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(228,228,228)));	
		titleFont = FontLibrary.getFont(customui.Constants.FONT_OPEN_SANS, 23.0f);
		bodyFont = FontLibrary.getFont(customui.Constants.FONT_OPEN_SANS, 19.0f);
		authorFont = FontLibrary.getFont(customui.Constants.FONT_OPEN_SANS, 15.0f);
		commentsFont = FontLibrary.getFont(customui.Constants.FONT_OPEN_SANS, 15f);

		instantiateComponents();
		createGUI();
		addActionListeners();
	}
	
	private void instantiateComponents() {
		ncp = new NewCommentPanel(hc, announcement);
		showComments = new JLabel("Show Comments");
		showComments.setForeground(new Color(120,120,120));
		cPanel = new JPanel();
		cPanel.setBackground(Color.WHITE);
		cPanel.setLayout(new GridBagLayout());
		body = new JLabel(html1 + announcement.getAnnouncementText() + html2);
		body.setFont(bodyFont);
		body.setBorder(BorderFactory.createEmptyBorder(10,0,30,0));
		title = new JLabel(announcement.getAnnouncementTitle());
		title.setBorder(new EmptyBorder(30,0,5,0));
		title.setFont(titleFont);
		title.setAlignmentX(LEFT_ALIGNMENT);
		author = new JLabel("Posted by: " + announcement.getAuthor() + " on " + announcement.getDate());
		author.setAlignmentX(LEFT_ALIGNMENT);
		author.setFont(authorFont);
		author.setForeground(new Color(120,120,120));
		body.setAlignmentX(LEFT_ALIGNMENT);
		Image img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/deletePost.png"), 20, 20);
		deleteIcon = new JLabel(new ImageIcon(img));
		deleteIcon.setAlignmentY(CENTER_ALIGNMENT);
		deleteIcon.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
        deleteIcon.setToolTipText("Delete announcement");
	}
	private void createGUI() {
		GridBagConstraints gbc = new GridBagConstraints();
		//gbc.weightx = 1;
		gbc.weightx = 0.5;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.anchor = GridBagConstraints.WEST;
		this.add(title, gbc);
		gbc.weightx = 0.5;
		gbc.gridx = 0;
		gbc.gridy = 1;
		this.add(author, gbc);
		gbc.weightx = 0.5;
		gbc.gridx = 0;
		gbc.gridy = 2;
		this.add(body, gbc);
		gbc.weightx = 0.5;
		gbc.gridx = 1;
		gbc.gridy = 0;
		gbc.gridheight = 3;
		this.add(deleteIcon, gbc);
		if (hc.getUserInfo().getUsername() == "Guest") {
			deleteIcon.setVisible(false);
		}
		gbc = new GridBagConstraints();
		/* Comments */
		gbc.weightx = 0.0;
		gbc.gridx = 0;
		gbc.gridy = 3;
		gbc.anchor = GridBagConstraints.NORTH;
		this.add(showComments, gbc);
		gbc = new GridBagConstraints();
		gbc.weightx = 0.0;
		gbc.gridx = 0;
		gbc.gridy = 4;
		ncp.setVisible(false);
		this.add(ncp, gbc);
		gbc = new GridBagConstraints();
		gbc.weightx = 0.0;
		gbc.gridx = 0;
		gbc.gridy = 5;
		this.add(cPanel, gbc);
		cPanel.setVisible(true);
	}
	
	private void addActionListeners() {
		deleteIcon.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent me) {
				aGUI.deleteAnnouncement(announcement);
			}
		});
		showComments.addMouseListener(new MouseAdapter() {
			boolean showing = false;
			public void mouseReleased(MouseEvent me) {
				if (showing == false) {
					showing = true;
					showComments.setText("Hide Comments");
					hc.sendPackage(new ServerClientPackage(ServerClientPackage.Category.FETCH_COMMENTS, announcement));
				} else {
					showComments.setText("Show Comments");
					showing = false;
					ncp.setVisible(false);
					cPanel.setVisible(false);
				}
			}
		});
	}
	public void clearOldComments() {
		if (comments == null) return;
		this.comments.clear();
		cPanel.removeAll();
		cPanel.revalidate();
		cPanel.repaint();
	}
	public void addComments(ArrayList<Comment> comments) {
		this.comments = comments;
		GridBagConstraints gbc = new GridBagConstraints();
		if (hc.getUserInfo().getUsername() != "Guest") {
			ncp.setVisible(true);
		}
		if (comments.isEmpty()){
			gbc.gridx = 0;
			gbc.gridy = 0;
			gbc.anchor = GridBagConstraints.CENTER;
			JLabel noComments = new JLabel("No comments");
			noComments.setFont(commentsFont);
			noComments.setBackground(Color.WHITE);
			cPanel.add(noComments, gbc);
			this.revalidate();
			this.repaint();
			cPanel.setVisible(true);
			return;
		}
		gbc.gridx = 0;
		int i = 0;
		gbc.ipady = 10;
		for (Comment c : comments) {
			gbc.gridy = i;
			cPanel.add(new CommentPanel(c, hc), gbc);
			i++;
		}
		cPanel.revalidate();
		cPanel.repaint();
		this.revalidate();
		this.repaint();
		cPanel.setVisible(true);
	}
	
	public int getID() {
		return announcement.getID();
	}
}
