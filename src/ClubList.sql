DROP DATABASE if exists ClubList;

CREATE DATABASE ClubList;

USE ClubList;

CREATE TABLE Clubs (
	id int(11) UNIQUE PRIMARY KEY NOT NULL AUTO_INCREMENT,
	hostname VARCHAR(40) NOT NULL,
    club VARCHAR(100) NOT NULL,
    acronym VARCHAR(10)
);

INSERT INTO Clubs(hostname, club) VALUES ('localhost', 'ClubHub Dev Team');
INSERT INTO Clubs(hostname, club) VALUES ('localhost', 'KASA');
INSERT INTO Clubs(hostname, club) VALUES ('localhost', 'Troy Phi');

