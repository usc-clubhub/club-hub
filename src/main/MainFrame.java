package main;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.JFrame;

import announcements.AnnouncementsGUI;
import client.Heartbeat;
import client.HubClient;
import server.UserInformation;

public class MainFrame extends JFrame {
	private static final long serialVersionUID = 1L;
	protected NavigationPanel leftNav;
	protected ContentPanel cp;
	//private UserInformation userInfo;
	protected HubClient hubClient;
	protected Heartbeat heartbeat;
	protected String currentClub;
	
	
	public MainFrame(HubClient hc) {
		setTitle("Club Hub");
		hubClient = hc;
		heartbeat = new Heartbeat(hubClient.getOOS(),hubClient.getUserInfo().getUsername(),this);
		setSize(1500,850);
		setMinimumSize(new Dimension(1400,650));
		setPreferredSize(new Dimension(1500,850));
		setLocationRelativeTo(null);
		setDefaultCloseOperation(EXIT_ON_CLOSE);	
		instantiateComponents();
		createGUI();
		addActionListeners();
		
	}	
	protected void instantiateComponents() {
		cp = new ContentPanel(hubClient, this);
		leftNav = new NavigationPanel(cp, new AnnouncementsGUI(hubClient), hubClient);
	}
	protected void createGUI() {
		/* left nav */
		this.add(leftNav, BorderLayout.WEST);
		/* Content Panel - defualts to Announcements */
		this.add(cp, BorderLayout.CENTER);
		cp.setUserName(hubClient.getUserInfo().getUsername());
		cp.setClubName(hubClient.getUserInfo().getClub().replaceAll("_", " "));
	}
	private void addActionListeners() {
		
	}
	
	public Heartbeat getHeartBeat(){
		return heartbeat;
	}

	public static void main(String[] args) {
		//MainFrame mf = new MainFrame();

		//mf.setVisible(true);
	}
}
