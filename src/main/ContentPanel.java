package main;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.LinkedList;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.border.EmptyBorder;

import client.HubClient;
import customui.HubScrollBarUI;
import library.FontLibrary;
import library.ImageLibrary;
import server.ServerClientPackage;
import splash.LoginGUI;

public class ContentPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	private LinkedList<JLabel> titleTexts;
	private JPanel content;
	private JPanel topMenuPanel;
	private JLabel currentTitle;
	private JLabel clubFlagLabel;
	private JLabel userIconLabel;
	private JLabel notificationIconLabel;
	private JScrollPane scrollPane;
	private JPanel contentPanel;
	private HubClient hubClient;
	private JLabel userManagementLabel;
	private JLabel logoutLabel;
	private JLabel userText;
	private JLabel clubText;
	private MainFrame parentFrame;
	
	public ContentPanel(HubClient hubClient, MainFrame parentFrame) {
		this.parentFrame = parentFrame;
		this.hubClient = hubClient;
		hubClient.setContentPanel(this);
		this.setLayout(new BorderLayout());
		instantiateComponents();
		createGUI();
		addActionListeners();
		this.setBackground(Color.WHITE);
	}
	private void instantiateComponents() {
		JLabel hold;
		userText = new JLabel("exampleuser@usc.edu");
		clubText = new JLabel("Example Club Name");
		
		userText.setFont(FontLibrary.getFont(customui.Constants.FONT_OPEN_SANS, 16.0f));
		userText.setForeground(Color.BLACK);
		userText.setAlignmentX(RIGHT_ALIGNMENT);
		userText.setBorder(BorderFactory.createEmptyBorder(30,5,10,10));
		
		clubText.setFont(FontLibrary.getFont(customui.Constants.FONT_OPEN_SANS, 16.0f));
		clubText.setForeground(Color.BLACK);
		clubText.setAlignmentX(RIGHT_ALIGNMENT);
		clubText.setBorder(BorderFactory.createEmptyBorder(30,5,10,10));
		
		Image img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/notification.png"), 30, 30);
		notificationIconLabel = new JLabel(new ImageIcon(img));
		notificationIconLabel.setAlignmentX(LEFT_ALIGNMENT);
		notificationIconLabel.setBorder(BorderFactory.createEmptyBorder(30,10,10,10));
		notificationIconLabel.setVisible(false);
		
		img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/userTop.png"), 30, 30);
		userIconLabel = new JLabel(new ImageIcon(img));
		userIconLabel.setAlignmentX(LEFT_ALIGNMENT);
		userIconLabel.setBorder(BorderFactory.createEmptyBorder(30,20,10,10));
		
		img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/clubFlag.png"), 30, 30);
		clubFlagLabel = new JLabel(new ImageIcon(img));
		clubFlagLabel.setAlignmentX(LEFT_ALIGNMENT);;
		clubFlagLabel.setBorder(BorderFactory.createEmptyBorder(30,10,10,10));
		
		titleTexts = new LinkedList<JLabel>();

		/* get all title texts */
		img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/nav/announcementsTitle.png"), 200, 40);
		hold = new JLabel(new ImageIcon(img));
		hold.setAlignmentX(LEFT_ALIGNMENT);;
		hold.setBorder(BorderFactory.createEmptyBorder(30,30,10,10));
		titleTexts.add(hold);
		
		img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/nav/calendarTitle.png"), 200, 40);
		hold = new JLabel(new ImageIcon(img));
		hold.setAlignmentX(LEFT_ALIGNMENT);
		hold.setBorder(BorderFactory.createEmptyBorder(20,30,10,10));
		titleTexts.add(hold);
		
		img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/nav/membersTitle.png"), 200, 40);
		hold = new JLabel(new ImageIcon(img));
		hold.setAlignmentX(LEFT_ALIGNMENT);
		hold.setBorder(BorderFactory.createEmptyBorder(20,30,10,10));
		titleTexts.add(hold);
		
		img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/nav/financeTitle.png"), 200, 40);
		hold = new JLabel(new ImageIcon(img));
		hold.setAlignmentX(LEFT_ALIGNMENT);
		hold.setBorder(BorderFactory.createEmptyBorder(20,30,10,10));
		titleTexts.add(hold);
		
		img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/nav/userManagementTitle.png"), 200, 40);
		hold = new JLabel(new ImageIcon(img));
		hold.setAlignmentX(LEFT_ALIGNMENT);
		hold.setBorder(BorderFactory.createEmptyBorder(20,30,10,10));
		titleTexts.add(hold);
	
		/* Create Top Menu */
		topMenuPanel = new JPanel();
		topMenuPanel.setOpaque(true);
		topMenuPanel.setBackground(Color.WHITE);
		topMenuPanel.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(228,228,228)));
		topMenuPanel.setLayout(new BoxLayout(topMenuPanel, BoxLayout.X_AXIS));
		currentTitle = titleTexts.getFirst();
		topMenuPanel.add(currentTitle);
		topMenuPanel.add(Box.createHorizontalGlue());
		topMenuPanel.add(notificationIconLabel);
		topMenuPanel.add(clubFlagLabel);
		topMenuPanel.add(clubText);
		topMenuPanel.add(userIconLabel);
		topMenuPanel.add(userText);
		
		if (hubClient.getUserInfo().isAdmin()) {
			img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/userManagement.png"), 30, 30);
			userManagementLabel = new JLabel(new ImageIcon(img));
			userManagementLabel.setAlignmentX(LEFT_ALIGNMENT);
			userManagementLabel.setBorder(BorderFactory.createEmptyBorder(25,30,10,10));
			userManagementLabel.setVisible(true);
			topMenuPanel.add(userManagementLabel);
		}
		
		img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/logoutRed.png"), 30, 30);
		logoutLabel = new JLabel(new ImageIcon(img));
		logoutLabel.setAlignmentX(LEFT_ALIGNMENT);
		logoutLabel.setBorder(BorderFactory.createEmptyBorder(25,30,10,10));
		logoutLabel.setVisible(true);
		logoutLabel.setToolTipText("Logout");
		topMenuPanel.add(logoutLabel);
		
		/* Place holder  */
		content = new JPanel();
		content.setOpaque(true);
		content.setBackground(Color.WHITE);
		contentPanel = new JPanel();
	}
	private void createGUI() {
		//AnnouncementsGUI aGUI = new AnnouncementsGUI(null);
		scrollPane = new JScrollPane();
		//newContent.setLayout(new FlowLayout());
		scrollPane.setBackground(Color.WHITE);
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBackground(Color.WHITE);
		//contentPanel.add(aGUI);
		scrollPane.getVerticalScrollBar().setUI(new HubScrollBarUI());
		scrollPane.getViewport().add(contentPanel);
		scrollPane.setBorder(new EmptyBorder(0,0,0,0));
		scrollPane.getVerticalScrollBar().setUnitIncrement(10);
		this.add(scrollPane, BorderLayout.CENTER);
		this.add(topMenuPanel, BorderLayout.NORTH);
	}
	
	private void addActionListeners() {
		if (userManagementLabel != null) {
			userManagementLabel.addMouseListener(new MouseAdapter() {
				public void mouseReleased(MouseEvent e) {
					// Send package request
					hubClient.sendPackage(new ServerClientPackage(server.ServerClientPackage.Category.REQUEST_USER_LIST, null));
					// Set Content
					// setContent(stuff);
					setTitle(4);
				}
			});
		}
		
		logoutLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				LoginGUI lgui = new LoginGUI();
				lgui.setVisible(true);
				parentFrame.getHeartBeat().setLogout();
				hubClient.close();
				parentFrame.dispose();
			}
		});
	}
	
	public void setUserName(String username) {
		userText.setText(username);
		this.revalidate();
		this.repaint();
	}
	public void setClubName(String clubname) {
		clubText.setText(clubname);
		this.revalidate();
		this.repaint();
	}
	public void setTitle(int page) {
		topMenuPanel.removeAll();
		topMenuPanel.add(titleTexts.get(page));
		topMenuPanel.add(Box.createHorizontalGlue());
		topMenuPanel.add(notificationIconLabel);
		topMenuPanel.add(clubFlagLabel);
		topMenuPanel.add(clubText);
		topMenuPanel.add(userIconLabel);
		topMenuPanel.add(userText);
		
		if (hubClient.getUserInfo().isAdmin()) {
			topMenuPanel.add(userManagementLabel);
		};
		topMenuPanel.add(logoutLabel);
		revalidate();
		repaint();
	}
	public void setContent(JPanel newContent) {
		content = newContent;
		contentPanel.removeAll();
		contentPanel.add(content);
		contentPanel.revalidate();
		contentPanel.repaint();
		revalidate();
		repaint();
	}
	public void newNotifications() {
		if(!notificationIconLabel.isVisible()) {
			notificationIconLabel.setVisible(true);
			this.revalidate();
			this.repaint();
		}
	}
	public void notificationsRead() {
		notificationIconLabel.setVisible(false);
		this.revalidate();
		this.repaint();
	}
}
