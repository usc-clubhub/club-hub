package main;

import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;

import announcements.AnnouncementsGUI;
import calendar.CalendarFrame;
import client.Constants;
import client.HubClient;
import finance.FinancePanel;
import library.ImageLibrary;
import membership.MembershipTable;
import server.ServerClientPackage;
import server.ServerClientPackage.Category;

public class NavigationPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	protected JLabel logoLabel, announcementsLabel, financeLabel, membersLabel, calendarLabel, infoLabel, bitbucketLabel, settingsLabel;
	protected ContentPanel cp;
	protected AnnouncementsGUI announcementsGUI;
	protected Color navColor;
	protected int notifications;
	protected HubClient hubClient;
	protected ClubDescriptionDisplayer clubDesc;
	
	public NavigationPanel(ContentPanel cp, AnnouncementsGUI announcementsGUI, HubClient hubClient) {
		this.hubClient = hubClient;
		this.announcementsGUI = announcementsGUI;
		/* Default page */
		cp.setContent(announcementsGUI);
		this.cp = cp;
		setOpaque(true);
		setBackground(hubClient.getUserInfo().getColor());
		setPreferredSize(new Dimension(240, 10));
		setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		instantiateComponents();
		createGUI();
		addActionListeners();
	}
	
	private void instantiateComponents() {
		
		/* Club Description */
		clubDesc = new ClubDescriptionDisplayer(hubClient);
	
		/* Logo */
		Image img = ImageLibrary.getScaledImage(ImageLibrary.getImage(Constants.SPLASH_LOGO), 210, 58);
		logoLabel = new JLabel(new ImageIcon(img));
		logoLabel.setAlignmentX(CENTER_ALIGNMENT);
		logoLabel.setBorder(BorderFactory.createEmptyBorder(20,10,60,10));
		
		/* Nav Item 1 */
		Image imgA = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/nav/announcementsButton.png"), 199, 37);
		announcementsLabel = new JLabel(new ImageIcon(imgA));
		announcementsLabel.setAlignmentX(CENTER_ALIGNMENT);
		announcementsLabel.setBorder(BorderFactory.createEmptyBorder(10,10,10,15));
		
		/* Nav Item 1 */
		Image imgB = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/nav/calendarButton.png"), 199, 37);
		calendarLabel = new JLabel(new ImageIcon(imgB));
		calendarLabel.setAlignmentX(CENTER_ALIGNMENT);
		calendarLabel.setBorder(BorderFactory.createEmptyBorder(10,10,10,15));
		
		/* Nav Item 1 */
		Image imgC = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/nav/membersButton.png"), 199, 37);
		membersLabel = new JLabel(new ImageIcon(imgC));
		membersLabel.setAlignmentX(CENTER_ALIGNMENT);
		membersLabel.setBorder(BorderFactory.createEmptyBorder(10,10,10,15));
		
		/* Nav Item 1 */
		Image imgD = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/nav/financeButton.png"), 199, 37);
		financeLabel = new JLabel(new ImageIcon(imgD));
		financeLabel.setAlignmentX(CENTER_ALIGNMENT);
		financeLabel.setBorder(BorderFactory.createEmptyBorder(10,10,10,15));
		
		img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/nav/settingsLink.png"), 50, 50);
		settingsLabel = new JLabel(new ImageIcon(img));
		settingsLabel.setAlignmentX(CENTER_ALIGNMENT);
		settingsLabel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		
		img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/nav/aboutLink.png"), 50, 50);
		infoLabel = new JLabel(new ImageIcon(img));
		infoLabel.setAlignmentX(CENTER_ALIGNMENT);
		infoLabel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		
		img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/nav/bitbucketLink.png"), 50, 50);
		bitbucketLabel = new JLabel(new ImageIcon(img));
		bitbucketLabel.setAlignmentX(CENTER_ALIGNMENT);
		bitbucketLabel.setBorder(BorderFactory.createEmptyBorder(10,10,80,10));
		
	}
	protected void createGUI() {
		this.add(logoLabel);
		this.add(announcementsLabel);
		this.add(calendarLabel);
		this.add(membersLabel);
		this.add(financeLabel);
		this.add(Box.createVerticalGlue());
		this.add(settingsLabel);
		this.add(infoLabel);
		this.add(bitbucketLabel);
	}
	
	private void addActionListeners() {
		membersLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				cp.setTitle(2);
				cp.setContent(new MembershipTable(hubClient));
				cp.revalidate();
				cp.repaint();
			}
		});
		
		calendarLabel.addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent e){
				CalendarFrame cf = new CalendarFrame(hubClient);
				cp.setTitle(1);
				cp.setContent(cf.getPanel());
				cp.revalidate();
				cp.repaint();
			}
		});
		
		financeLabel.addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent e){
				cp.setTitle(3);
				cp.setContent(new FinancePanel(hubClient, new Dimension(cp.getWidth()-(cp.getWidth()/6), cp.getHeight()-(cp.getHeight()/8)) ));
				cp.revalidate();
				cp.repaint();
			}
		});
		
		announcementsLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				//Send msg to server to get list of announcements
				//announcementsGUI.populateAnnouncements(/*new announcements*/);
				cp.setTitle(0);
				cp.setContent(new AnnouncementsGUI(hubClient));
				cp.revalidate();
				cp.repaint();
			}
		});
		
		settingsLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
				 navColor = JColorChooser.showDialog(null,
				            "Choose Navigation Bar Color", Color.BLACK);
				 setBackground(navColor);
				 //TODO Save user's color
				 hubClient.getUserInfo().setColor(navColor);
				 ServerClientPackage scp = new ServerClientPackage(Category.UPDATE_COLOR, hubClient.getUserInfo());
				 hubClient.sendPackage(scp);
				 revalidate();
				 repaint();
			}
		});
		
		bitbucketLabel.addMouseListener(new MouseAdapter() {
			public void mouseReleased(MouseEvent e) {
			    try {
					URL url = new URL("https://bitbucket.org/usc-clubhub/club-hub");
			        openWebpage(url.toURI());
			    } catch (URISyntaxException e1) {
			        e1.printStackTrace();
			    } catch (MalformedURLException e1) {
					e1.printStackTrace();
				}
			}
		});
		infoLabel.addMouseListener(new MouseAdapter(){
			public void mouseReleased(MouseEvent e){
				clubDesc.setText(hubClient.getClubDesc());
				clubDesc.setVisible(true);
			}
		});
	}
	
	private static void openWebpage(URI uri) {
	    Desktop desktop = Desktop.isDesktopSupported() ? Desktop.getDesktop() : null;
	    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
	        try {
	            desktop.browse(uri);
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
	    }
	}
	

}
