package main;

import java.awt.BorderLayout;

import announcements.AnnouncementsGUI;
import client.HubClient;

public class GuestMainFrame extends MainFrame {
	private String clubName;
	
	public GuestMainFrame(HubClient hc, String clubName) {
		super(hc);
		cp.setClubName(clubName);
	}
	
	protected void instantiateComponents() {
		cp = new ContentPanel(hubClient, this);
		leftNav = new GuestNavigationPanel(cp, new AnnouncementsGUI(hubClient), hubClient);
	}
	
	protected void createGUI() {
		/* left nav */
		this.add(leftNav, BorderLayout.WEST);
		/* Content Panel - defualts to Announcements */
		this.add(cp, BorderLayout.CENTER);
		cp.setUserName("Guest");
	}
	
}
