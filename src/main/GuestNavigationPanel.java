package main;

import javax.swing.Box;

import announcements.AnnouncementsGUI;
import client.HubClient;

public class GuestNavigationPanel extends NavigationPanel {
	private static final long serialVersionUID = 1L;

	public GuestNavigationPanel(ContentPanel cp, AnnouncementsGUI announcementsGUI, HubClient hubClient) {
		super(cp, announcementsGUI, hubClient);
	}
	
	protected void createGUI() {
		this.add(logoLabel);
		this.add(announcementsLabel);
	//	this.add(calendarLabel);
	//	this.add(membersLabel);
	//	this.add(financeLabel);
		this.add(Box.createVerticalGlue());
		this.add(infoLabel);
		this.add(bitbucketLabel);
	}
}
