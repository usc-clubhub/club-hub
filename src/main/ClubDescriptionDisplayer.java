package main;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JTextArea;

import client.HubClient;
import customui.HubButton;
import server.ServerClientPackage;
import server.ServerClientPackage.Category;

public class ClubDescriptionDisplayer extends JFrame {
	
	private static final long serialVersionUID = 13485795436L;
	
	private HubClient hubClient;
	private JTextArea clubDescriptionArea;
	private HubButton changeText;
	
	public ClubDescriptionDisplayer(HubClient hubClient){
		this.hubClient = hubClient;
		setSize(new Dimension(350, 200));
		setLocationRelativeTo(null);
		setTitle("Club Info");
		clubDescriptionArea = new JTextArea();
		clubDescriptionArea.setRows(4);
		changeText = new HubButton("Save Text", customui.ThemeColors.SIGNUP_COLOR, customui.ThemeColors.SIGNUP_HIGHLIGHT_COLOR);
		add(clubDescriptionArea);
		clubDescriptionArea.setEditable(false);
		if(!hubClient.getUserInfo().getUsername().equals("Guest")){
			add(changeText, BorderLayout.SOUTH);
			clubDescriptionArea.setEditable(true);
		}
		hubClient.sendPackage(new ServerClientPackage(Category.GET_CLUB_DESCRIPTION, null));
		addActions();
		this.setDefaultCloseOperation(HIDE_ON_CLOSE);
		Font font = new Font("Verdana", Font.BOLD, 12);
		clubDescriptionArea.setFont(font);
	}
	public void addActions(){
		changeText.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent ae){
				hubClient.sendPackage(new ServerClientPackage(Category.SET_CLUB_DESCRIPTION, clubDescriptionArea.getText()));
				hubClient.setClubDesc(clubDescriptionArea.getText());
			}
		});
	}
	
	public void setText(String text){
		clubDescriptionArea.setText(text);
	}
//	public static void main(String[] args) {
//	String[] test = {};
//	ClubDescriptionDisplayer(new HubClient());
//	System.out.println(fd.select());
//	}
}
