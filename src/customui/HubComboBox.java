	package customui;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.util.Vector;

import javax.swing.JComboBox;
import javax.swing.border.EmptyBorder;

import library.FontLibrary;
import library.ImageLibrary;

public class HubComboBox<T> extends JComboBox<T> {
	private static final long serialVersionUID = -7835999902645001564L;
	private Image mBackgroundImage;
	private String text;
	private Font font;
	private Font fontBold;
	{	
		mBackgroundImage = ImageLibrary.getImage("img/ui/text.png");
		setOpaque(false);
		setBorder(new EmptyBorder(0,0,0,0));
		font = FontLibrary.getFont(Constants.FONT_GLOBER_REGULAR, 18);
		fontBold = FontLibrary.getFont(Constants.FONT_GLOBER_BOLD, 18);
		this.setForeground(ThemeColors.DARKGRAY);
		this.setFont(font);
		this.setUI(new HubComboBoxUI());
		text = "No set text";
	}
	
	public HubComboBox(){
		super();
	}
	
	public HubComboBox(Vector<T> vect){
		super(vect);
	}
	
	public void setText(String text) {
		this.text = text;
	}
	
	
	protected void paintComponent(Graphics g) {
		g.drawImage(mBackgroundImage, 0, 0, getWidth(), getHeight(), null);
		if (this.getSelectedIndex() == -1) {
			int height = this.getHeight();
			Font prev = g.getFont();
			Color prevColor = g.getColor();
			g.setFont(font);
			g.setColor(ThemeColors.INACTIVE_TEXT);
			int h = g.getFontMetrics().getHeight();
			int textBottom = (height - h) / 2 + h - 4;
			Graphics2D g2d = (Graphics2D) g;
			RenderingHints hints = g2d.getRenderingHints();
			g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			g2d.drawString(text, 10, textBottom);
			g2d.setRenderingHints(hints);
			g.setFont(prev);
			g.setColor(prevColor); 
		} else {
			String selectedIndex = (String) this.getSelectedItem();
			int height = this.getHeight();
			Font prev = g.getFont();
			Color prevColor = g.getColor();
			g.setFont(fontBold);
			int h = g.getFontMetrics().getHeight();
			int textBottom = (height - h) / 2 + h - 4;
			Graphics2D g2d = (Graphics2D) g;
			RenderingHints hints = g2d.getRenderingHints();
			g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
			g2d.drawString(selectedIndex, 10, textBottom);
			g2d.setRenderingHints(hints);
			g.setFont(prev);
			g.setColor(prevColor);
		}
	}
}
