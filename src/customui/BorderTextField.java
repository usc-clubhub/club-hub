package customui;

import java.awt.Color;
import java.awt.Font;

import javax.swing.BorderFactory;
import javax.swing.JTextField;

public class BorderTextField extends JTextField {
	private static final long serialVersionUID = 1L;
	{
		this.setBorder(BorderFactory.createMatteBorder(10, 10, 10, 10, new Color(235,235,235)));
		this.setBackground(Color.WHITE);
		this.setFont(new Font("Trebuchet MS", Font.PLAIN, 18));
	}
	public BorderTextField() {
		super();
	}
}
