package customui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.SwingConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class BorderButton extends HubButton {
	private static final long serialVersionUID = -1285510119716449816L;
	private Font font;
	public BorderButton(String text, Color buttonColor, Color highlightColor) {
		super(text, buttonColor, highlightColor);
		this.setOpaque(false);
		this.setBorder(new EmptyBorder(0,0,0,0));
		File font_file = new File(Constants.BUTTON_FONT);
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, font_file).deriveFont(Font.PLAIN, 16);
		} catch (FontFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.setFont(font);
		//this.setBackground(buttonColor);
	    Border thickBorder = new LineBorder(Color.WHITE, 3);
	    this.setBorder(thickBorder);
		this.setForeground(Color.WHITE);

		this.setVerticalTextPosition(SwingConstants.CENTER);

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				setBackground(highlightColor);
				
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				setBackground(buttonColor);
			}
		});
	}
}
