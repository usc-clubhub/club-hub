package customui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

public class HubButton extends JButton {
	private static final long serialVersionUID = -1285510119716449816L;
	private Font font;
	public HubButton(String text, Color buttonColor, Color highlightColor) {
		super(text);
        this.setPreferredSize(new Dimension(150, 50));
		this.setOpaque(true);
		this.setBorder(new EmptyBorder(0,0,0,0));
		File font_file = new File(Constants.BUTTON_FONT);
		try {
			font = Font.createFont(Font.TRUETYPE_FONT, font_file).deriveFont(Font.PLAIN, 16);
		} catch (FontFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		this.setFont(font);
		this.setBackground(buttonColor);
		this.setForeground(Color.WHITE);

		this.setVerticalTextPosition(SwingConstants.CENTER);

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				setBackground(highlightColor);
				
			}
			@Override
			public void mouseExited(MouseEvent arg0) {
				setBackground(buttonColor);
			}
		});
	}
}
