package customui;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Insets;
import java.awt.RenderingHints;
import javax.swing.ImageIcon;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import library.FontLibrary;
import library.ImageLibrary;

public class HubTextField extends JTextField{
	private static final long serialVersionUID = 3964529762960557244L;
	
	private String hint;
	private static Image mBackgroundImage;
	private Insets dummyInsets;
	private ImageIcon icon;
	private Font font, fontBold;
	{
		icon = null;
		setOpaque(false);
		setBorder(new EmptyBorder(4, 6, 4, 6));
		mBackgroundImage = ImageLibrary.getImage("img/ui/text.png");
		JTextField dummy = new JTextField();
	    Border border = UIManager.getBorder("TextField.border");
	    this.dummyInsets = border.getBorderInsets(dummy);
		font = FontLibrary.getFont(Constants.FONT_GLOBER_REGULAR, 18);
		fontBold = FontLibrary.getFont(Constants.FONT_GLOBER_BOLD, 18);
		setFont(fontBold);
		setForeground(ThemeColors.DARKGRAY);
	}
	public HubTextField(String string) {
		super();
		hint = string;
		this.setFont(fontBold);
		//icon = new ImageIcon(ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/user.jpg"), 25, 25));

	}
	public void setIcon(String address) {
		icon = new ImageIcon(ImageLibrary.getScaledImage(ImageLibrary.getImage(address), 25, 25));
		setBorder(new EmptyBorder(4, 40, 4, 6));
	}
	public HubTextField() {
		super();
	}
	@Override
	protected void paintComponent(Graphics g) {
		g.drawImage(mBackgroundImage, 0, 0, getWidth(), getHeight(), null);
		super.paintComponent(g);	
        if(this.icon!=null){
            int iconHeight = icon.getIconHeight();
            int x = dummyInsets.left + 5;
            int y = (this.getHeight() - iconHeight)/2;
            icon.paintIcon(this, g, x, y);
        	setMargin(new Insets(5, 2, 2, 2));
        } else {
        	setMargin(new Insets(2, 2, 2, 2));
        }

        if(this.getText().equals("")) {
        	//to avoid null pointer exception
        	if(hint == null){
        		hint = "";
        	}
            int height = this.getHeight();
            Font prev = g.getFont();
            Color prevColor = g.getColor();
            g.setFont(font);
            g.setColor(ThemeColors.INACTIVE_TEXT);
            int h = g.getFontMetrics().getHeight();
            int textBottom = (height - h) / 2 + h - 4;
            Graphics2D g2d = (Graphics2D) g;
            RenderingHints hints = g2d.getRenderingHints();
            g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            if (icon != null){ g2d.drawString(hint, 43, textBottom); }
            else { g2d.drawString(hint, 4, textBottom); }
            g2d.setRenderingHints(hints);
            g.setFont(prev);
            g.setColor(prevColor);
        }
	}
}