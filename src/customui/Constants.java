package customui;


public class Constants {
	public static final String BUTTON_FONT = "resources/fonts/Brandon_blk.otf";
	public static final String FONT_GLOBER = "resources/fonts/GloberBold.otf";
	public static final String FONT_GLOBER_REGULAR = "resources/fonts/GloberRegular.otf";
	public static final String FONT_GLOBER_BOLD = "resources/fonts/GloberBold.otf";
	public static final String FONT_GLOBER_THIN = "resources/fonts/GloberThin.otf";
	public static final String FONT_OPEN_SANS = "resources/fonts/OpenSans-Regular.ttf";
}
