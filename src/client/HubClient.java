package client;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import announcements.Announcement;
import announcements.AnnouncementsGUI;
import announcements.Comment;
import calendar.CalendarEvent;
import calendar.CalendarFrame;
import finance.FinancePanel;
import finance.Transaction;
import main.ContentPanel;
import membership.MembershipTable;
import server.ServerClientPackage;
import server.ServerClientPackage.Category;
import server.UserInformation;
import splash.LoginGUI;
import usermanagement.UserManagementPanel;

public class HubClient extends Thread{
	
	private ObjectInputStream ois;
	private ObjectOutputStream oos;
	private Socket s;
	private String hostname;
	private LoginGUI lgui;
	private ResultSet rs;
	private CalendarFrame calendarFrame;
	private UserInformation userInfo;
	private boolean noAnnouncements;
	private DefaultTableModel dtm;
	/* ALL GUI'S WILL BE STORED BELOW */
	/* THEY ARE STORED WITH SETTER METHODS ONLY AFTER THEY HAVE BEEN CREATED */
	private AnnouncementsGUI announcementsGUI;
	private ContentPanel contentPanel;
	private MembershipTable mt;
	private FinancePanel mFinancePanel;
	private String clubDesc;
	public HubClient(String hostname, int port, LoginGUI lgui){
		this.hostname = hostname;
		this.lgui = lgui;
		noAnnouncements = false;
		try {
			s = new Socket(hostname, port);
			oos = new ObjectOutputStream(s.getOutputStream());
			ois = new ObjectInputStream(s.getInputStream());
			this.start();
		} catch (IOException ioe) {
			lgui.showCannotConnectToServer();
			//System.out.println("ioe in OfficeClient: "+ ioe.getMessage());
		}
	}
	@SuppressWarnings("unchecked")
	public void run(){
		try {
			while(true) {
				//System.out.println("ABOUT TO RECEIVE PACKAGE");
				ServerClientPackage scp = (ServerClientPackage)ois.readObject();
				//System.out.println("RECEIVED A PACKAGE");
				if (scp.getCategory() == Category.CLUB_LIST){
					lgui.populateClubs((Vector<String>)scp.getObject());
				}
				else if (scp.getCategory() == Category.BG_IMG) {
					ByteArrayInputStream bais = new ByteArrayInputStream((byte[]) scp.getObject());
					BufferedImage bi = ImageIO.read(bais);
					lgui.setBackgroundImage(bi);
				}
				
				else if (scp.getCategory() == Category.CLUB_EXISTS) {
					JOptionPane.showMessageDialog(lgui, "A club with this name already exists", 
							"Error creating club", JOptionPane.PLAIN_MESSAGE);
				}
				else if (scp.getCategory() == Category.CLUB_CREATE_SUCCESS) {
					JOptionPane.showMessageDialog(lgui, "Club successfully created!", 
							"Success", JOptionPane.PLAIN_MESSAGE);
					setUserInfo((UserInformation)scp.getObject());
					lgui.proceedToMainWindow();		
				}
				else if(scp.getCategory() == Category.LOGIN_FAILURE){
					lgui.showLoginError();
				}
				else if(scp.getCategory() == Category.LOGIN_SUCCESS){
					//Close LoginGUI and open main window
					UserInformation uinfo = (UserInformation) scp.getObject();
					setUserInfo(uinfo);
					lgui.proceedToMainWindow();
				}
				else if(scp.getCategory() == Category.RETURN_MEMBERS) {
					//send back rs of members
					dtm = (DefaultTableModel) scp.getObject();
					JTable jt = new JTable(dtm){
						private static final long serialVersionUID = 1L;

						public boolean isCellEditable(int row, int col)
						    {
						        return false;
						    }
						};
					Color c = userInfo.getColor();
					mt.setTable(jt, c);
				}
				
				//FINANCE
				else if(scp.getCategory() == Category.RETURN_TRANSACTIONS){
					dtm = (DefaultTableModel) scp.getObject();
					JTable jt = new JTable(dtm){
						private static final long serialVersionUID = 1L;

						public boolean isCellEditable(int row, int col)
					    {
					        return false;
					    }
					};
					Color c = userInfo.getColor();
					mFinancePanel.setTable(jt, c);
				}
				else if(scp.getCategory() == Category.UPDATE_TRANSACTION){
					Transaction t = (Transaction)scp.getObject();
					//System.out.println("hubclient: updating transaction");
					mFinancePanel.addTransactionRow(t);
				}
				else if(scp.getCategory() == Category.FETCH_ANNOUNCEMENTS){
					ArrayList<Announcement> announcements = (ArrayList<Announcement>)scp.getObject();
					announcementsGUI.populateAnnouncements(announcements);
				}
				else if(scp.getCategory() == Category.NO_ANNOUNCEMENTS){
					//System.out.println("Client no announcements received");
					//announcementsGUI.noAnnouncements();
					//noAnnouncements = true;
				}
				else if(scp.getCategory() == Category.ALREADY_REQUESTED){
					lgui.showSignUpError1();
				}
				else if(scp.getCategory() == Category.USER_EXISTS){
					lgui.showSignUpError2();
				}
				else if(scp.getCategory() == Category.SIGN_UP_SUCCESS){
					lgui.showSignUpSuccess();
				}
				else if(scp.getCategory() == Category.NEW_POST) {
					announcementsGUI.newAnnouncementNotification();
				}
				else if(scp.getCategory() == Category.USER_LIST) {
					Vector<UserInformation> userList = (Vector<UserInformation>)scp.getObject();
					contentPanel.setContent(new UserManagementPanel(userList, this));
				}
				else if(scp.getCategory() == Category.SEND_CLUB_DESCRIPTION){
					clubDesc = (String)scp.getObject();
				}
				else if (scp.getCategory() == Category.COMMENT_LIST) {
					ArrayList<Object> pair = (ArrayList<Object>) scp.getObject();
					ArrayList<Comment> commentList = (ArrayList<Comment>) pair.get(0);
					int ID = (int) pair.get(1);
					announcementsGUI.setComments(commentList, ID);
				}
				else if(scp.getCategory() == Category.SEND_CALENDAR_EVENTS){
					ArrayList<CalendarEvent> event = (ArrayList<CalendarEvent>)scp.getObject();
					calendarFrame.populateEvents(event);
				}
				else if(scp.getCategory() == Category.GUEST_LOGIN_SUCCESS){
					lgui.proceedToGuestWindow();
				}
				
			}
		} catch (ClassNotFoundException cnfe) {
			System.out.println("cnfe: " + cnfe.getMessage());
		} catch (IOException ioe) {
			System.out.println("ioe: " + ioe.getMessage());
		} finally {
			try {
				if (s != null) {
					s.close();
				}
			} catch (IOException ioe) {
				System.out.println("ioe: " + ioe.getMessage());
			}
		}
	}
	public void sendPackage(ServerClientPackage scp){
		try {
			oos.writeObject(scp);
			oos.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public String getHostname(){
		return hostname;
	}
	public ObjectOutputStream getOOS(){
		return oos;
	}
	public JTable getJTable() {
		JTable jt = new JTable(dtm);
		return jt;
	}
	public ResultSet getResultSet(){
		return rs;
	}
	public void setUserInfo(UserInformation ui){
		userInfo = ui;
	}
	public UserInformation getUserInfo(){
		return userInfo;
	}
	public boolean noAnnouncements(){
		return noAnnouncements;
	}
	public void setAnnouncementsGUI(AnnouncementsGUI aGUI) {
		announcementsGUI = aGUI;
	}
	public void setContentPanel(ContentPanel content) {
		this.contentPanel = content;
	}
	public void setMembershipTable(MembershipTable membershipTable) {
		mt = membershipTable;
	}
	
	public void setFinancePanel(FinancePanel fp){
		mFinancePanel = fp;
	}
	public String getClubDesc() {
		return clubDesc;
	}
	public void setClubDesc(String desc){
		clubDesc = desc;
	}
	public void close() {
		try {
			if (s != null) {
				s.close();
			}
			if (ois != null) {
				ois.close();
			}
			if (oos != null) {
				oos.close();
			}
		} catch (IOException ioe) {
			System.out.println("ioe : " + ioe.getMessage());
		}
	}
	
	public void setCalendarFrame(CalendarFrame calendarFrame){
		this.calendarFrame = calendarFrame;
	}
}
