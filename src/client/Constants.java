package client;

public class Constants {
	public final static String SPLASH_IMAGE = "img/backgrounds/defaultBg.png";
	public final static String SPLASH_LOGO = "img/logo/logo2.png";

	public final static String LOGIN_TEXT = "LOGIN";
	public final static String SIGNUP_TEXT = "SIGN UP";
	public final static String GUEST_TEXT = "GUEST LOGIN";
	public final static String SELECT_SERVER_TEXT = "CONNECT";
	
	public final static String USERNAME_TEXT = "Username";
	public final static String PASSWORD_TEXT = "Password";
	public final static String SERVERS_FILE = "src/servers.txt";
	public final static String CLUBS_FILE = "src/clubs.txt";
	public static final String USER_ICON = "img/ui/user.jpg";
	public static final String SERVER_COMBOBOX_DEF_TEXT = "Select school";
	public static final String CLUB_COMBOBOX_DEF_TEXT = "Select club";
	public static final String CREATE_CLUB_TEXT = "CREATE CLUB";
	public static final String CANCEL_TEXT = "CANCEL";
}
