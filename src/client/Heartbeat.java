package client;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

import javax.swing.JOptionPane;

import main.MainFrame;
import server.ServerClientPackage;
import server.ServerClientPackage.Category;
import splash.LoginGUI;

public class Heartbeat extends Thread{
	
	private boolean logout = false;
	private ObjectOutputStream oos;
	private Socket s;
	private String username;
	private int counter;
	private MainFrame mf;
	public Heartbeat(ObjectOutputStream oos, String username, MainFrame mf){
		this.username = username;
		this.mf = mf;
		counter = 0;
		this.oos = oos;
		this.start();
	}
	
	public void run(){
		try {
			while(true) {
				ServerClientPackage scp;
				if(counter == 0){
					scp = new ServerClientPackage(Category.TIME_UPDATE, username);
				} else {
					scp = new ServerClientPackage(Category.HEARTBEAT, null);
				}
				oos.writeObject(scp);
				counter++;
				if(counter == 20)
					counter = 0;
				sleep(3000);
			}
		} catch (IOException e) {
			if (logout == false) {
				//Close the window, open a login
				JOptionPane.showMessageDialog(null, "Connection to server lost. Returning to login screen", 
					"Connection lost", JOptionPane.PLAIN_MESSAGE);
				//mf.setVisible(false);
				LoginGUI restartLogin = new LoginGUI();
				restartLogin.setVisible(true);
				mf.dispose();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} finally {
			try {
				if (s != null) {
					s.close();
				}
			} catch (IOException ioe) {
				System.out.println("ioe: " + ioe.getMessage());
			}
		}
	}
	public void setLogout() {
		logout = true;
	}
}
