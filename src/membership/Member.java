package membership;

import java.io.Serializable;

public class Member implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3769299286526473821L;
	private String firstName = "";
	private String lastName = "";
	private String email = "";
	private String phone = "";
	private String year = "";
	private String car = "";
	private String duesPaid = "";
	private String memType = "";
	private String admin = "";
			
	Member() {
		
	}
	
	//SETTERS
	public void setFirstName(String firstname_) {
		firstName = firstname_;
	}
	public void setLastName(String lastname_) {
		lastName = lastname_;
	}
	public void setPhone(String phone_) {
		phone = phone_;
	}
	public void setCar(String car_) {
		car = car_;
	}
	public void setDues(String duesPaid_) {
		duesPaid = duesPaid_;
	}
	public void setYear(String year_) {
		year = year_;
	}
	public void setEmail(String email_) {
		email = email_;
	}

	
	
	public void setAdmin(String admin_) {
		admin = admin_;
	}
	public void setMemType(String memType_) {
		memType = memType_;
	}
	//GETTERS
	//SETTERS
	public String getFirstName() {
		return firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public String getPhone() {
		return phone;
	}
	public String getCar() {
		return car;
	}
	public String getDues() {
		return duesPaid;
	}
	public String getYear() {
		return year;
	}
	public String getEmail() {
		return email;
	}

	public String getAdmin() {
		return admin;
	}
	public String getMemType() {
		return memType;
	}
}
