package membership;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import client.HubClient;
import customui.BorderButton;
import customui.HubButton;
import server.ServerClientPackage;
import server.ServerClientPackage.Category;

public class MembershipTable extends JPanel{
	private static final long serialVersionUID = 1L;
	private String selectedFN = "";
	private String selectedLN = "";
	private String selectedEmail = "";
	private String selectedPhone = "";
	private String selectedYear = "";
	private String selectedCar = "";
	private String selectedDuesPaid = "";
	private String selectedMemType = "";
	private String selectedAdmin = "";
	private HubClient hubclient;
	
	JTable table;
	Member currentMem = new Member();
	private int currentRow;
	public MembershipTable(HubClient hc) {
			this.setLayout(new BorderLayout());
			hubclient = hc;
			hubclient.setMembershipTable(this);
			//creates a ServerClientPackage that prompts server to return all members
			ServerClientPackage scp = new ServerClientPackage(Category.GET_MEMBERS, null);
			hc.sendPackage(scp);
			//gets the returned DefaultTableModel form the server
	}
	
	public void setTable(JTable t, Color color) {
		table = t;		
		DefaultTableCellRenderer center = new DefaultTableCellRenderer();
		center.setHorizontalAlignment(JLabel.CENTER);


		for (int i = 0; i < table.getColumnCount(); i++) {
			table.getColumnModel().getColumn(i).setCellRenderer(center);			
		}
		table.setRowHeight(50);

		table.setTableHeader(new JTableHeader(table.getColumnModel()) {
			private static final long serialVersionUID = 1L;
			@Override public Dimension getPreferredSize() {
			    this.setBackground(Color.white);
			    this.setFont(new Font("Helvana", Font.BOLD, 13));
				Dimension d = super.getPreferredSize();
			    d.height = 35;
			    return d;
			  }
			});

		table.setGridColor(Color.black);
        table.setBackground(Color.darkGray);
        table.setForeground(Color.white);
        table.setSelectionBackground(color);
        table.setSelectionForeground(Color.white);
        table.setOpaque(false);
        table.setFont(new Font("Helvana", Font.PLAIN, 12));
		table.setAutoCreateRowSorter(true);
		table.getTableHeader().setReorderingAllowed(false);
		table.setPreferredScrollableViewportSize(new Dimension(1000, 500));
		table.setSelectionModel(new ForcedListSelectionModel());
		table.setRowSelectionAllowed(true);
        //keeps track of the selected row of the table and updates the private variables

        JScrollPane tableHolder = new JScrollPane(table);	        
        JPanel buttonHolder = new JPanel();
        Box buttonBox = Box.createHorizontalBox();
        buttonHolder.setBackground(Color.darkGray);
		BorderButton addMem = new BorderButton("  Add member  ", Color.BLACK, Color.blue);
        buttonBox.add(addMem);
        //when the addMem button is clicked
        addMem.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
				Box addUserBox = Box.createVerticalBox();
				JLabel title = new JLabel("Add Member");
				title.setFont(new Font("Helvana",Font.BOLD, 20));
		        addUserBox.add(title, SwingConstants.CENTER);					
				addUserBox.add(Box.createRigidArea(new Dimension(20,20)));
				
				JLabel firstname = new JLabel("First Name: ", JLabel.RIGHT);
		        firstname.setFont(new Font("Helvana",Font.PLAIN, 16));
				addUserBox.add(firstname);
				JTextField firstname_text = new JTextField(8);
		        firstname_text.setFont(new Font("Helvana", Font.PLAIN, 16));
				addUserBox.add(firstname_text);
				JLabel lastname = new JLabel("Last Name: ", JLabel.RIGHT);
		        lastname.setFont(new Font("Helvana", Font.PLAIN, 16));
				addUserBox.add(lastname);
				JTextField lastname_text = new JTextField(8);
		        lastname_text.setFont(new Font("Helvana", Font.PLAIN, 16));
				addUserBox.add(lastname_text);
				JLabel email = new JLabel("Email: ", JLabel.RIGHT);
				email.setFont(new Font("Helvana", Font.PLAIN, 16));
				addUserBox.add(email);
				JTextField email_text = new JTextField(8);
		        email_text.setFont(new Font("Helvana", Font.PLAIN, 16));
				addUserBox.add(email_text);
				JLabel phone = new JLabel("Phone: ", JLabel.RIGHT);
		        phone.setFont(new Font("Helvana", Font.PLAIN, 16));
				addUserBox.add(phone);
				JTextField phone_text = new JTextField(8);
		        phone_text.setFont(new Font("Helvana", Font.PLAIN, 16));
				addUserBox.add(phone_text);
				JLabel year = new JLabel("Year: ", JLabel.RIGHT);
		        year.setFont(new Font("Helvana", Font.PLAIN, 16));
				addUserBox.add(year);
				JTextField year_text = new JTextField(8);
		        year_text.setFont(new Font("Helvana", Font.PLAIN, 16));
				addUserBox.add(year_text);
				JLabel car = new JLabel("Has a Car: ", JLabel.RIGHT);
		        car.setFont(new Font("Helvana", Font.PLAIN, 16));
				addUserBox.add(car);
				JCheckBox has_car = new JCheckBox();
				addUserBox.add(has_car);
				JLabel duesPaid = new JLabel("Dues Paid: ", JLabel.RIGHT);
		        duesPaid.setFont(new Font("Helvana", Font.PLAIN, 16));

				addUserBox.add(duesPaid);
				JCheckBox has_duesPaid = new JCheckBox();
				addUserBox.add(has_duesPaid);
				JLabel memType = new JLabel("Member Type: ");
		        memType.setFont(new Font("Helvana", Font.PLAIN, 16));
				addUserBox.add(memType);
				String[] memType_options = {"Semester", "Year"};
				JComboBox<String> memType_box = new JComboBox<String>(memType_options);
		        memType_box.setFont(new Font("Helvana", Font.PLAIN, 16));
		        memType_box.setForeground(Color.BLACK);
				addUserBox.add(memType_box);
				addUserBox.add(Box.createRigidArea(new Dimension(40,40)));

				HubButton ok = new HubButton("  Add Member  ", Color.BLACK, Color.blue);
				HubButton cancel = new HubButton("  Cancel  ", Color.BLACK, Color.blue);

				addUserBox.add(ok);
				addUserBox.add(Box.createRigidArea(new Dimension(20, 20)));
				addUserBox.add(cancel);
				//addPanel panel contains the addUserBox
				JPanel addPanel = new JPanel();
				addPanel.add(addUserBox);
				
				//customize addPanel
				addPanel.setBorder(BorderFactory.createLineBorder(Color.black));
				
				addUserBox.setBackground(Color.white);
				addPanel.setBackground(color.white);
				
				addPanel.setPreferredSize((new Dimension(600, 700)));
				
				tableHolder.setVisible(false);
				add(addPanel, BorderLayout.CENTER);
				addPanel.setVisible(true);
				buttonHolder.setVisible(false);
				setVisible(true);	
				
		        cancel.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        		addPanel.setVisible(false);
		        		add(tableHolder, BorderLayout.CENTER);
		                add(buttonHolder, BorderLayout.SOUTH);
		        		buttonHolder.setVisible(true);
		        		tableHolder.setVisible(true);
		        		revalidate();
		        	}
		        });
		        ok.addActionListener(new ActionListener() {
		        	public void actionPerformed(ActionEvent e) {
		        		DefaultTableModel model = (DefaultTableModel) table.getModel();
						String hasCar;
						String duesPaid;
						if (has_car.isSelected()) {
							hasCar = "Yes";
						}
						else {
							hasCar = "No";
						}
						if (has_duesPaid.isSelected()) {
							duesPaid = "Yes";
						}
						else {
							duesPaid = "No";
						}
						String membertype = String.valueOf(memType_box.getSelectedItem());
						Member new_mem = new Member();
						new_mem.setFirstName(firstname_text.getText());
						new_mem.setLastName(lastname_text.getText());
						new_mem.setPhone(phone_text.getText());
						new_mem.setEmail(email_text.getText());
						new_mem.setYear(year_text.getText());
						new_mem.setCar(hasCar);
						new_mem.setMemType(membertype);
						new_mem.setDues(duesPaid);
						if (!(new_mem.getFirstName().matches(".*[a-zA-Z]+.*"))) {
							JOptionPane.showMessageDialog(addPanel,
								    "Please enter first name, last name, and email of member",
								    "Add Error",
								    JOptionPane.ERROR_MESSAGE);
						}
						else if (!(new_mem.getLastName().matches(".*[a-zA-Z]+.*"))) {
							JOptionPane.showMessageDialog(addPanel,
								    "Please enter first name, last name, and email of member",
								    "Add Error",
								    JOptionPane.ERROR_MESSAGE);
						}
						else if (new_mem.getEmail().equals("")) {
							JOptionPane.showMessageDialog(addPanel,
								    "Please enter first name, last name, and email of member",
								    "Add Error",
								    JOptionPane.ERROR_MESSAGE);
						}
						else {
							model.addRow(new Object[]{new_mem.getFirstName(), new_mem.getLastName(), new_mem.getEmail(), new_mem.getPhone(), new_mem.getYear(), new_mem.getCar(),  new_mem.getMemType(), new_mem.getDues(), "No"});
							ServerClientPackage scp = new ServerClientPackage(Category.ADD_MEMBER, new_mem);
							hubclient.sendPackage(scp);
			        		addPanel.setVisible(false);
			        		add(tableHolder, BorderLayout.CENTER);
			                add(buttonHolder, BorderLayout.SOUTH);
			        		buttonHolder.setVisible(true);
			        		tableHolder.setVisible(true);
			        		revalidate();	
						}
		        	}
		        });
        	}
        });
		BorderButton editMem = new BorderButton("  Edit Member  ", Color.BLACK, Color.blue);
		buttonBox.add(Box.createRigidArea(new Dimension(20,20)));
		buttonBox.add(editMem);
        //when the addMem button is clicked
        editMem.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
				if (table.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(tableHolder,
						    "No user is selected",
						    "Edit Error",
						    JOptionPane.ERROR_MESSAGE);
				}
				else {
					updateSelected();
	        		//get the selected row     
	        		Box editUserBox = Box.createVerticalBox();
					JLabel title = new JLabel("Edit Member");				
					title.setFont(new Font("Helvana",Font.BOLD, 20));
					editUserBox.add(title, SwingConstants.CENTER);					
					editUserBox.add(Box.createRigidArea(new Dimension(20,20)));
					JLabel firstname = new JLabel("First Name: ", JLabel.RIGHT);
			        firstname.setFont(new Font("Helvana", Font.PLAIN, 16));
					editUserBox.add(firstname);				
					JTextField firstname_text = new JTextField(8);
					firstname_text.setText(selectedFN);
			        firstname_text.setFont(new Font("Helvana", Font.PLAIN, 16));
					editUserBox.add(firstname_text);
					JLabel lastname = new JLabel("Last Name: ", JLabel.RIGHT);
			        lastname.setFont(new Font("Helvana", Font.PLAIN, 16));
					editUserBox.add(lastname);
					JTextField lastname_text = new JTextField(8);
					lastname_text.setFont(new Font("Helvana", Font.PLAIN, 16));
					lastname_text.setText(selectedLN);
					editUserBox.add(lastname_text);
					JLabel email = new JLabel("Email: ", JLabel.RIGHT);
					email.setFont(new Font("Helvana", Font.PLAIN, 16));
					editUserBox.add(email);
					JTextField email_text = new JTextField(8);
					email_text.setFont(new Font("Helvana", Font.PLAIN, 16));
					email_text.setText(selectedEmail);
					editUserBox.add(email_text);
					JLabel phone = new JLabel("Phone: ", JLabel.RIGHT);
					phone.setFont(new Font("Helvana", Font.PLAIN, 16));
					editUserBox.add(phone);
					JTextField phone_text = new JTextField(8);
					phone_text.setFont(new Font("Helvana", Font.PLAIN, 16));
					phone_text.setText(selectedPhone);
					editUserBox.add(phone_text);
					JLabel year = new JLabel("Year: ", JLabel.RIGHT);
					year.setFont(new Font("Helvana", Font.PLAIN, 16));
					editUserBox.add(year);
					JTextField year_text = new JTextField(8);
					year_text.setFont(new Font("Helvana", Font.PLAIN, 16));
					year_text.setText(selectedYear);
					editUserBox.add(year_text);
					JLabel car = new JLabel("Has a Car: ", JLabel.RIGHT);
					car.setFont(new Font("Helvana", Font.PLAIN, 16));
					editUserBox.add(car);
					JCheckBox has_car = new JCheckBox();
					System.out.println(selectedCar);
					if (selectedCar.equals("Yes")) {
						has_car.setSelected(true);;
					}
					else {
						has_car.setSelected(false);
					}
					editUserBox.add(has_car);
					JLabel duesPaid = new JLabel("Dues Paid: ", JLabel.RIGHT);
					duesPaid.setFont(new Font("Helvana", Font.PLAIN, 16));
					editUserBox.add(duesPaid);
					JCheckBox has_duesPaid = new JCheckBox();
					if (selectedDuesPaid.equals("Yes")) {
						has_duesPaid.setSelected(true);;
					}
					else {
						has_duesPaid.setSelected(false);
					}
					editUserBox.add(has_duesPaid);
					JLabel memType = new JLabel("Member Type: ");
					memType.setFont(new Font("Helvana", Font.PLAIN, 16));
					editUserBox.add(memType);
					String[] memType_options = {"Semester", "Year"};
					JComboBox<String> memType_box = new JComboBox<String>(memType_options);
					memType_box.setFont(new Font("Helvana", Font.PLAIN, 16));
					memType_box.setForeground(Color.BLACK);
					memType_box.setSelectedItem(selectedMemType);
					editUserBox.add(memType_box);
					editUserBox.add(Box.createRigidArea(new Dimension(40,40)));
	
					HubButton ok = new HubButton("  Edit Member  ", Color.BLACK, Color.blue);
					HubButton cancel = new HubButton("  Cancel  ", Color.BLACK, Color.blue);

					editUserBox.add(ok);
					editUserBox.add(Box.createRigidArea(new Dimension(20, 20)));
					editUserBox.add(cancel);
					//addPanel panel contains the addUserBox
					JPanel editPanel = new JPanel();
					editPanel.add(editUserBox);
					
					//customize addPanel
					editPanel.setBorder(BorderFactory.createLineBorder(Color.black));
					
					editUserBox.setBackground(Color.white);
					editPanel.setBackground(Color.white);
					
					editPanel.setPreferredSize((new Dimension(600, 700)));
 
					tableHolder.setVisible(false);
					add(editPanel, BorderLayout.CENTER);
					editPanel.setVisible(true);
					buttonHolder.setVisible(false);
					setVisible(true);
					
					//save data BEFORE it is editted
					String old_hasCar;
					String old_duesPaid;
					if (has_car.isSelected()) {
						old_hasCar = "Yes";
					}
					else {
						old_hasCar = "No";
					}
					if (has_duesPaid.isSelected()) {
						old_duesPaid = "Yes";
					}
					else {
						old_duesPaid = "No";
					}
					String membertype = String.valueOf(memType_box.getSelectedItem());
					Member old_mem = new Member();
					old_mem.setFirstName(firstname_text.getText());
					old_mem.setLastName(lastname_text.getText());
					old_mem.setPhone(phone_text.getText());
					old_mem.setEmail(email_text.getText());
					old_mem.setYear(year_text.getText());
					old_mem.setCar(old_hasCar);
					old_mem.setMemType(membertype);
					old_mem.setDues(old_duesPaid);
					
					cancel.addActionListener(new ActionListener() {
			        	public void actionPerformed(ActionEvent e) {
			        		editPanel.setVisible(false);
			        		add(tableHolder, BorderLayout.CENTER);
			                add(buttonHolder, BorderLayout.SOUTH);
			        		buttonHolder.setVisible(true);
			        		tableHolder.setVisible(true);
			        	}
			        });
					ok.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							DefaultTableModel model = (DefaultTableModel) table.getModel();
							String hasCar;
							String duesPaid;
							if (has_car.isSelected()) {
								hasCar = "Yes";
							}
							else {
								hasCar = "No";
							}
							if (has_duesPaid.isSelected()) {
								duesPaid = "Yes";
							}
							else {
								duesPaid = "No";
							}
							String membertype = String.valueOf(memType_box.getSelectedItem());
							Member new_mem = new Member();
							new_mem.setFirstName(firstname_text.getText());
							new_mem.setLastName(lastname_text.getText());
							new_mem.setPhone(phone_text.getText());
							new_mem.setEmail(email_text.getText());
							new_mem.setYear(year_text.getText());
							new_mem.setCar(hasCar);
							new_mem.setMemType(membertype);
							new_mem.setDues(duesPaid);
							model.removeRow(currentRow);
							model.addRow(new Object[]{new_mem.getFirstName(), new_mem.getLastName(), new_mem.getEmail(), new_mem.getPhone(), new_mem.getYear(), new_mem.getCar(),  new_mem.getMemType(), new_mem.getDues()});
							ServerClientPackage scp = new ServerClientPackage(Category.DELETE_MEMBER, old_mem);
							hubclient.sendPackage(scp);
							ServerClientPackage scp2 = new ServerClientPackage(Category.ADD_MEMBER, new_mem);
							hubclient.sendPackage(scp2);
			        		editPanel.setVisible(false);
			        		add(tableHolder, BorderLayout.CENTER);
			                add(buttonHolder, BorderLayout.SOUTH);
			        		buttonHolder.setVisible(true);
			        		tableHolder.setVisible(true);
			        		revalidate();
						}
					});
				}
        	}
        });
        //add the remove button
		BorderButton removeMem = new BorderButton("  Delete member  ", Color.BLACK, Color.blue);
		buttonBox.add(Box.createRigidArea(new Dimension(20,20)));
		buttonBox.add(removeMem);
		
        removeMem.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
				updateSelected();
				if (table.getSelectedRow() == -1) {
					JOptionPane.showMessageDialog(tableHolder,
						    "No user is selected",
						    "Delete Error",
						    JOptionPane.ERROR_MESSAGE);
				}
				else if (selectedAdmin.equals("Yes")) {
					JOptionPane.showMessageDialog(tableHolder,
						    "You cannot delete the admin",
						    "Delete Error",
						    JOptionPane.ERROR_MESSAGE);
				}
				else {
					int n = JOptionPane.showConfirmDialog(tableHolder, "Are you sure you want to delete " + currentMem.getFirstName() + " " + currentMem.getLastName() + "?", "Confirm delete", JOptionPane.YES_NO_OPTION);
					if (n == JOptionPane.YES_OPTION) {
						DefaultTableModel model = (DefaultTableModel) table.getModel();
						model.removeRow(currentRow);
						ServerClientPackage scp = new ServerClientPackage(Category.DELETE_MEMBER, currentMem);
						hubclient.sendPackage(scp);	
					}
				}
        	}
        });
        //add all buttons to buttonHolder panel
        buttonHolder.add(buttonBox);
        //set buttonHolder and tableHolder to be visible
        buttonHolder.setVisible(true);
        tableHolder.setVisible(true);
        //add tableHolder and buttonHolder to the JPanel
        this.add(tableHolder, BorderLayout.CENTER);
        this.add(buttonHolder, BorderLayout.SOUTH);
        //set it to be visible- repaint and revalidate
        this.setVisible(true);
		this.revalidate();
		this.repaint();
    }
        
	
	public void updateSelected() {
        if (table.getSelectedRow() > -1) {
        	currentRow = table.getSelectedRow();
        	currentMem.setFirstName(table.getValueAt(table.getSelectedRow(), 0).toString());
        	selectedFN = currentMem.getFirstName();
        	currentMem.setLastName(table.getValueAt(table.getSelectedRow(), 1).toString());
        	selectedLN = currentMem.getLastName();
        	currentMem.setEmail(table.getValueAt(table.getSelectedRow(), 2).toString());
        	selectedEmail = currentMem.getEmail();
			if (table.getValueAt(table.getSelectedRow(), 3) != null) {
				selectedPhone = table.getValueAt(table.getSelectedRow(), 3).toString();						
			}
			if (table.getValueAt(table.getSelectedRow(), 4) != null) {
				selectedYear = table.getValueAt(table.getSelectedRow(), 4).toString();
			}
			if (table.getValueAt(table.getSelectedRow(), 5) != null) {
				selectedCar = table.getValueAt(table.getSelectedRow(), 5).toString();
			}
			if (table.getValueAt(table.getSelectedRow(), 6) != null) {
				selectedMemType = table.getValueAt(table.getSelectedRow(), 6).toString();
			}
			if (table.getValueAt(table.getSelectedRow(), 7) != null) {
				selectedDuesPaid = table.getValueAt(table.getSelectedRow(), 7).toString();
			}
			if (table.getValueAt(table.getSelectedRow(), 8) != null) {
				selectedAdmin = table.getValueAt(table.getSelectedRow(), 8).toString();
			}
			
        }
	}
	
}
	 
