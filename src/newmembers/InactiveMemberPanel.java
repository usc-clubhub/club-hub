package newmembers;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import library.ImageLibrary;
import server.UserInformation;

public class InactiveMemberPanel extends JPanel {
	private static final long serialVersionUID = 1L;
	UserInformation member;
	JLabel userIconLabel;
	
	public InactiveMemberPanel(UserInformation member) {
		this.setLayout(new GridBagLayout());
		this.member = member;
		this.setBorder(BorderFactory.createMatteBorder(0, 0, 1, 0, new Color(228,228,228)));
	}
	
	private void instantiateComponents() {
		Image img = ImageLibrary.getScaledImage(ImageLibrary.getImage("img/ui/userTop.png"), 20, 20);
		userIconLabel = new JLabel(new ImageIcon(img));
		userIconLabel.setAlignmentX(LEFT_ALIGNMENT);
		userIconLabel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
		
	}
	private void createGUI() {
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(userIconLabel);
	}
}
